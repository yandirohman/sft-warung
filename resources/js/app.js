require('./bootstrap');

require('alpinejs');

import Vue from 'vue'
import Swal from 'sweetalert2'
import VueSweetalert2 from 'vue-sweetalert2';


Vue.use(VueSweetalert2);
Vue.component('monitoring', require('./components/Monitoring').default)
Vue.component('pencairan-rembes', require('./components/PencairanRembes').default)

const app = new Vue({
  el: "#app",
})
