export const lineChartData = {
  type: "line",
  data: {
    labels: ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"],
    datasets: [
      {
        label: "Dana Bantuan",
        data: [0, 0, 1, 2, 79, 82, 27, 14],
        backgroundColor: "rgb(74,164,144,0)",
        borderColor: "rgb(74,164,144)",
        borderWidth: 3
      },
      {
        label: "Realisasi",
        data: [0, 0, 1, 2, 79, 82, 27, 14],
        backgroundColor: "rgba(120, 120, 120,0)",
        borderColor: "rgba(120, 120, 120)",
        borderWidth: 3
      },
      {
        label: "Rembes",
        data: [0.166, 2.081, 3.003, 0.323, 954.792, 285.886, 43.662, 51.514],
        backgroundColor: "rgb(107,113,146,0)",
        borderColor: "rgb(107,113,146)",
        borderWidth: 3
      }
    ]
  },
  options: {
    responsive: true,
    lineTension: 1,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            padding: 25
          }
        }
      ]
    }
  }
};

export default lineChartData;
