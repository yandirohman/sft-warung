@extends('layouts.master-bjb')

@section('title', 'Cetak Pencairan Rembes')

@section('pencairan-rembes', 'active')

@section('content')
    <div class="col-md-12">
        <div class="card-custom container-fluid">
            @include('layouts.partials.bjb.navbar')
            <div class="card-body">
                <button class="btn mb-4 btn-grey-white" onclick="window.print()"><span class="fas fa-print"></span> Cetak Laporan</button>
                <div class="print-area">
                    <div id="print-area">
                        <h3 class="title">Daftar Transaksi Yang Dirembeskan</h3>
                        <div class="info-warung">
                          <div class="row">
                            <p class="col-2">Nama Warung</p>
                            <p class="col-10">: {{$warung->nama_warung}}</p>
                          </div>
                          <div class="row">
                            <p class="col-2">Nama Pemilik Warung</p>
                            <p class="col-10">: {{$warung->nama_pemilik}}</p>
                          </div>
                          <div class="row">
                            <p class="col-2">No Telepon</p>
                            <p class="col-10">: {{$warung->hp}}</p>
                          </div>
                          <div class="row">
                            <p class="col-2">Tanggal Rembes</p>
                            <p class="col-10">: {{ date('d/m/Y') }}</p>
                          </div>
                        </div>

                        <table class="table table-bordered mt-4" style="margin-bottom:5rem">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal</th>
                                    <th>NIK Penerima</th>
                                    <th>Nama Penerima</th>
                                    <th>Nominal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($transaksi as $t)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ date('d/m/Y', strtotime($t->created_at)) }}</td>
                                        <td>{{ $t->nik }}</td>
                                        <td>{{ $t->nama }}</td>
                                        <td>Rp {{ number_format($t->total_harga, 2,',','.') }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">Tidak Ada Data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>

                        <div class="row">
                          <div class="col-10"></div>
                          <div class="col-2">
                            <p class="text-center">{{date('d F Y')}}</p>
                            <br><br><br>
                            <p class="text-center mb-0">{{$warung->nama_pemilik}}</p>
                            <p class="text-center">Pemilik Warung</p>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('custom-css')
    <style>
        .pull-right{
            float: right;
        }

        .print-area{
            padding: 1.5rem;
            border: 2px solid black;
        }

        .print-area .title{
            text-align: center;
            font-size: 16px;
            font-weight: bold;
            padding: 1.5rem;
        }

        .info-warung p{
          font-size:14px;
        }

        table thead tr th{
            text-align: center;
            font-size : 14px;
        }

        table tbody tr td{
            text-align: center;
            font-size:12px;
        }

        @media print {
            body * {
                visibility: hidden;
            }
            #print-area, #print-area * {
                visibility: visible;
            }
            #print-area {
                width: 100%;
                position: absolute;
                left: 0;
                top: 0;
            }
        }
    </style>
@endpush

@push('custom-js')
    <script>

    </script>
@endpush
