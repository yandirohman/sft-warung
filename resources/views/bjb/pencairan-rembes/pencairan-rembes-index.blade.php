@extends('layouts.master-bjb')

@section('title', 'Pencairan Rembes')

@section('pencairan-rembes', 'active')

@section('content')
  <div class="container-fluid" id="app">
    <pencairan-rembes></pencairan-rembes>
  </div>
@stop
@push('custom-css')
    <style>
      form {
        display: inline-block;
      }

      .span-box {
        border-color: gray;
        border-radius: 5px;
      }
      .control-page {
        margin: 0 10px;
      }

      @media only screen and (max-width: 600px) {
        .control-page {
          margin: 10px;
        }

        a {
          margin-bottom: 10px;
        }

        form {
          width: 100%;
        }

        .btn-grey-white {
          min-width: 49%;
        }
      }

      @media only screen and (min-width: 1366px) {
        form {
          margin-left: 2px;
          width: 22%;
        }
      }

      @media only screen and (min-width: 1920px) {
        form {
          margin-left: 10px;
          width: 45%;
        }
      }
    /* daterange picker */
    .calendars {
      width: 100%;
    }
    .vue-daterange-picker {
      width: 100% !important;
    }
    .vue-daterange-picker .form-control {
      display: none !important
    }
    .daterangepicker.ltr.show-calendar.show-ranges.opensinline.linked {
      width: 100% !important;
      min-width: 0 !important;
      border: 0 !important;
      outline: none;
      box-shadow: 1px 2px 5px 1px rgb(0 0 0 / 10%);
      border-radius: 2.5px;
      margin: 0 !important;
      background-color: #FFFFFF !important;
    }
    .ranges.col-12.col-md-auto ul {
      display: flex;
      flex-direction: row;
      white-space: nowrap;
    }
    .drp-calendar, .drp-buttons {
      border: none !important;
    }
    .in-range {
      background-color: #5c6bc08a !important;
      color: #fff !important;
    }
    .active.in-range, li.active {
      background-color: #9575cd !important;
      color: #fff !important;
    }
    .in-range.disabled {
      background: #5c6bc08a !important;
    }
    .applyBtn {
      background-color: #17173fa6 !important;
      padding: 5px 15px !important;
    }
    .cancelBtn {
      color: #6c757d !important;
      background-color: #ccc !important;
      padding: 5px 15px !important;
    }
    </style>
@endpush
