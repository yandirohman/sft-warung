@extends('layouts.master-bjb', [
  'monitoring' => true
])

@section('title', 'Dashboard Dinsos')

@section('monitoring', 'active')

@section('content')
  <div id="app" class="container-fluid">
    <monitoring role="bjb" />
  </div>
@stop
@push('custom-css')
<style>

    .span-box {
      border-color: gray;
    }
    .control-page {
      margin: 0 10px;
      width: 100% !important;
    }
    .vue-daterange-picker .form-control {
      display: none !important
    }
    .daterangepicker {
      width: 100% !important;
      border: 0 !important;
      outline: none;
      box-shadow: 1px 2px 5px 1px rgb(0 0 0 / 10%);
      border-radius: 2.5px;
      margin: 0 !important;
      background-color: #FFFFFF !important;
    }
    .drp-calendar, .drp-buttons {
      border: none !important;
    }
    .in-range {
      background-color: #5c6bc08a !important;
      color: #fff !important;
    }
    .active.in-range, li.active {
      background-color: #9575cd !important;
      color: #fff !important;
    }
    .in-range.disabled {
      background: #5c6bc08a !important;
    }
    li.active:active,li.active::before {
      border: 0 !important;

    }
    .applyBtn {
      background-color: #17173fa6 !important;
      padding: 5px 15px !important;
    }
    .cancelBtn {
      color: #6c757d !important;
      background-color: #ccc !important;
      padding: 5px 15px !important;
    }
    td.disabled {
      text-decoration: none !important;
      background: transparent !important;
    }
    @media only screen and (max-width: 600px) {
      .control-page {
        margin: 10px;
      }
    }
    .cari {
      border: 1px solid rgb(209, 211, 226)
    }
    .full-width: {
      width: 100% !important;
    }

    .card-custom-body {
      padding: 15px;
    }
    .card-custom-title {
      font-weight: bold;
      font-size: 14px;
      margin-bottom: 10px;
    }
    .card-custom-body {
      font-weight: bold;
      font-size: 17px;
    }

    .vue-daterange-picker {
      width: 100% !important;
    }


  .input-group-text {
    padding-left: 10px;
    background: transparent !important;
    border-right: none;
    /* border-left: 1px solid rgb(209, 211, 226); */
  }
  .input-group-text i {
    color: #ccc;
  }
  .form-control.cari {
    border-left: none;
  }
  .form-control.cari::placeholder {
    color: #ccc;
  }
  hr {
    margin: 0;
    border-top: 1px solid #eaeaeab9;
  }
  .tr-card {
    box-shadow: 1px 2px 3px rgba(0,0,0,.2);
    background:#fff;
  }
  .calendars {
    widows: 100%;
  }
  table {
    border-collapse:separate;
    border-spacing: 0 0.5rem !important;
  }
  .daterangepicker .drp-calendar {
    width:45% !important;
  }

  .daterangepicker.show-calendar .ranges {
    width: 20% !important;
  }

  .daterangepicker .calendars-container {
    width: 80% !important;
  }

  @media screen and (min-width: 1336px){
    .daterangepicker.show-ranges.show-weeknumbers[data-v-4391f606], .daterangepicker.show-ranges[data-v-4391f606] {
      min-width: 100% !important;
    }
  }

  .filter-pencairan {
    cursor: pointer;
  }
</style>
@endpush
