@extends('layouts.master-bjb')

@section('title', 'Dashboard Bjb')

@section('overview', 'active')

@section('content')
  <div class="col-md-9">
    <div class="card-custom container-fluid">
      @include('layouts.partials.bjb.navbar')
      <div class="row">
          <div class="col-xl-6">
              <div class="card-custom">
                  <div class="card-header pb-2">
                      <h4 class="title">Besaran Penyaluran Dana Perbulan</h4>
                      <h4 class="sub-title">Dana Bantuan vs Penyaluran</h4>
                  </div>
                  <div class="card-body py-2">
                      <div id="dana-vs-realisasi"></div>
                  </div>
              </div>
          </div>

          <div class="col-xl-3">
              <div class="card-custom">
                  <div class="card-header pb-2">
                      <h4 class="title">Persentasi Penyaluran Sembako</h4>
                      <h4 class="sub-title">Tersalurkan vs Belum Tersalurkan</h4>
                  </div>
                  <div class="card-body py-2">
                      <canvas id="pie-deposit" width="400" height="300"></canvas>
                  </div>
              </div>
          </div>

          <div class="col-xl-3">
              <div class="card-custom">
                  <div class="card-header pb-2">
                      <h4 class="title">Persentasi Dana Rembes Warung</h4>
                      <h4 class="sub-title">Rembes vs Belum Rembes</h4>
                  </div>
                  <div class="card-body py-2">
                      <canvas id="pie-dana-blt" width="400" height="300"></canvas>
                  </div>
              </div>
          </div>
      </div>

      <div class="row">
          <div class="col-xl-3">
              <div class="card-custom">
                  <div class="card-header p-3">
                      <h4 class="title">Deposit Warung</h4>
                      <div class="row">
                          <div class="col-7 nominal-info">
                              <p class="info-title">Total Rembes</p>
                              <p class="info-nominal">{{GeneralHelper::toRupiah($totalRembes)}}</p>
                          </div>
                          <div class="col-5 pl-0">
                              <i class="fas fa-search input-search-icon"></i>
                              <input type="text" class="form-control search small-search" placeholder="search..." id="table-deposit-search">
                          </div>
                      </div>
                  </div>
                  <div class="card-body p-2">
                      <div class="table-responsive">
                          <table class="card-table text-center" id="table-deposit">
                              <thead>
                                  <tr>
                                      <th class="col-4">Warung</th>
                                      <th class="col-4">Jml Penerima</th>
                                      <th class="col-4">Nominal</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  @forelse ($warungTerlaris as $warung)
                                      <tr>
                                          <td class="col-4">{{$warung->nama_warung}}</td>
                                          <td class="col-4">{{$warung->jumlah_penerima ?? 0}}</td>
                                          <td class="col-4">{{GeneralHelper::toRupiah($warung->nominal ?? 0)}}</td>
                                      </tr>
                                  @empty
                                      <tr>
                                          <td class="col-12">Data Tidak Ada</td>
                                      </tr>
                                  @endforelse
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>

          <div class="col-xl-3">
              <div class="card-custom">
                  <div class="card-header p-3">
                      <h4 class="title">Harga Per Produk</h4>
                      <div class="row pt-3">
                          <div class="col-7 nominal-info pr-5">
                          </div>
                          <div class="col-5 pl-0" style="height:fit-content; margin:auto">
                              <i class="fas fa-search input-search-icon"></i>
                              <input type="text" class="form-control search small-search" placeholder="search..." id="table-harga-search">
                          </div>
                      </div>
                  </div>
                  <div class="card-body p-2">
                      <div class="table-responsive">
                          <table class="card-table text-center" id="table-harga">
                              <thead>
                                  <tr>
                                      <th class="col-4">Produk</th>
                                      <th class="col-4">Harga Terendah</th>
                                      <th class="col-4">Harga Tertinggi</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  @forelse ($produk as $produk)
                                      <tr>
                                          <td class="col-4"><a href="#" onclick="hargaProdukPerWarung('{{$produk->id}}')">{{$produk->nama}}</a></td>
                                          <td class="col-4">{{GeneralHelper::toRupiah($produk->harga_terendah ?? 0)}}</td>
                                          <td class="col-4">{{GeneralHelper::toRupiah($produk->harga_tertinggi ?? 0)}}</td>
                                      </tr>
                                  @empty
                                      <tr>
                                          <td class="col-12">Tidak Ada Data</td>
                                      </tr>
                                  @endforelse
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>

          <div class="col-xl-6">
              <div class="card-custom">
                  <div class="card-header">
                      <h4 class="title">Riwayat Pencairan Dana BLT (Real Time)</h4>
                  </div>
                  <div class="card-body">
                      <div class="card-tabs">
                          <div class="row">
                              <div class="col-9">
                                  <div class="tabs">
                                      <label class="tab" id="realtime-tab" onclick="bukaTab('realtime')">Hari Ini</label>
                                      <label class="tab" id="bulan-ini-tab" onclick="bukaTab('bulan-ini')">Bulan Ini</label>
                                      <label class="tab" id="minggu-ini-tab" onclick="bukaTab('minggu-ini')">Minggu Ini</label>
                                      {{-- <label class="tab" id="gelombang-1-tab" onclick="bukaTab('gelombang-1')">Gelombang 1</label> --}}
                                  </div>
                              </div>
                              <div class="col-3 pl-0">
                                  <i class="fas fa-search input-search-icon"></i>
                                  <input type="text" class="form-control search small-search" placeholder="cari hari ini" id="table-riwayat-hari-ini-search">
                                  <input type="text" class="form-control search small-search" placeholder="cari bulan ini" id="table-riwayat-bulan-ini-search">
                                  <input type="text" class="form-control search small-search" placeholder="cari minggu ini" id="table-riwayat-minggu-ini-search">
                              </div>
                          </div>
                          <div class="panels">
                              <div class="panel" id="realtime-panel">
                                  <div class="table-responsive">
                                      <table class="table table-striped table-hover text-center" id="table-riwayat-hari-ini">
                                          <thead>
                                              <tr>
                                                  <th>Tanggal</th>
                                                  <th>Warung</th>
                                                  <th>ID Warung</th>
                                                  <th>Penerima</th>
                                                  <th>NIK</th>
                                                  <th>Nominal</th>
                                                  <th>Aksi</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @forelse ($riwayatTransaksiHariIni as $transaksi)
                                                  <tr>
                                                      <td>{{$transaksi->created_at}}</td>
                                                      <td>{{$transaksi->nama_warung}}</td>
                                                      <td>{{$transaksi->warung_id}}</td>
                                                      <td>{{$transaksi->nama}}</td>
                                                      <td>{{$transaksi->nik}}</td>
                                                      <td>{{GeneralHelper::toRupiah($transaksi->total_harga ?? 0)}}</td>
                                                      <td>
                                                          <button class="btn btn-primary">Detail</button>
                                                      </td>
                                                  </tr>
                                              @empty
                                                  <tr>
                                                      <td colspan="7">Tidak Ada Data</td>
                                                  </tr>
                                              @endforelse
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                              <div class="panel" id="bulan-ini-panel">
                                  <div class="table-responsive">
                                      <table class="table table-striped table-hover text-center" id="table-riwayat-bulan-ini">
                                          <thead>
                                              <tr>
                                                  <th>Tanggal</th>
                                                  <th>Warung</th>
                                                  <th>ID Warung</th>
                                                  <th>Penerima</th>
                                                  <th>NIK</th>
                                                  <th>Nominal</th>
                                                  <th>Aksi</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @forelse ($riwayatTransaksiBulanIni as $transaksi)
                                                  <tr>
                                                      <td>{{$transaksi->created_at}}</td>
                                                      <td>{{$transaksi->nama_warung}}</td>
                                                      <td>{{$transaksi->warung_id}}</td>
                                                      <td>{{$transaksi->nama}}</td>
                                                      <td>{{$transaksi->nik}}</td>
                                                      <td>{{GeneralHelper::toRupiah($transaksi->total_harga)}}</td>
                                                      <td>
                                                          <button class="btn btn-primary">Detail</button>
                                                      </td>
                                                  </tr>
                                              @empty
                                                  <tr>
                                                      <td colspan="7">Tidak Ada Data</td>
                                                  </tr>
                                              @endforelse
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                              <div class="panel" id="minggu-ini-panel">
                                  <div class="table-responsive">
                                      <table class="table table-striped table-hover text-center" id="table-riwayat-minggu-ini">
                                          <thead>
                                              <tr>
                                                  <th>Tanggal</th>
                                                  <th>Warung</th>
                                                  <th>ID Warung</th>
                                                  <th>Penerima</th>
                                                  <th>NIK</th>
                                                  <th>Nominal</th>
                                                  <th>Aksi</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @forelse ($riwayatTransaksiMingguIni as $transaksi)
                                                  <tr>
                                                      <td>{{$transaksi->created_at}}</td>
                                                      <td>{{$transaksi->nama_warung}}</td>
                                                      <td>{{$transaksi->warung_id}}</td>
                                                      <td>{{$transaksi->nama}}</td>
                                                      <td>{{$transaksi->nik}}</td>
                                                      <td>{{$transaksi->total_harga}}</td>
                                                      <td>
                                                          <button class="btn btn-primary">Detail</button>
                                                      </td>
                                                  </tr>
                                              @empty
                                                  <tr>
                                                      <td colspan="7">Tidak Ada Data</td>
                                                  </tr>
                                              @endforelse
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                              <div class="panel" id="gelombang-1-panel">
                                  <div class="table-responsive">
                                      <table class="table table-striped table-hover text-center">
                                          <thead>
                                              <tr>
                                                  <th>Tanggal</th>
                                                  <th>Warung</th>
                                                  <th>ID Warung</th>
                                                  <th>Penerima</th>
                                                  <th>NIK</th>
                                                  <th>Nominal</th>
                                                  <th>Aksi</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              <tr>
                                                  <td>15/04/2021</td>
                                                  <td>Warung 01</td>
                                                  <td>M0001</td>
                                                  <td>Ayu</td>
                                                  <td>320123456789</td>
                                                  <td>150.000</td>
                                                  <td>
                                                      <button class="btn btn-primary">Detail</button>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>15/04/2021</td>
                                                  <td>Warung 01</td>
                                                  <td>M0001</td>
                                                  <td>Ayu</td>
                                                  <td>320123456789</td>
                                                  <td>150.000</td>
                                                  <td>
                                                      <button class="btn btn-primary">Detail</button>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>15/04/2021</td>
                                                  <td>Warung 01</td>
                                                  <td>M0001</td>
                                                  <td>Ayu</td>
                                                  <td>320123456789</td>
                                                  <td>150.000</td>
                                                  <td>
                                                      <button class="btn btn-primary">Detail</button>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>15/04/2021</td>
                                                  <td>Warung 01</td>
                                                  <td>M0001</td>
                                                  <td>Ayu</td>
                                                  <td>320123456789</td>
                                                  <td>150.000</td>
                                                  <td>
                                                      <button class="btn btn-primary">Detail</button>
                                                  </td>
                                              </tr>
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                          </div>
                      </div>

                  </div>
              </div>
          </div>
      </div>

      <div class="modal fade" id="modal-produk" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-body">
                      <h4 class="modal-title">Daftar Harga Terendah Dan Tertinggi Per Warung</h4>
                      <div class="info-warung">
                          <div class="row">
                              <div class="col-4">
                                  <p class="mb-1">Nama Produk</p>
                                  <p class="mb-1">Batas Harga</p>
                              </div>
                              <div class="col-8">
                                  <p class="mb-1" id="nama-produk"></p>
                                  <p class="mb-1" id="batas-harga"></p>
                              </div>
                          </div>
                      </div>
                      <div class="table-responsive">
                          <table class="table table-striped table-hover text-center" id="table-harga-perwarung">
                              <thead>
                                  <tr>
                                      <th class="text-center">No</th>
                                      <th class="text-center">Warung</th>
                                      <th class="text-center">Harga Terendah</th>
                                      <th class="text-center">Harga Tertinggi</th>
                                  </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                      </div>
                      <div class="d-flex justify-content-end">
                          <button class="btn btn-sm btn-secondary" data-dismiss="modal">TUTUP</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <div class="col-md-3">
    <div class="card-custom">
        <div class="card-header">
            <h4 class="title mt-4">Jumlah Saldo Transaksi dan Rembes Warung</h4>
        </div>
        <div class="pr-4">
            <div style="overflow: auto; max-height:1000px">
                <div id="progress-bar"></div>
            </div>
        </div>
    </div>
  </div>
@stop

@push('custom-js')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.min.js"></script>

    <script>
        $("#table-deposit-search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#table-deposit tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $("#table-harga-search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#table-harga tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $("#table-riwayat-hari-ini-search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#table-riwayat-hari-ini tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $("#table-riwayat-bulan-ini-search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#table-riwayat-bulan-ini tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $("#table-riwayat-minggu-ini-search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#table-riwayat-minggu-ini tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $('#table-riwayat-hari-ini-search').hide();
        $('#table-riwayat-bulan-ini-search').hide();
        $('#table-riwayat-minggu-ini-search').hide();
        bukaTab('realtime');

        var url = '{{route("bjb.get.data")}}';
        $.ajax({
            method: 'POST',
            url     : url,
            data : {
                _token : '{{csrf_token()}}'
            },
            success : function(res){
                chartDanaRealisasi(res.chartDanaVsRealisasi);

                var saldoWarung     = parseInt(res.saldoWarung ?? 0);
                var saldoPenerima   = parseInt(res.saldoPenerima ?? 0);
                chartDeposit(saldoWarung, saldoPenerima);

                chartDanaBantuan(res.saldoTotal,res.saldoRembes);
                chartPendapatanWarung(res.pendapatanPerwarung.transaksi, res.pendapatanPerwarung.rembes, res.pendapatanPerwarung.namaWarung);
            }
        });

        function chartDanaRealisasi(data){
            var options = {
                series: data,
                chart: {
                    type: 'bar',
                    height: 350
                },
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: '55%',
                        endingShape: 'rounded'
                    },
                },
                dataLabels: {
                    enabled: false
                    },
                    stroke: {
                        show: true,
                        width: 2,
                        colors: ['transparent']
                    },
                    xaxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agust', 'Sep', 'Oct', 'Nov', 'Des'],
                    },
                    fill: {
                        opacity: 1
                    },
                    tooltip: {
                        y: {
                            formatter: function (val) {
                            return formatRupiah(String(val));
                            }
                        }
                    }
                };

                var chart = new ApexCharts(document.querySelector("#dana-vs-realisasi"), options);
                chart.render();
        }

        function chartDeposit(saldoWarung, saldoPenerima){

            var pieDeposit = new Chart($('#pie-deposit'),{
                type: 'doughnut',
                data: {
                    labels: [
                        'Tersalurkan',
                        'Belum Tersalurakan',
                    ],
                    datasets: [{
                        label: 'Saldo',
                        data: [saldoWarung, saldoPenerima],
                        backgroundColor: [
                            'rgb(107, 113, 146)',
                            'rgb(173, 182, 230)'
                        ],
                        hoverOffset: 5
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'bottom',
                        },
                    }
                },
            });
        }

        function chartDanaBantuan(saldoWarung, danaBantuan){
            var pieDanaBLT = new Chart($('#pie-dana-blt'),{
                type: 'doughnut',
                data: {
                    labels: [
                        'Belum Rembes',
                        'Rembes',
                    ],
                    datasets: [{
                        label: 'Realisasi',
                        data: [saldoWarung, danaBantuan],
                        backgroundColor: [
                            'rgb(2,138,222)',
                            'rgb(99,207,255)'
                        ],
                        hoverOffset: 5
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'bottom',
                        },
                    }
                },
            });
        }

        function bukaTab(tab){
            $('.panels').children().css('display', 'none');
            $(`#${tab}-panel`).css("display", "block");

            $('.tabs').children().removeAttr('style');
            $(`#${tab}-tab`).css("color", "#757575");
            $(`#${tab}-tab`).css("font-weight", "bold");
            $(`#${tab}-tab`).css("box-shadow", "0px -3px 3px 0px rgb(0 0 0 / 10%)");

            if (tab == 'realtime') {
                $('#table-riwayat-hari-ini-search').hide();
                $('#table-riwayat-bulan-ini-search').hide();
                $('#table-riwayat-minggu-ini-search').hide();
                $('#table-riwayat-hari-ini-search').show();
            } else if (tab == 'bulan-ini') {
                $('#table-riwayat-hari-ini-search').hide();
                $('#table-riwayat-bulan-ini-search').hide();
                $('#table-riwayat-minggu-ini-search').hide();
                $('#table-riwayat-bulan-ini-search').show();
            } else if (tab == 'minggu-ini') {
                $('#table-riwayat-hari-ini-search').hide();
                $('#table-riwayat-bulan-ini-search').hide();
                $('#table-riwayat-minggu-ini-search').hide();
                $('#table-riwayat-minggu-ini-search').show();
            }
        }

        function formatRupiah(angka, prefix = "Rp. "){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split  = number_string.split(','),
            sisa   = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                var separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        function chartPendapatanWarung(dataTransaksi, dataRembes, namaWarung){
            var options = {
                series: [{
                    name : 'Transaksi',
                    data: dataTransaksi
                    }, {
                        name : 'Rembes',
                    data: dataRembes
                }],
                chart: {
                type: 'bar',
                height: 430
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        dataLabels: {
                        position: 'top',
                        },
                    }
                },
                dataLabels: {
                    enabled: false,
                    offsetX: -6,
                    style: {
                        fontSize: '12px',
                        colors: ['#fff']
                    }
                },
                stroke: {
                    show: true,
                    width: 1,
                    colors: ['#fff']
                },
                tooltip: {
                    y: {
                        formatter: function (val) {
                        return formatRupiah(String(val));
                        }
                    }
                },
                xaxis: {
                    categories: namaWarung,
                },
            };

            var chart = new ApexCharts(document.querySelector("#progress-bar"), options);
            chart.render();
        }

        function hargaProdukPerWarung(id){
            $.ajax({
                url : '{{route("bjb.dashboard.get-produk-berdasarkan-harga", "")}}' + '/' + id,
                method : 'GET',
                success : function(res){
                    $('#nama-produk').text(res.produk.nama);
                    $('#batas-harga').text(formatRupiah(String(res.produk.harga_terendah)) + ' - ' + formatRupiah(String(res.produk.harga_tertinggi)));
                    var data = res.produkPerWarung;
                    var body = '';
                    if(data.length > 0){
                        var i = 1;
                        data.forEach(val => {
                            body += `<tr>
                                        <td>${i++}</td>
                                        <td>${val.nama_warung}</td>
                                        <td>${formatRupiah(String(val.harga_terendah))}</td>
                                        <td>${val.harga_tertinggi}</td>
                                    </tr>`;
                        });
                    }else{
                        body += `<tr>
                                    <td colspan="4">Tidak ada data.</td>
                                </tr>`;
                    }
                    $('#table-harga-perwarung').children('tbody').children().remove();
                    $('#table-harga-perwarung').children('tbody').append(body);
                    $('#modal-produk').modal('show');
                }
            })
        }

    </script>
@endpush

@push('custom-css')
    <style>
        .small-select{
            font-size:12px;
        }

        .btn-warung-lainnya{
            background: #6B7192 !important;
            font-size: 12px;
            padding: 2.5px 10px;
            color: white !important;
        }

        table thead tr th, table tbody tr td{
            margin: auto;
        }
    </style>
@endpush
