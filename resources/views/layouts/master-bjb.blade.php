<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SFT E-Post || @yield('title')</title>

    <!-- Link CSS -->
    @include('layouts.partials.bjb.component-css')

    @stack('custom-css')
</head>
<body style="background: #f3f6f9">
    <div class="row" style="width: 100%">
        <div class="col-xl-12">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-nav" href="#">
                    <div class="angular-sm">
                        <i class="fab fa-angular"></i>
                    </div>
                </a>
                <ul class="navbar-nav ml-auto">
                  <form method="POST" action="{{ route('logout') }}">
                    @csrf
                      <div class="user-circle-sm">
                          <i class="fas fa-power-off mt-2" onclick="event.preventDefault();
                          this.closest('form').submit();"></i>
                      </div>
                  </form>
                </ul>
            </nav>
        </div>
    </div>
    <div class="container-fluid row">
      @include('sweetalert::alert')
      @yield('content')
    </div>

    <br /><br /><br /><br /><br />


    <!-- Link JS -->
    @include('layouts.partials.bjb.component-js')

    @stack('custom-js')
</body>
</html>


