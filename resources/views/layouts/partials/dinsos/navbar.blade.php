<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand @yield('e-post')" href="{{ route('dinsos.dashboard') }}">Dashboard</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-link ml-5 @yield('produk')" href="{{ route('dinsos.produk-view') }}">Produk</a>
      {{-- <a class="nav-link ml-4 @yield('paket')" href="{{ route('dinsos.paket.index') }}">Paket</a> --}}
      <a class="nav-link ml-4 @yield('warung')" href="{{ route('dinsos.warung.index') }}">Warung</a>
      <a class="nav-link ml-4 @yield('gelombang')" href="{{ route('dinsos.gelombang.index') }}">Konfigurasi</a>
      <a class="nav-link ml-4 @yield('penerima-blt')" href="{{ route('dinsos.penerima-blt.index') }}">Penerima BLT</a>
      <a class="nav-link ml-4 @yield('monitoring')" href="{{ route('dinsos.monitoring.index') }}">Monitoring</a>
      <a class="nav-link ml-4 @yield('laporan-laba-rugi')" href="{{ route('dinsos.laporan-laba-rugi') }}">Laporan Laba</a>
    </div>
  </div>
</nav>
