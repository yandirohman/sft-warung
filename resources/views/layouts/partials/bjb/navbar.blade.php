<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand @yield('overview')" href="{{ route('bjb.dashboard') }}">Dashboard</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-link ml-5 @yield('pencairan-rembes')" href="{{ route('bjb.pencairan-rembes') }}">Pencairan Rembes</a>
      <a class="nav-link ml-4 @yield('laporan-rembes')" href="{{ route('bjb.laporan-rembes') }}">Laporan Rembes</a>
    </div>
  </div>
</nav>
