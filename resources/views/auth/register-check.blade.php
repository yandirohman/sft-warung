<x-guest-layout>
  <x-auth-card>
      <x-slot name="logo">
          <a href="/">
              <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
          </a>
      </x-slot>

      <div class="mb-4 text-sm text-gray-600">
          {{ __('Anda Akan Mendaftarkan Warung Anda? Silahkan Masukan Username Anda Untuk Dilakukan Pengecekan Apakah Warung Anda Benar Sudah Terdaftar.') }}
      </div>

      <!-- Session Status -->
      <x-auth-session-status class="mb-4 text-green-600" :status="session('success')" />
      <x-auth-session-status class="mb-4 text-red-600" :status="session('info')" />

      <!-- Validation Errors -->
      <x-auth-validation-errors class="mb-4" :errors="$errors" />

      <form method="POST" action="{{ route('register.check.store') }}">
          @csrf

          <!-- Email Address -->
          <div>
              <x-label for="username" :value="__('Username Warung')" />

              <x-input id="username" class="block mt-1 w-full" type="text" name="username" :value="old('username')" required autofocus />
          </div>

          <div class="flex items-center justify-end mt-4">
              <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                  {{ __('Kembali') }}
              </a>
              <x-button class="ml-3">
                  {{ __('Cek Warung') }}
              </x-button>
          </div>
      </form>
  </x-auth-card>
</x-guest-layout>
