<x-guest-layout>
  <x-auth-card name="card">
    <x-slot name="logo"></x-slot>
    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <!-- Validation Errors -->
    <x-auth-validation-errors class="mb-4" :errors="$errors" />
    <strong style="color: gray; font-size: 24px;">SELAMAT DATANG !</strong>
    <div class="mb-8" style="color: gray; font-size: 11px;"><strong>Silahkan Login Kedalam Sistem</strong></div>
    <center>
      <img src="{{asset('img/login.png')}}" alt="img" class="img_icon mb-6">
    </center>
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <!-- Username Address -->
        <div>
            <x-label style="font-weight: bold; font-size: 12px;" for="username" :value="__('Username')" />

            <x-input style="border-radius: 18px; padding: 20px; height: 15px;" id="username" class="block mt-1 w-full" type="text" name="username" :value="old('username')" required autofocus />
        </div>

        <!-- Password -->
        <div class="mt-4">
            <x-label style="font-weight: bold; font-size: 12px;" for="password" :value="__('Password')" />

            <x-input style="border-radius: 18px; padding: 20px; height: 15px;" id="password" class="block mt-1 w-full"
                            type="password"
                            name="password"
                            required autocomplete="current-password" />
        </div>
        <div class="mt-4">
            <button class="" style="border-radius: 18px; width: 100%; height: 50px; position: relative; overflow: hidden">
                <strong style="color: white; z-index: 9; position: absolute; width: 100%; height: 0px; display: flex; justify-content: center; align-items: center;">{{ __('Log in') }}</strong>
                <img src="{{asset('img/card/lease.png')}}" alt="img" class="img_icon mb-6" width="115" style="position: absolute; width: 100%; top: 0; left: 0">
            </button>
        </div>
    </form>
  </x-auth-card>
</x-guest-layout>
