@extends('warung.layout')

@section('content')
    <div class="profile-head">Profile Saya</div>
    <div class="profile">
        <div class="profile-title">{{ $warung->nama_warung }}</div>
        <div class="profile-text">
            QRCode dibawah merupakan QRCode anda<br/>
            yang bersifat unik dan berbeda dari yang lainnya<br/>
            tunjukan QRCode ini kepada teller bank BJB untuk<br/>
            proses pencairan dana rembes SFT e-post.
        </div>
        <div class="profile-qr-wrap">
            <div class="profile-qr">

                {!! QrCode::size(150)->generate($warung->id); !!}
                
                <div class="profile-qr-tl"></div>
                <div class="profile-qr-tr"></div>
                <div class="profile-qr-br"></div>
                <div class="profile-qr-bl"></div>
            </div>
            <div class="profile-id">ID : {{ $warung->id }}</div>
        </div>
        <div class="profile-info">
            <div class="profile-info-hp">
                <div class="profile-info-wrap">
                    <div class="profile-info-icon">
                        <i class="material-icons">mode_comment</i>
                    </div>
                    <div class="profile-info-title">WhatsApp</div>
                    <div class="profile-info-value">{{ $warung->hp }}</div>
                </div>
            </div>
            <div class="profile-info-name">
                <div class="profile-info-wrap">
                    <div class="profile-info-icon">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="profile-info-title">Pemilik</div>
                    <div class="profile-info-value">{{ $warung->nama_pemilik }}</div>
                </div>
            </div>
            <div class="profile-info-location">
                <div class="profile-info-wrap">
                    <div class="profile-info-icon">
                        <i class="material-icons">location_on</i>
                    </div>
                    <div class="profile-info-title">Alamat</div>
                    <div class="profile-info-value">{{ $warung->alamat }}</div>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center mt-2">
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <button class="btn btn-danger btn-sm shadow" type="button" onclick="event.preventDefault();this.closest('form').submit();this.setAttribute('disabled', 'true')"><b>LOGOUT</b></button>
            </form>
        </div>
    </div>
    @include('warung.component.menu')
@endsection

@push('script')

@endpush

@push('styles')
    <style>
        .profile-head {
            font-size: 20px;
            color: #6b7192;
            font-weight: 600;
            justify-self: start;
            display: flex;
            align-items: center;
            margin-top: 15px;
        }

        .profile {
            width: 100%;
            height: max-content;
            background: #fff;
            margin-top: 15px;
            box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.20);
            padding: 20px 0;
            margin-bottom: 100px;
        }

            .profile-title {
                width: 100%;
                color: #6b7192;
                text-align: center;
                font-size: 20px;
                font-weight: 600;
                margin-bottom: 20px;
            }

            .profile-text {
                width: 100%;
                color: #6b7192;
                text-align: center;
                font-size: 12px;
            }

        .profile-qr-wrap {
            width: 100%;
            height: max-content;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            margin-top: 50px;
            margin-bottom: 50px;
        }

            .profile-qr {
                position: relative;
                width: 150px;
                height: 150px;
            }

                .profile-qr img {
                    width: 100%;
                    height: 100%;
                }

                .profile-qr-tl {
                    position: absolute;
                    width: 30px;
                    height: 30px;
                    top: -20px;
                    left: -20px;
                    border-top: 4px solid #2196f3;
                    border-left: 4px solid #2196f3;
                }

                .profile-qr-tr {
                    position: absolute;
                    width: 30px;
                    height: 30px;
                    top: -20px;
                    right: -20px;
                    border-top: 4px solid #2196f3;
                    border-right: 4px solid #2196f3;
                }

                .profile-qr-br {
                    position: absolute;
                    width: 30px;
                    height: 30px;
                    bottom: -20px;
                    right: -20px;
                    border-bottom: 4px solid #2196f3;
                    border-right: 4px solid #2196f3;
                }

                .profile-qr-bl {
                    position: absolute;
                    width: 30px;
                    height: 30px;
                    bottom: -20px;
                    left: -20px;
                    border-bottom: 4px solid #2196f3;
                    border-left: 4px solid #2196f3;
                }

            .profile-id {
                width: 100%;
                text-align: center;
                color: #6b7192;
                font-size: 12px;
                font-weight: 600;
            }

        .profile-info {
            width: 100%;
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 10px;
            padding: 10px;
        }

            .profile-info-hp {
                width: 100%;
            }

            .profile-info-name {
                width: 100%;
            }

            .profile-info-location {
                grid-column: 1/3;
                width: 100%;
            }

                .profile-info-wrap {
                    display: grid;
                    grid-template-columns: 50px 1fr;
                }

                    .profile-info-icon {
                        grid-row: 1/3;
                        color: #6b7192;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }

                    .profile-info-title {
                        font-size: 16px;
                        font-weight: 600;
                        color: #6b7192;
                    }

                    .profile-info-value {
                        font-size: 12px;
                        color: #6b7192;
                    }

    </style>
@endpush