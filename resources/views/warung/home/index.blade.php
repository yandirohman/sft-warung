@extends('warung.layout')

@section('content')
    <div class="header">
        <div class="header-title">
            <a class="header-title-profile" href="{{ route('warung.profile.index') }}">
                <i class="material-icons">person</i>
            </a>
            <div class="header-title-name">{{ $namaWarung }}</div>
        </div>
        <a class="header-notif" href="{{ route('warung.notif.index') }}">
            <i class="material-icons">notifications</i>
        </a>
    </div>
    <div class="slide">
        <div class="slide-item" style="background: #039be5">
            <div class="slide-item-bg1"></div>
            <div class="slide-item-bg2"></div>
            <div class="slide-item-bg3"></div>
            <img class="slide-item-logo" src="/img/bjb.png" />
            <div class="slide-item-info">
                <div class="slide-item-title">Total pendapatan</div>
                <div class="slide-item-value">Rp. {{ number_format($pendapatanWarung->saldo_total, 0, ',', '.') }}</div>
            </div>
        </div>
        <div class="slide-item" style="background: #43a047">
            <div class="slide-item-bg1"></div>
            <div class="slide-item-bg2"></div>
            <div class="slide-item-bg3"></div>
            <img class="slide-item-logo" src="/img/bjb.png" />
            <div class="slide-item-info">
                <div class="slide-item-title">Telah rembes</div>
                <div class="slide-item-value">Rp. {{ number_format($pendapatanWarung->saldo_rembes, 0, ',', '.') }}</div>
            </div>
        </div>
        <div class="slide-item" style="background: #ef6c00">
            <div class="slide-item-bg1"></div>
            <div class="slide-item-bg2"></div>
            <div class="slide-item-bg3"></div>
            <img class="slide-item-logo" src="/img/bjb.png" />
            <div class="slide-item-info">
                <div class="slide-item-title">Rembes pending</div>
                <div class="slide-item-value">Rp. {{
                    number_format((($pendapatanWarung->saldo_total - $pendapatanWarung->saldo_rembes) ?? 0), 0, ',', '.')
                }}</div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="section-title">Produk Terlaris</div>
        <div class="terlaris-box">
          @foreach ($produk as $list)
            <div class="terlaris-item">
                <div class="terlaris-item-bg1"></div>
                <div class="terlaris-item-bg2"></div>
                <div class="terlaris-item-bg3"></div>
                <img class="terlaris-item-img"/>
                <div class="terlaris-item-wrap">
                    <div class="terlaris-item-logo">
                        <img src="/img/icon/{{ $list->foto_produk }}">
                    </div>
                    <div class="terlaris-item-info">
                        <div class="terlaris-item-info-name">{{ $list->nama }}</div>
                        <div class="terlaris-item-info-satuan">{{ $list->jumlah }} {{ $list->satuan }}</div>
                    </div>
                    <div class="terlaris-item-total">Rp.&nbsp;{{ number_format($list->nominal, 0, ',', '.') }}</div>
                </div>
            </div>
          @endforeach
        </div>
    </div>
    @include('warung.component.menu')
@endsection

@push('script')

@endpush

@push('styles')
    <style>
        .header {
            width: 100%;
            height: 60px;
            display: grid;
            grid-template-columns: 1fr 60px;
        }

            .header-title {
                justify-self: start;
                display: flex;
                align-items: center;
            }

                .header-title-profile {
                    width: 40px;
                    height: 40px;
                    background: #6b7192;
                    border-radius: 20px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }

                .header-title-profile:active,
                .header-title-profile:hover {
                    text-decoration: none;
                }

                    .header-title-profile i {
                        color: #fff;
                    }

                .header-title-name {
                    padding-left: 10px;
                    color: #6b7192;
                    font-weight: 600;
                    font-size: 18px;
                }

            .header-notif {
                justify-self: end;
                display: flex;
                align-items: center;
                justify-content: center;
                text-decoration: none;
                color: #6b7192;
                outline: none;
                width: 40px;
                height: 40px;
                align-self: center;
            }

            .header-notif:hover,
            .header-notif:active {
                text-decoration: none;
                color: #6b7192;
                outline: none;
                background: #ddd;
            }

                .header-notif i {
                    color: #6b7192;
                }

        .slide {
            width: calc(100% + 10px);
            margin-left: -10px;
            overflow-x: scroll;
            overflow-y: hidden;
            white-space: nowrap;
            margin-top: 20px;
            padding-left: 10px;
        }

            .slide-item {
                position: relative;
                width: 270px;
                height: 150px;
                margin-right: 10px;
                display: inline-block;
                border-radius: 10px;
                box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.20);
                overflow: hidden;
            }

                .slide-item-bg1 {
                    position: absolute;
                    width: 200px;
                    height: 200px;
                    background: rgba(255,255,255,0.2);
                    border-radius: 200px;
                    top: -20px;
                    left: 40px;
                }

                .slide-item-bg2 {
                    position: absolute;
                    width: 150px;
                    height: 150px;
                    background: rgba(255,255,255,0.2);
                    border-radius: 200px;
                    top: -40px;
                    left: -20px;
                }

                .slide-item-bg3 {
                    position: absolute;
                    width: 150px;
                    height: 150px;
                    background: rgba(255,255,255,0.2);
                    border-radius: 200px;
                    top: 50px;
                    left: -20px;
                }

                .slide-item-logo {
                    position: absolute;
                    width: 50px;
                    top: 20px;
                    left: 20px;
                }

                .slide-item-info {
                    position: absolute;
                    width: 100%;
                    height: max-content;
                    bottom: 0;
                    left: 0;
                    padding-left: 20px;
                }

                    .slide-item-title {
                        font-size: 16px;
                        color: #fff;
                    }

                    .slide-item-value {
                        font-size: 25px;
                        font-weight: 600;
                        color: #fff;
                        margin-bottom: 10px;
                    }

        .section {
            width: 100%;
            height: max-content;
            margin-top: 20px;
            margin-bottom: 100px;
        }

            .section-title {
                width: 100%;
                height: 40px;
                color: #6b7192;
                font-size: 18px;
                font-weight: 600;
            }

        .terlaris-box {
            width: 100%;
        }

            .terlaris-item {
                position: relative;
                width: 100%;
                height: 70px;
                background: #fff;
                border-radius: 10px;
                box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.20);
                overflow: hidden;
                position: relative;
                margin-bottom: 15px;
            }

                .terlaris-item-img {
                    width: 100%;
                }

                .terlaris-item-wrap {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    display: grid;
                    grid-template-columns: 80px 1fr 1fr;
                    z-index: 1;
                    font-weight: 600;
                }

                    .terlaris-item-logo {
                        width: 100%;
                        height: 100%;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }

                        .terlaris-item-logo img {
                            width: 40px;
                        }

                    .terlaris-item-info {
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                    }

                        .terlaris-item-info-name {
                            font-size: 14px;
                            color: #6b7192;
                        }

                        .terlaris-item-info-satuan {
                            font-size: 14px;
                            color: #6b7192;
                        }

                    .terlaris-item-total {
                        justify-self: end;
                        padding-right: 20px;
                        color: #6b7192;
                        display: flex;
                        align-items: center;
                        font-size: 14px;
                    }

    </style>
@endpush
