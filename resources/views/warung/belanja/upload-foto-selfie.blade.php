@extends('warung.layout')

@section('content')
    <div class="container" style="color: #555 !important">
      <h5 class="font-weight-bold">Silahkan Upload Bukti Foto</h5>
      <form action="{{ route('warung.belanja.upload-foto-selfie.store') }}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{Request::get('id')}}">
        <x-input-foto id="file" name="base64" label="Foto" imageId="image"/>
      </form>
    </div>
    @include('warung.component.menu')
@endsection
@push('styles')
    <style>
      .container {
        margin: 50px 20px 0 0;
        max-width: 100%;
      }
    </style>
@endpush
