@extends('warung.layout')

@section('content')
    <div class="modal fade" id="manual" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="modal-body" method="GET" action="{{ route('warung.belanja.verifying') }}">
                    <div class="manual-title">Cari ID</div>
                    <input class="form-control mt-2" type="text" placeholder="Masukan ID Penerima" name="code">
                    <div class="d-flex justify-content-end mt-4">
                        <a class="btn btn-sm shadow" data-dismiss="modal">BATAL</a>
                        <button class="btn btn-sm shadow ml-3" style="background: #6b7192; color: #fff">CARI</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- <div class="belanja-head">Belanja</div> --}}
    <div class="belanja">
        <div class="belanja-title">Scan QRCode</div>
        <div class="belanja-text">
            Arahkan kamera ke QRCode yang dibawa<br/>
            oleh penerima BLT.
        </div>
        <div class="belanja-qr-wrap">
            <div class="belanja-qr">
                <img src="/sample/qr.png" />
                <div class="belanja-qr-tl"></div>
                <div class="belanja-qr-tr"></div>
                <div class="belanja-qr-br"></div>
                <div class="belanja-qr-bl"></div>
            </div>
        </div>
        <div class="d-flex justify-content-center mt-2">
            <a
                class="btn shadow btn-custom"
                style="background: #6b7192; color: #fff"
                onclick="android.doScan()"
            >
                <b>SCAN</b>
            </a>
        </div>
        <div class="text-center mt-3" style="color: #aaa; font-size: 10px">- atau -</div>
        <div class="d-flex justify-content-center mt-2">
            <a
                style="background: #fff; color: #fff; font-size: 14px"
                data-toggle="modal"
                data-target="#manual"
            >
                <b style="color: #6b7192">CARI ID MANUAL</b>
            </a>
        </div>
    </div>

    @include('warung.component.menu')
@endsection

@push('script')
</script>
@endpush

@push('styles')
    <style>
        .belanja-head {
            font-size: 20px;
            color: #6b7192;
            font-weight: 600;
            justify-self: start;
            display: flex;
            align-items: center;
            margin-top: 15px;
        }

        .belanja {
            width: 100%;
            height: calc(100vh - 90px);
            background: #fff;
            margin-top: 15px;
            box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.20);
            padding: 20px 0 30px 0;
        }

            .belanja-title {
                width: 100%;
                color: #6b7192;
                text-align: center;
                font-size: 20px;
                font-weight: 600;
                margin-bottom: 20px;
            }

            .belanja-text {
                width: 100%;
                color: #6b7192;
                text-align: center;
                font-size: 12px;
            }

        .belanja-qr-wrap {
            width: 100%;
            height: max-content;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            margin-top: 50px;
            margin-bottom: 50px;
        }

            .belanja-qr {
                position: relative;
                width: 150px;
                height: 150px;
            }

                .belanja-qr img {
                    width: 100%;
                    height: 100%;
                }

                .belanja-qr-tl {
                    position: absolute;
                    width: 30px;
                    height: 30px;
                    top: -5px;
                    left: -5px;
                    border-top: 4px solid #6b7192;
                    border-left: 4px solid #6b7192;
                }

                .belanja-qr-tr {
                    position: absolute;
                    width: 30px;
                    height: 30px;
                    top: -5px;
                    right: -5px;
                    border-top: 4px solid #6b7192;
                    border-right: 4px solid #6b7192;
                }

                .belanja-qr-br {
                    position: absolute;
                    width: 30px;
                    height: 30px;
                    bottom: -5px;
                    right: -5px;
                    border-bottom: 4px solid #6b7192;
                    border-right: 4px solid #6b7192;
                }

                .belanja-qr-bl {
                    position: absolute;
                    width: 30px;
                    height: 30px;
                    bottom: -5px;
                    left: -5px;
                    border-bottom: 4px solid #6b7192;
                    border-left: 4px solid #6b7192;
                }

        .manual-title {
            width: 100%;
            color: #6b7192;
            font-weight: 600;
        }

        .btn-custom {
            width: 160px;
            border-radius: 20px;
        }

    </style>
@endpush
