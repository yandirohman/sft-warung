@extends('warung.layout')

@section('content')
    <div class="b-verified">
        <a href="{{ route('warung.belanja.index') }}" class="b-verified-back">
            <i class="material-icons">arrow_back</i>
        </a>
        <div class="b-verified-title text-success">
            <span>QR Code Terverifikasi!</span>&nbsp;
            <i class="material-icons">done</i>
        </div>
        <div class="b-verified-qr-wrap">
            <div class="b-verified-qr">
                {!! QrCode::size(150)->generate($data->id) !!}
            </div>
        </div>
        <div class="b-verified-info">
            <div class="b-verified-info-title">SALDO :</div>
            <div class="b-verified-info-saldo">Rp. {{ number_format($data->saldo, 0, ",", ".") }},-</div>
            <div class="b-verified-info-table mt-2">
                <table>
                    <tr>
                        <td>Nama</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                        <td>{{ $data->nama }}</td>
                    </tr>
                    <tr>
                        <td>NIK</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                        <td>{{ $data->nik }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="b-verified-table mt-4 mb-4">
            <div class="b-verified-table-title">Riwayat Transaksi</div>
            <div class="table-responsive">
                <table class="table table-sm table-striped text-center">
                    <thead>
                        <tr> 
                            <th>Tanggal</th>
                            <th>Bln</th>
                            <th>Total</th>
                            <th style="text-align: center; padding-right: 15px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($riwayatTransaksi as $transaksi)
                            <tr>
                                <td>{{ date('d/m/Y', strtotime($transaksi->created_at)) }}</td>
                                <td>{{ $transaksi->gelombang }}</td>
                                <td>Rp {{ number_format($transaksi->total_harga, 0,',','.') }}</td>
                                <td style="text-align: center; padding-right: 15px">
                                    <button class="b-verified-detail btn shadow" onclick="showDetail('{{$transaksi->id}}', '{{$transaksi->nama}}', '{{date('d/m/Y', strtotime($transaksi->created_at))}}', '{{$transaksi->total_harga}}')">Detail</button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4"> Belum Ada Riwayat Transaksi</td></td>    
                            </tr>    
                        @endforelse
                    </tbody>
                </table>
            </div>
            {{ $riwayatTransaksi->links() }}
        </div>
        <div class="b-verified-action">
            <a class="btn shadow b-verified-action-scan" style="color: #6b7192" href="{{ route('warung.belanja.index') }}">
                <i class="material-icons">photo_camera</i>&nbsp;
                <b>Scan Ulang</b>
            </a>
            <a 
                class="btn shadow b-verified-action-next" 
                href="{{ 
                    route('warung.belanja.beli.index', [
                        'p' => $data->id,
                        'g' => $data->gelombang
                    ]) 
                }}" 
                style="background: #6b7192; color: #fff"
            >
                Lanjut Belanja
            </a>
        </div>
    </div>

    <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="detail">
                        <div class="detail-order">
                            <div class="detail-order-qr">
                                <canvas id="qr-code"></canvas>
                            </div>
                            <div class="detail-order-title">Order ID</div>
                            <div class="detail-order-value" id="id-transaksi">#1</div>
                        </div>
                        <div class="detail-date">
                            <div class="detail-date-title">Tanggal</div>
                            <div class="detail-date-value" id="tanggal-transaksi">01 Januari 2021</div>
                        </div>
                        <div class="detail-person">
                            <div class="detail-person-photo">
                                <img id="foto-transaksi" src="/img/sample.jpg" />
                            </div>
                            <div class="detail-person-wrap">
                                <div class="detail-person-title">Penerima BLT</div>
                                <div class="detail-person-name" id="nama-penerima">Bambang</div>
                                <div class="detail-person-nik" id="nik-penerima">3203576587787</div>
                            </div>
                        </div>
                        <div class="detail-warung">
                            <div class="detail-warung-title">Warung BLT</div>
                            <div class="detail-warung-name" id="nama-warung">Warung Barokah 1</div>
                            <div class="detail-warung-nik" id="id-warung">ID-1</div>
                        </div>
                        <div class="detail-trx">
                            <table class="table table-sm table-striped" style="font-size: 12px; width: 100%;" id="table-produk">
                                <thead>
                                    <tr style="background: #6b7192; color: #fff">
                                        <th>Nama</th>
                                        <th>Harga</th>
                                        <th>Jml</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-sm btn-secondary" data-dismiss="modal">TUTUP</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('warung.component.menu')
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/qrious/4.0.2/qrious.min.js"></script>

    <script>
        function formatRupiah(angka, prefix = "Rp. "){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split  = number_string.split(','),
            sisa   = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                var separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        var qr;

        qr = new QRious({
                element: document.getElementById('qr-code'),
                size: 200,
                value: 'sft-epost'
            });
            
        function generateQRCode(text) {
            qr.set({
                foreground: 'black',
                size: 50,
                value: text
            });
        }
        function showDetail(id){
            url = '{{route("warung.belanja.detail-transaksi", "")}}' + '/' + id;

            $.ajax({
                url : url,
                method : 'get',
                success : function(res){
                    if(res.length > 0){
                        
                        generateQRCode(String(res[0].id));

                        var tanggal = new Date(res[0].created_at);
                            tanggal = ("0" + tanggal.getDate()).slice(-2) + '/' + ("0" + (tanggal.getMonth() + 1)).slice(-2) + '/' + tanggal.getFullYear();
                        $('#id-transaksi').text(res[0].id);
                        $('#tanggal-transaksi').text(tanggal);
                        $('#nama-warung').text(res[0].nama_warung);
                        $('#id-warung').text(res[0].warung_id);
                        $('#nama-penerima').text(res[0].nama);
                        $('#nik-penerima').text(res[0].nik);
                        $('#foto-transaksi').attr('src', res[0].foto_transaksi);

                        var body = '';
                        res.forEach(value => {
                            body += `<tr>
                                        <td>${value.nama_produk}</td>
                                        <td>${formatRupiah(String(value.harga_satuan))}</td>
                                        <td>${value.jumlah_produk}</td>
                                        <td>${formatRupiah(String(value.sub_total))}</td>
                                    </tr>`;
                        });

                        body += `<tr style="background: #6b7192; color: #fff">
                                    <td colspan="3">
                                        <b>TOTAL HARGA KESELURUHAN</b>
                                    </td>
                                    <td>
                                        <b>${formatRupiah(String(res[0].total_harga))}</b>
                                    </td>
                                </tr>`;
                        $('#table-produk').children('tbody').children().remove();
                        $('#table-produk').children('tbody').append(body);
                        $('#detail').modal('show');
                    }
                }
            });
        }
    </script>
@endpush

@push('styles')
    <style>
        .b-verified {
            position: relative;
            width: 100%;
            height: max-content;
            background: #fff;
            margin-top: 15px;
            box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.20);
            padding: 50px 0 30px 0;
            margin-bottom: 100px;
        }

            .b-verified-back {
                position: absolute;
                width: 50px;
                height: 50px;
                display: flex;
                align-items: center;
                justify-content: center;
                color: #6b7192;
                top: 0;
                left: 0;
                text-decoration: none;
                outline: none;
            }

            .b-verified-back:active {
                text-decoration: none;
                outline: none;
                color: #6b7192;
            }

            .b-verified-back:active {
                background: #eee;
            }

                .b-verified-back i {
                    font-size: 30px;
                }

            .b-verified-title {
                width: 100%;
                text-align: center;
                font-size: 16px;
                font-weight: 600;
                margin-bottom: 20px;
                display: flex;
                align-items: center;
                justify-content: center;
            }

        .b-verified-qr-wrap {
            width: 100%;
            height: max-content;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            margin-bottom: 20px;
        }

            .b-verified-qr {
                position: relative;
                width: 150px;
                height: 150px;
                border: 4px solid #4caf50;
                padding: 10px;
                box-sizing: content-box;
            }

                .b-verified-qr img {
                    width: 100%;
                    height: 100%;
                }

        .b-verified-info {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            color: #6b7192;
        }

            .b-verified-info-title {
                font-size: 12px;
                font-weight: 600;
            }

            .b-verified-info-saldo {
                margin-top: -5px;
                font-size: 34px;
                font-weight: 600;
                /* font-family: 'Courier New', Courier, monospace; */
            }

            .b-verified-info-table {
                font-size: 15px;
            }

        .b-verified-table {
            width: 100%;
            margin-top: 10px;
            padding: 0px 2rem;
        }

            .b-verified-table-title {
                width: 100%;
                font-size: 17px;
                font-weight: 600;
                color: #6b7192;
                margin-bottom: 10px;
                padding-left: 15px;
            }

            .table {
                font-size: 12px;
                white-space: nowrap;
            }

                .table th, .table td{
                    vertical-align: middle ;
                } 

                .b-verified-detail {
                    width: max-content;
                    height: max-content;
                    padding: 2px 10px;
                    background: #6b7192;
                    border-radius: 100px;
                    text-decoration: none;
                    outline: none;
                    color: #fff;
                    font-size: 12px;
                } 

                .b-verified-detail:active {
                    text-decoration: none;
                    color: #fff;
                }

        .b-verified-action {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 0 20px;
        }

            .b-verified-action-scan {
                width: 100%;
                display: flex;
                justify-content: center;
                margin-bottom: 10px;
                border-radius: 100px;
                padding-left: 20px;
                padding-right: 20px;
            }

            .b-verified-action-scan:active {
                background: #eee;
            }

            .b-verified-action-next {
                width: 100%;
                margin-bottom: 10px;
                border-radius: 100px;
                padding-left: 20px;
                padding-right: 20px;
            }

        .modal .modal-header,.modal .modal-body, .modal .modal-footer{
            border: none;
        }

        .text-small{
            font-size:12px;
        }

        .detail {
            width: 100%;
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 10px;
        }

            .detail-order {
                justify-self: start;
                display: grid;
                grid-template-columns: 50px 1fr;
                font-size: 12px;
            }

                .detail-order-qr {
                    grid-row: 1/3;
                    display: flex;
                    align-items: center;
                }

                .detail-order-title {
                    width: 100%;
                    font-weight: 600;
                }

                .detail-order-value {
                    width: 100%;
                }

            .detail-date {
                font-size: 12px;
            }

                .detail-date-title {
                    width: 100%;
                    font-weight: 600;
                    text-align: end;
                }

                .detail-date-value {
                    width: 100%;
                    text-align: end;
                }

            .detail-person {
                justify-self: start;
                display: grid;
                grid-template-columns: 80px 1fr;
                font-size: 12px;
            }

                .detail-person-photo {
                    width: 100%;
                }

                    .detail-person-photo img {
                        width: 70px;
                    }

                .detail-person-wrap {
                    display: flex;
                    display: flex;
                    justify-content: flex-start;
                    flex-direction: column;
                }

                    .detail-person-title {
                        width: 100%;
                        font-weight: 600;
                        color: #6b7192;
                    }

                    .detail-person-name {
                        width: 100%;
                        font-weight: 600;
                    }

                    .detail-person-nik {
                        width: 100%;
                        font-weight: 600;
                    }


            .detail-warung {
                width: 100%;
                font-size: 12px;
            }

                .detail-warung-title {
                    width: 100%;
                    font-weight: 600;
                    color: #6b7192;
                    text-align: end;
                }

                .detail-warung-name {
                    width: 100%;
                    font-weight: 600;
                    text-align: end;
                }

                .detail-warung-nik {
                    width: 100%;
                    font-weight: 600;
                    text-align: end;
                }

            .detail-trx {
                grid-column: 1/3;
            }

    </style>
@endpush