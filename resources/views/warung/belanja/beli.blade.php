@extends('warung.layout')

@section('content')
    <div class="header mb-3">
        <a href="{{ 
            route('warung.belanja.verifying', [
                'code' => $gelombang.$penerimaId
            ]) 
        }}" class="header-back">
            <i class="material-icons">arrow_back</i>
        </a>
        <div class="header-title">Pilih jenis belanja</div>
    </div>
    <div class="paket">
        @foreach($pakets as $paket)
            <div class="paket-item-wrap">
                <a class="paket-item {{ $paket->id == $idPaket ? 'paket-item-active' : '' }}" href="{{ route('warung.belanja.beli.index', [
                    'idPaket' => $paket->id,
                    'p' => $penerimaId,
                    'g' => $gelombang
                ]) }}">{{ $paket->nama }}</a>
            </div>
        @endforeach
    </div>
    <div class="card">
        <div class="card-body">
            <div class="card-top-title mb-2">{{ $paketData->nama }}</div>
            <table style="width: 100%; font-size: 12px;" class="table table-sm">
                <tr>
                    <th>Barang</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                    <th>Total</th>
                </tr>
                @foreach($paketData->list_produk as $item)
                    <tr>
                        <td>{{ $item->nama }}</td>
                        <td valign="middle">{{ $item->jumlah }}</td>
                        <td valign="middle">{{ $item->satuan }}</td>
                        <td valign="middle">Rp.&nbsp;{{ number_format($item->harga, 0, ',', '.') }}</td>
                        <td valign="middle">Rp.&nbsp;{{ number_format($item->sub_total, 0, ',', '.') }}</td>
                    </tr>
                @endforeach
                <tr>
                    <th colspan="4">TOTAL</th>
                    <th>Rp.&nbsp;{{ number_format($paketData->total_harga, 0, ',', '.') }}</th>
                </tr>
            </table>
            <form 
                class="d-flex justify-content-center" 
                action="{{ 
                    route('warung.belanja.beli.paket', [
                        'idPaket' => $idPaket
                    ]) 
                }}"
                method="POST"    
            >
                @csrf
                <input type="hidden" name="penerima_id" value="{{ $penerimaId }}">
                <input type="hidden" name="gelombang" value="{{ $gelombang }}">
                <input type="hidden" name="sumber" value="pencairan_paket">
                <button 
                    class="btn shadow mt-5 mb-3" 
                    style="background: #6b7192; color: #fff"
                >
                    <b>LANJUTKAN</b>
                </button>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
@endpush

@push('styles')
    <style>
        .header {
            position: relative;
            width: 100%;
            height: 50px;
            display: grid;
            grid-template-columns: 50px 1fr;
        }

            .header-back {
                width: 50px;
                height: 50px;
                display: flex;
                align-items: center;
                justify-content: center;
                color: #6b7192;
                top: 0;
                left: 0;
                text-decoration: none;
                outline: none;
            }

            .header-back:active,
            .header-back:hover {
                text-decoration: none;
                color: #6b7192;
                background-color: #ddd;
            }

            .header-title {
                width: 100%;
                height: 50px;
                display: flex;
                align-items: center;
                font-size: 20px;
                font-weight: 600;
                color: #6b7192;
            }
        
        .paket {
            width: 100%;
            white-space: nowrap;
            overflow-x: scroll;
            padding-left: 5px;
        }

            .paket-item-wrap {
                display: inline-block;
                margin: 5px 5px 5px 0;
            }

                .paket-item {
                    width: 100%;
                    height: max-content;
                    background: #fff;
                    color: #6b7192;
                    box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                    border-radius: 8px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    font-size: 16px;
                    padding: 5px 15px;
                }

                .paket-item-active {
                    background: #6b7192;
                    color: #fff;
                }

                .paket-item:active,
                .paket-item:hover {
                    text-decoration: none;
                    color: #fff;
                }

        .card {
            box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
            border: 0 !important;
            border-radius: 8px;
            margin-top: 10px;
        }

            .card-body {
                padding: 15px !important;
            }

                .card-top-title {
                    width: 100%;
                    display: flex;
                    align-items: center;
                    font-size: 22px;
                    font-weight: 600;
                    color: #6b7192;
                }

                .custom-input {
                    width: max-content;
                    display: flex;
                    align-items: center;
                }

                    .custom-input-dec,
                    .custom-input-inc {
                        width: 20px;
                        height: 20px;
                        background: #6b7192;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        font-size: 15px;
                        font-weight: 600;
                        color: #fff;
                        border-radius: 20px;
                        user-select: none;
                        cursor: pointer;
                    }

                    .custom-input-value {
                        width: 30px;
                        border: 1px solid #ccc;
                        height: 30px;
                        border-radius: 7px;
                        margin: 5px 3px;
                        text-align: center;
                        outline: none;
                    }

                    .custom-input-value:focus {
                        border: 1px solid #000;
                    }
    </style>
@endpush