@extends('warung.layout')

@section('content')
    <div class="b-verified">
        <a href="{{ route('warung.belanja.index') }}" class="b-verified-back">
            <i class="material-icons">arrow_back</i>
        </a>
        <div class="b-verified-title text-danger">
            <span>QRCode tidak ditemukan</span>&nbsp;
            <i class="material-icons">clear</i>
        </div>
        <div class="b-verified-qr-wrap">
            <div class="b-verified-qr">
                {!! QrCode::size(150)->generate($code) !!}
            </div>
        </div>
        <div class="b-verified-info mb-5">
            <span style="font-size: 23px">:(</span>
        </div>

        <div class="b-verified-action">
            <a class="btn shadow b-verified-action-scan" style="background: #6b7192; color: #fff" href="{{ route('warung.belanja.index') }}">
                <i class="material-icons">photo_camera</i>&nbsp;
                <b>Scan Ulang</b>
            </a>
        </div>
    </div>
    @include('warung.component.menu')
@endsection

@push('script')

@endpush

@push('styles')
    <style>
        .b-verified {
            position: relative;
            width: 100%;
            height: max-content;
            background: #fff;
            margin-top: 15px;
            box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.20);
            padding: 50px 0 30px 0;
            margin-bottom: 100px;
        }

            .b-verified-back {
                position: absolute;
                width: 50px;
                height: 50px;
                display: flex;
                align-items: center;
                justify-content: center;
                color: #6b7192;
                top: 0;
                left: 0;
                text-decoration: none;
                outline: none;
            }

            .b-verified-back:active {
                text-decoration: none;
                outline: none;
                color: #6b7192;
            }

            .b-verified-back:active {
                background: #eee;
            }

                .b-verified-back i {
                    font-size: 30px;
                }

            .b-verified-title {
                width: 100%;
                text-align: center;
                font-size: 15px;
                font-weight: 600;
                margin-bottom: 20px;
                display: flex;
                align-items: center;
                justify-content: center;
            }

        .b-verified-qr-wrap {
            width: 100%;
            height: max-content;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            margin-bottom: 20px;
        }

            .b-verified-qr {
                position: relative;
                width: 150px;
                height: 150px;
                border: 4px solid #f44336;
                padding: 5px;
                box-sizing: content-box;
            }

                .b-verified-qr img {
                    width: 100%;
                    height: 100%;
                }

        .b-verified-info {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            color: #6b7192;
        }

            .b-verified-info-title {
                font-size: 15px;
                font-weight: 600;
            }

            .b-verified-info-saldo {
                font-size: 25px;
                font-weight: 600;
                font-family: 'Courier New', Courier, monospace;
            }

            .b-verified-info-table {
                font-size: 15px;
            }

        .b-verified-table {
            width: 100%;
            margin-top: 10px;
        }

            .b-verified-table-title {
                width: 100%;
                font-size: 17px;
                font-weight: 600;
                color: #6b7192;
                margin-bottom: 10px;
                padding-left: 15px;
            }

            .table {
                font-size: 12px;
            }

                .b-verified-detail {
                    width: max-content;
                    height: max-content;
                    padding: 2px 10px;
                    background: #6b7192;
                    border-radius: 100px;
                    text-decoration: none;
                    outline: none;
                    color: #fff;
                } 

                .b-verified-detail:active {
                    text-decoration: none;
                    color: #fff;
                }

        .b-verified-action {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 0 20px;
        }

            .b-verified-action-scan {
                width: 100%;
                display: flex;
                justify-content: center;
                margin-bottom: 10px;
                border-radius: 100px;
                padding-left: 20px;
                padding-right: 20px;
            }

            .b-verified-action-scan:active {
                background: #eee;
            }

            .b-verified-action-next {
                width: 100%;
                margin-bottom: 10px;
                border-radius: 100px;
                padding-left: 20px;
                padding-right: 20px;
            }
    </style>
@endpush