@extends('warung.layout')

@section('content')
    <div class="header mb-3">
        <a href="{{ 
            route('warung.belanja.beli.index', [
                'p' => $request->penerima_id,
                'g' => $request->gelombang
            ]) 
        }}" class="header-back">
            <i class="material-icons">arrow_back</i>
        </a>
        <div class="header-title">Total belanja</div>
    </div>
    <div class="card">
        <div class="card-body">
            <h5 style="color: #6b7192; text-align: center" class="mb-3">Checkout</h5>
            <table class="table table-sm table-striped" style="font-size: 12px">
                <thead>
                    <tr style="background: #6b7192; color: #fff">
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Jml</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($produks as $produk)
                        <tr>
                            <td>{{ $produk->nama }}</td>
                            <td>Rp. {{ number_format($produk->harga, 0, ',', '.') }}</td>
                            <td>{{ $produk->jumlah }}</td>
                            <td>Rp. {{ number_format(($produk->harga * $produk->jumlah), 0, ',', '.') }}</td>
                        </tr>
                    @endforeach
                    <tr style="background: #6b7192; color: #fff">
                        <td colspan="3">
                            <b>TOTAL HARGA KESELURUHAN</b>
                        </td>
                        <td>
                            <b>Rp. {{ number_format($paket->total_harga, 0, ',', '.') }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <b>SISA SALDO</b>
                        </td>
                        <td>
                            <b 
                                class="{{ ($dompet->saldo < $paket->total_harga) ? 'text-danger' : 'text-success' }}">
                                Rp. {{ number_format(($dompet->saldo - $paket->total_harga), 0, ',', '.') }}
                            </b>
                        </td>
                    </tr>
                </tbody>
            </table>
            @if($dompet->saldo < $paket->total_harga)
                <div class="d-flex justify-content-center text-center text-danger">Maaf, saldo penerima tidak mencukupi untuk transaksi ini.</div>
            @else
                <div class="d-flex justify-content-center">
                    <a
                        onclick="lanjutkan()" 
                        class="btn shadow mt-2 mb-3" 
                        style="background: #6b7192; color: #fff"
                    >
                        <b>LANJUTKAN</b>
                    </a>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
<script>
    function lanjutkan() {
        Swal.fire({
            title: "Perhatian!!!",
            text: "jika anda menekan lanjutkan, transaksi tidak dapat diubah lagi atau dibatalkan dan transaksi akan langsung tercatat",
            icon: "warning",
            showCancelButton: true,
            cancelButtonText: 'KEMBALI',
            confirmButtonText: 'TETAP LANJUTKAN',
            dangerMode: true
        }).then(function(isConfirm) {
            if (isConfirm.isConfirmed) {
                window.location = '{{ route('warung.belanja.checkout.trx') }}'
            }
        })
    }
</script>
@endpush

@push('styles')
    <style>
        .header {
            position: relative;
            width: 100%;
            height: 50px;
            display: grid;
            grid-template-columns: 50px 1fr;
        }

            .header-back {
                width: 50px;
                height: 50px;
                display: flex;
                align-items: center;
                justify-content: center;
                color: #6b7192;
                top: 0;
                left: 0;
                text-decoration: none;
                outline: none;
            }

            .header-back:active,
            .header-back:hover {
                text-decoration: none;
                color: #6b7192;
                background-color: #ddd;
            }

            .header-title {
                width: 100%;
                height: 50px;
                display: flex;
                align-items: center;
                font-size: 20px;
                font-weight: 600;
                color: #6b7192;
            }

        .card {
            box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
            border: 0 !important;
            border-radius: 8px;
            margin-top: 10px;
        }
    </style>
@endpush