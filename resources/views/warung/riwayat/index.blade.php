@extends('warung.layout')

@section('content')

    <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="detail">
                        <div class="detail-order">
                            <div class="detail-order-qr">
                                <canvas id="qr-code"></canvas>
                            </div>
                            <div class="detail-order-title">Order ID</div>
                            <div class="detail-order-value" id="id-transaksi">#1</div>
                        </div>
                        <div class="detail-date">
                            <div class="detail-date-title">Tanggal</div>
                            <div class="detail-date-value" id="tanggal-transaksi">01 Januari 2021</div>
                        </div>
                        <div class="detail-person">
                            <div class="detail-person-photo">
                                <img id="foto-transaksi" src="/img/sample.jpg" />
                            </div>
                            <div class="detail-person-wrap">
                                <div class="detail-person-title">Penerima BLT</div>
                                <div class="detail-person-name" id="nama-penerima">Bambang</div>
                                <div class="detail-person-nik" id="nik-penerima">3203576587787</div>
                            </div>
                        </div>
                        <div class="detail-warung">
                            <div class="detail-warung-title">Warung BLT</div>
                            <div class="detail-warung-name" id="nama-warung">Warung Barokah 1</div>
                            <div class="detail-warung-nik" id="id-warung">ID-1</div>
                        </div>
                        <div class="detail-trx">
                            <table class="table table-sm table-striped" style="font-size: 12px; width: 100%;" id="table-produk">
                                <thead>
                                    <tr style="background: #6b7192; color: #fff">
                                        <th>Nama</th>
                                        <th>Harga</th>
                                        <th>Jml</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-sm btn-secondary" data-dismiss="modal">TUTUP</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    {{-- <div class="filter-title">Filter berdasarkan paket</div>
                    <div class="filter-paket">
                        <div class="filter-paket-item">Semua</div>
                        <div class="filter-paket-item filter-paket-active">Paket 1</div>
                        <div class="filter-paket-item">Paket 2</div>
                        <div class="filter-paket-item">Paket 3</div>
                        <div class="filter-paket-item">Custom</div>
                    </div> --}}
                    <form id="formFilter">
                      <div class="filter-title mt-2 mb-2">Filter berdasarkan tanggal</div>
                      <input class="form-control" name="daterange" value="{{ date('m-d-Y') }} - {{ date('m-d-Y') }}" />
                      <div class="d-flex justify-content-end mt-4">
                        <button class="btn btn-sm shadow" data-dismiss="modal">BATAL</button>
                        <button class="btn btn-sm shadow ml-3" style="background: #6b7192; color: #fff" onclick="submit()">FILTER</button>
                      </div>
                    </form>
                  </div>
            </div>
        </div>
    </div>

    <div class="header">
        <img class="header-img" src="/img/city.png" />
        <div class="header-title">Riwayat</div>
        <div class="header-search">
            <div class="header-search-wrap">
                <input class="header-search-input" type="text" placeholder="Cari Nama">
                <i class="material-icons header-search-btn">search</i>
            </div>
        </div>
    </div>
    <div class="dompet">
        <div class="dompet-title">Saldo Saat Ini</div>
        <div class="dompet-value">Rp. {{ number_format($saldo, 0, ',', '.') }}</div>
    </div>
    <div class="riwayat">
        <div class="riwayat-head">
            <div class="riwayat-head-title">Riwayat Transaksi</div>
            <div class="riwayat-head-filter" data-toggle="modal" data-target="#filter">
                <i class="material-icons">filter_alt</i>
            </div>
        </div>
        <table class="table table-sm table-striped">
            <thead>
                <tr>
                    <th style="padding-left: 15px">Tanggal</th>
                    <th>Jam</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Nominal</th>
                    <th style="padding-right: 15px">Detail</th>
                </tr>
            </thead>
            <tbody>
              @if(count($riwayat) == 0)
                <tr>
                    <td colspan="6" class="text-center">- tidak ada data -</td>
                </tr>
              @else
                @foreach($riwayat as $data)
                <tr>
                    <td style="padding-left: 15px; white-space: nowrap;">{{ $data->created_at->format('d/m/Y') }}</td>
                    <td>{{ $data->created_at->format('H:i') }}</td>
                    <td>{{ $data->nik }}</td>
                    <td>{{ $data->nama }}</td>
                    <td align="right">{{ number_format($data->total_harga, 0, ',', '.') }}</td>
                    <td style="padding-right: 15px" class="d-flex justify-content-end">
                        <button
                            data-toggle="modal"
                            data-target="#detail"
                            onclick="showModal({{ $data->id }})"
                            class="btn btn-sm btn-primary"
                            style="height: 20px; width: 20px; display: flex; justify-content: center; align-items: center"
                        >
                            <i class="material-icons" style="font-size: 13px">visibility</i>
                        </button>
                    </td>
                </tr>
                @endforeach
              @endif
            </tbody>
        </table>
    </div>
    @include('warung.component.menu')

@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/qrious/4.0.2/qrious.min.js"></script>

    <script>
        function submit(){
            $('#formFilter').submit()
        }

        function formatRupiah(angka, prefix = "Rp. "){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split  = number_string.split(','),
            sisa   = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                var separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        var qr;

        qr = new QRious({
                element: document.getElementById('qr-code'),
                size: 200,
                value: 'sft-epost'
            });

        function generateQRCode(text) {
            qr.set({
                foreground: 'black',
                size: 50,
                value: text
            });
        }

      function showModal(id){
        $.ajax({
               url : "{{ url('warung/riwayat/get-detail-transaksi') }}/" + id,
               method : "GET",
               success : (res) => {

                    if(res.length > 0){

                        generateQRCode(String(res[0].id));

                        var tanggal = new Date(res[0].created_at);
                            tanggal = ("0" + tanggal.getDate()).slice(-2) + '/' + ("0" + (tanggal.getMonth() + 1)).slice(-2) + '/' + tanggal.getFullYear();
                        $('#id-transaksi').text(res[0].id);
                        $('#tanggal-transaksi').text(tanggal);
                        $('#nama-warung').text(res[0].nama_warung);
                        $('#id-warung').text(res[0].warung_id);
                        $('#nama-penerima').text(res[0].nama);
                        $('#nik-penerima').text(res[0].nik);
                        $('#foto-transaksi').attr('src', res[0].foto_transaksi);

                        var body = '';
                        res.forEach(value => {
                            body += `<tr>
                                        <td>${value.nama_produk}</td>
                                        <td>${formatRupiah(String(value.harga_satuan))}</td>
                                        <td>${value.jumlah_produk}</td>
                                        <td>${formatRupiah(String(value.sub_total))}</td>
                                    </tr>`;
                        });

                        body += `<tr style="background: #6b7192; color: #fff">
                                    <td colspan="3">
                                        <b>TOTAL HARGA KESELURUHAN</b>
                                    </td>
                                    <td>
                                        <b>${formatRupiah(String(res[0].total_harga))}</b>
                                    </td>
                                </tr>`;
                        $('#table-produk').children('tbody').children().remove();
                        $('#table-produk').children('tbody').append(body);
                        $('#detail').modal('show');
                    }
                }
               })
      }
        $('input[name="daterange"]').daterangepicker({
            opens: 'center',
            autoApply: true
        });

        $('.header-search-btn').on('click', function() {
            $('.header-search-input').css({
                'width': '100%',
                'background': '#fff'
            })
            $('.header-search-btn').css('background', '#fff')
        })
    </script>
@endpush

@push('styles')
    <style>
        .header {
            position: relative;
            width: 100%;
            height: 60px;
            display: grid;
            grid-template-columns: 1fr 200px;
        }

            .header-img {
                position: absolute;
                width: calc(100% + 30px);
                margin-left: -15px;
                z-index: -1;
            }

            .header-title {
                font-size: 20px;
                color: #6b7192;
                font-weight: 600;
                justify-self: start;
                display: flex;
                align-items: center;
            }

            .header-search {
                justify-self: end;
                display: flex;
                align-items: center;
            }

                .header-search-wrap {
                    width: 100%;
                    height: max-content;
                    display: flex;
                    justify-content: flex-end;
                }

                    .header-search-wrap i {
                        width: 35px;
                        height: 35px;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        color: #6b7192;
                    }

                    .header-search-input {
                        width: 0%;
                        height: 35px;
                        border: 0;
                        padding: 0;
                        outline: none;
                        font-size: 14px;
                        color: #555;
                        padding: 0 10px;
                        transition: 0.3s;
                        background: transparent;
                    }

            .header-filter,
            .header-notif {
                display: flex;
                align-items: center;
                justify-content: center;
            }

                .header-filter i,
                .header-notif i {
                    color: #6b7192;
                }

        .dompet {
            width: 100%;
            color: #6b7192;
        }

            .dompet-title {
                font-size: 13px;
            }

            .dompet-value {
                font-size: 25px;
                font-weight: 600;
            }

        .riwayat {
            width: calc(100% + 30px);
            background: #fff;
            margin-left: -15px;
            margin-bottom: 100px;
        }

            .riwayat-head {
                padding: 10px 15px;
                margin-top: 10px;
                display: grid;
                grid-template-columns: 1fr 1fr;
            }

                .riwayat-head-title {
                    font-size: 16px;
                    color: #6b7192;
                    font-weight: 600;
                    display: flex;
                    align-items: center;
                }

                .riwayat-head-filter {
                    width: 30px;
                    height: 30px;
                    border-radius: 7px;
                    box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    justify-self: end;
                }

                    .riwayat-head-filter i {
                        color: #6b7192;
                    }

        .table {
            font-size: 10px !important;
        }

        .filter-title {
            width: 100%;
            color: #6b7192;
            font-weight: 600;
        }

        .filter-paket {
            width: 100%;
            overflow-x: scroll;
            white-space: nowrap;
            padding: 10px 0 10px 10px;
            margin-left: -10px;
        }

            .filter-paket-item {
                width: max-content;
                height: max-content;
                margin-right: 10px;
                display: inline-block;
                border-radius: 15px;
                padding: 5px 10px;
                font-size: 14px;
                color: #555;
                box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.20);
            }

            .filter-paket-active {
                background: #6b7192;
                color: #fff;
                font-weight: 600;
            }

        .detail {
            width: 100%;
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 10px;
        }

            .detail-order {
                justify-self: start;
                display: grid;
                grid-template-columns: 50px 1fr;
                font-size: 12px;
            }

                .detail-order-qr {
                    grid-row: 1/3;
                    display: flex;
                    align-items: center;
                }

                .detail-order-title {
                    width: 100%;
                    font-weight: 600;
                }

                .detail-order-value {
                    width: 100%;
                }

            .detail-date {
                font-size: 12px;
            }

                .detail-date-title {
                    width: 100%;
                    font-weight: 600;
                    text-align: end;
                }

                .detail-date-value {
                    width: 100%;
                    text-align: end;
                }

            .detail-person {
                justify-self: start;
                display: grid;
                grid-template-columns: 80px 1fr;
                font-size: 12px;
            }

                .detail-person-photo {
                    width: 100%;
                }

                    .detail-person-photo img {
                        width: 70px;
                    }

                .detail-person-wrap {
                    display: flex;
                    display: flex;
                    justify-content: flex-start;
                    flex-direction: column;
                }

                    .detail-person-title {
                        width: 100%;
                        font-weight: 600;
                        color: #6b7192;
                    }

                    .detail-person-name {
                        width: 100%;
                        font-weight: 600;
                    }

                    .detail-person-nik {
                        width: 100%;
                        font-weight: 600;
                    }


            .detail-warung {
                width: 100%;
                font-size: 12px;
            }

                .detail-warung-title {
                    width: 100%;
                    font-weight: 600;
                    color: #6b7192;
                    text-align: end;
                }

                .detail-warung-name {
                    width: 100%;
                    font-weight: 600;
                    text-align: end;
                }

                .detail-warung-nik {
                    width: 100%;
                    font-weight: 600;
                    text-align: end;
                }

            .detail-trx {
                grid-column: 1/3;
            }


    </style>
@endpush
