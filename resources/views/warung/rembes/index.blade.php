@extends('warung.layout')

@section('content')
    <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="detail">
                        <div class="detail-order">
                            <div class="detail-order-qr">
                                <canvas id="qr-code"></canvas>
                            </div>
                            <div class="detail-order-title">Order ID</div>
                            <div class="detail-order-value" id="id-transaksi">#1</div>
                        </div>
                        <div class="detail-date">
                            <div class="detail-date-title">Tanggal</div>
                            <div class="detail-date-value" id="tanggal-transaksi">01 Januari 2021</div>
                        </div>
                        <div class="detail-person">
                            <div class="detail-person-photo">
                                <img id="foto-transaksi" src="/img/sample.jpg" />
                            </div>
                            <div class="detail-person-wrap">
                                <div class="detail-person-title">Penerima BLT</div>
                                <div class="detail-person-name" id="nama-penerima">Bambang</div>
                                <div class="detail-person-nik" id="nik-penerima">3203576587787</div>
                            </div>
                        </div>
                        <div class="detail-warung">
                            <div class="detail-warung-title">Warung BLT</div>
                            <div class="detail-warung-name" id="nama-warung">Warung Barokah 1</div>
                            <div class="detail-warung-nik" id="id-warung">ID-1</div>
                        </div>
                        <div class="detail-trx">
                            <table class="table table-sm table-striped" style="font-size: 12px; width: 100%;" id="table-produk">
                                <thead>
                                    <tr style="background: #6b7192; color: #fff">
                                        <th>Nama</th>
                                        <th>Harga</th>
                                        <th>Jml</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-sm btn-secondary" data-dismiss="modal">TUTUP</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                  <form id="formCari">
                    <div class="filter-title mt-2 mb-2">Filter berdasarkan tanggal</div>
                    <input class="form-control" name="tanggal" value="{{ $tgl ? $tgl : date('d-m-Y') }}" />
                    <div class="d-flex justify-content-end mt-4">
                        <button class="btn btn-sm shadow" data-dismiss="modal">BATAL</button>
                        <button class="btn btn-sm shadow ml-3" style="background: #6b7192; color: #fff" onclick="cari()">FILTER</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header">
        <img class="header-img" src="/img/city2.png" />
        <div class="header-title">Rembes</div>
    </div>
    <div class="rembes">
        <div class="rembes-title">Telah Rembes</div>
        <div class="rembes-value">Rp. {{ number_format($saldo, 0, ',', '.') }}</div>
    </div>

    <div class="data">
        <div class="data-action">
            <a class="data-action-item {{ $pending }}" href="{{ url('warung/rembes/pending') }}">Belum Cair</a>
            <a class="data-action-item {{ $aktif }}" href="{{ url('warung/rembes/cair') }}">Sudah Cair</a>
        </div>
        <div class="data-table">
            <div class="data-table-tools">
                <div class="data-table-tools-filter" data-toggle="modal" data-target="#filter">
                    <i class="material-icons">date_range</i>
                </div>
                <div class="data-table-tools-date">{{ $tgl ? $tgl : 'Semua Tanggal' }}</div>
                <div class="data-table-tools-search">
                    <input class="data-table-tools-search-input" type="text" placeholder="Cari NIK/Nama" value="{{ $nama ? $nama : '' }}" name="cari">
                    <i class="material-icons data-table-tools-search-btn" onclick="cari()">search</i>
                  </div>
                </form>
            </div>
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th style="padding-left: 15px">Tanggal</th>
                        <th>Jam</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Nominal</th>
                        <th style="padding-right: 15px">Detail</th>
                    </tr>
                </thead>
                <tbody>
                  @if(count($transaksi) == 0)
                    <tr>
                        <td colspan="6">- tidak ada data -</td>
                    </tr>
                  @else
                    @foreach($transaksi as $data)
                    <tr>
                        <td style="padding-left: 15px">{{ $data->created_at->format('d/m/Y') }}</td>
                        <td>{{ $data->created_at->format('H:i') }}</td>
                        <td>{{ $data->nik }}</td>
                        <td>{{ $data->nama }}</td>
                        <td align="right">{{ number_format($data->total_harga, 0, ',', '.') }}</td>
                        <td class="d-flex justify-content-end" style="padding-right: 15px">
                            <button
                                onclick="showDetail('{{$data->id}}')"
                                class="btn btn-sm btn-primary"
                                style="height: 20px; width: 20px; display: flex; justify-content: center; align-items: center"
                            >
                                <i class="material-icons" style="font-size: 13px">visibility</i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                  @endif
                </tbody>
            </table>
        </div>
    </div>
    @include('warung.component.menu')
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/qrious/4.0.2/qrious.min.js"></script>

    <script>
        $(()=>{
                $('.data-table-tools-search-input').css({
                    'width': '150px',
                    'background': '#eee'
                })
                $('.data-table-tools-search-btn').css({
                    'background': '#eee'
                })
        })

        function cari(){
            $("#formCari").submit()
        }

        $('input[name="tanggal"]').daterangepicker({
            autoApply: true
        })

        function formatRupiah(angka, prefix = "Rp. "){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split  = number_string.split(','),
            sisa   = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                var separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        var qr;

        qr = new QRious({
                element: document.getElementById('qr-code'),
                size: 200,
                value: 'sft-epost'
            });

        function generateQRCode(text) {
            qr.set({
                foreground: 'black',
                size: 50,
                value: text
            });
        }

      function showDetail(id){
          url = '{{route("warung.rembes.detail", "")}}' + '/' + id;

          $.ajax({
            url : url,
            method : 'get',
            success : function(res){
                if(res.length > 0){

                    generateQRCode(String(res[0].id));

                    var tanggal = new Date(res[0].created_at);
                        tanggal = ("0" + tanggal.getDate()).slice(-2) + '/' + ("0" + (tanggal.getMonth() + 1)).slice(-2) + '/' + tanggal.getFullYear();
                    $('#id-transaksi').text(res[0].id);
                    $('#tanggal-transaksi').text(tanggal);
                    $('#nama-warung').text(res[0].nama_warung);
                    $('#id-warung').text(res[0].warung_id);
                    $('#nama-penerima').text(res[0].nama);
                    $('#nik-penerima').text(res[0].nik);
                    $('#foto-transaksi').attr('src', res[0].foto_transaksi);

                    var body = '';
                    res.forEach(value => {
                        body += `<tr>
                                    <td>${value.nama_produk}</td>
                                    <td>${formatRupiah(String(value.harga_satuan))}</td>
                                    <td>${value.jumlah_produk}</td>
                                    <td>${formatRupiah(String(value.sub_total))}</td>
                                </tr>`;
                    });

                    body += `<tr style="background: #6b7192; color: #fff">
                                <td colspan="3">
                                    <b>TOTAL HARGA KESELURUHAN</b>
                                </td>
                                <td>
                                    <b>${formatRupiah(String(res[0].total_harga))}</b>
                                </td>
                            </tr>`;
                    $('#table-produk').children('tbody').children().remove();
                    $('#table-produk').children('tbody').append(body);
                    $('#detail').modal('show');
                }
            }
          });
      }
    </script>
@endpush

@push('styles')
    <style>
        body {
            background: #7d83a8;
        }

        .header {
            position: relative;
            width: 100%;
            height: 60px;
            display: grid;
            grid-template-columns: 1fr 200px;
        }

            .header-img {
                position: absolute;
                width: calc(100% + 30px);
                margin-left: -15px;
                z-index: -1;
            }

            .header-title {
                font-size: 20px;
                color: #fff;
                font-weight: 600;
                justify-self: start;
                display: flex;
                align-items: center;
            }

        .rembes {
            width: 100%;
            color: #fff;
        }

            .rembes-title {
                font-size: 13px;
            }

            .rembes-value {
                font-size: 25px;
                font-weight: 600;
            }

        .data {
            width: calc(100% + 30px);
            min-height: calc(100vh - 130px);
            margin-top: 10px;
            margin-left: -15px;
            background: #fff;
        }

            .data-action {
                width: 100%;
                display: flex;
                background: #fff;
            }

                .data-action-item {
                    width: 50%;
                    padding: 10px 20px;
                    font-size: 16px;
                    font-weight: 600;
                    color: #6b7192;
                    cursor: pointer;
                    outline: none;
                    text-decoration: none;
                    text-align: center;
                }

                .data-action-item:active {
                    text-decoration: none;
                    color: #6b7192;
                }

                .data-action-item-active {
                    background: #fff;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    flex-direction: column;
                }

                .data-action-item-active::after {
                    content: '';
                    width: 40%;
                    height: 2px;
                    border-radius: 2px;
                    margin-top: 5px;
                    background: #6b7192;
                }

            .data-table {
                background: #fff;
            }

                .data-table-tools {
                    width: 100%;
                    height: 40px;
                    display: grid;
                    grid-template-columns: 40px 1fr 200px;
                    padding: 0 10px 0 5px;
                }

                    .data-table-tools-filter {
                        width: 40px;
                        height: 40px;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }

                    .data-table-tools-filter:hover {
                        background: #eee;
                    }

                        .data-table-tools-filter i {
                            color: #6b7192;
                        }

                    .data-table-tools-date {
                        font-size: 13px;
                        font-weight: 600;
                        color: #6b7192;
                        display: flex;
                        align-items: center;
                        padding-left: 5px;
                    }

                    .data-table-tools-search {
                        width: 200px;
                        height: 40px;
                        display: flex;
                        align-items: center;
                        justify-content: flex-end;
                        justify-self: end;
                        padding-right: 5px;
                    }

                        .data-table-tools-search-btn {
                            width: 30px;
                            height: 30px;
                            display: flex;
                            align-items: center;
                            justify-content: center;
                            color: #6b7192;
                            border-top-right-radius: 5px;
                            border-bottom-right-radius: 5px;
                        }

                        .data-table-tools-search-input {
                            width: 0;
                            height: 30px;
                            border: 0;
                            padding: 0;
                            outline: none;
                            font-size: 14px;
                            color: #555;
                            padding: 0 10px;
                            transition: 0.3s;
                            border-top-left-radius: 5px;
                            border-bottom-left-radius: 5px;
                            transition: 0.3s;
                        }

        .table {
            font-size: 10px !important;
        }

        .filter-title {
            width: 100%;
            color: #6b7192;
            font-weight: 600;
        }

        .detail {
            width: 100%;
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 10px;
        }

            .detail-order {
                justify-self: start;
                display: grid;
                grid-template-columns: 50px 1fr;
                font-size: 12px;
            }

                .detail-order-qr {
                    grid-row: 1/3;
                    display: flex;
                    align-items: center;
                }

                .detail-order-title {
                    width: 100%;
                    font-weight: 600;
                }

                .detail-order-value {
                    width: 100%;
                }

            .detail-date {
                font-size: 12px;
            }

                .detail-date-title {
                    width: 100%;
                    font-weight: 600;
                    text-align: end;
                }

                .detail-date-value {
                    width: 100%;
                    text-align: end;
                }

            .detail-person {
                justify-self: start;
                display: grid;
                grid-template-columns: 80px 1fr;
                font-size: 12px;
            }

                .detail-person-photo {
                    width: 100%;
                }

                    .detail-person-photo img {
                        width: 70px;
                    }

                .detail-person-wrap {
                    display: flex;
                    display: flex;
                    justify-content: flex-start;
                    flex-direction: column;
                }

                    .detail-person-title {
                        width: 100%;
                        font-weight: 600;
                        color: #6b7192;
                    }

                    .detail-person-name {
                        width: 100%;
                        font-weight: 600;
                    }

                    .detail-person-nik {
                        width: 100%;
                        font-weight: 600;
                    }


            .detail-warung {
                width: 100%;
                font-size: 12px;
            }

                .detail-warung-title {
                    width: 100%;
                    font-weight: 600;
                    color: #6b7192;
                    text-align: end;
                }

                .detail-warung-name {
                    width: 100%;
                    font-weight: 600;
                    text-align: end;
                }

                .detail-warung-nik {
                    width: 100%;
                    font-weight: 600;
                    text-align: end;
                }

            .detail-trx {
                grid-column: 1/3;
            }

    </style>
@endpush
