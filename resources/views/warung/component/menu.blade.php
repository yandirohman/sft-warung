<div class="menu">
    <a
        class="menu-item @if(Request::route()->getName() == 'warung.belanja.index') menu-item-active @endif"
        href="{{ route('warung.belanja.index') }}"
    >
        <div class="menu-item-wrap">
            <i class="material-icons">shopping_cart</i>
            <div class="menu-item-title">Belanja</div>
        </div>
    </a>
    <a
        class="menu-item @if(Request::route()->getName() == 'warung.riwayat.index') menu-item-active @endif" h
        href="{{ route('warung.riwayat.index') }}"
    >
        <div class="menu-item-wrap">
            <i class="material-icons">receipt</i>
            <div class="menu-item-title">Riwayat</div>
        </div>
    </a>
    <a
        class="menu-item @if(Request::route()->getName() == 'warung.home.index') menu-item-active @endif"
        href="{{ route('warung.home.index') }}"
    >
        <div class="menu-item-wrap">
            <i class="material-icons">home</i>
            <div class="menu-item-title">Home</div>
        </div>
    </a>
    <a
        class="menu-item @if(Request::route()->getName() == 'warung.rembes.index') menu-item-active @endif"
        href="{{ route('warung.rembes.index', 'pending') }}"
    >
        <div class="menu-item-wrap">
            <i class="material-icons">attach_money</i>
            <div class="menu-item-title">Rembes</div>
        </div>
    </a>
    <a
        class="menu-item @if(Request::route()->getName() == 'warung.konfigurasi.index') menu-item-active @endif"
        href="{{ route('warung.konfigurasi.index') }}"
    >
        <div class="menu-item-wrap">
            <i class="material-icons">settings</i>
            <div class="menu-item-title">Atur</div>
        </div>
    </a>
</div>

<style>
    .menu {
        position: fixed;
        width: 100%;
        height: 60px;
        bottom: 0;
        left: 0;
        background: #fff;
        box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
        display: grid;
        grid-template-columns: repeat(5, 1fr);
        z-index: 99;
    }

        .menu-item {
            height: 60px;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            text-decoration: none !important;
        }

            .menu-item-wrap {
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;
            }

                .menu-item i {
                    font-size: 25px;
                    color: #888;
                }

                .menu-item-title {
                    font-size: 11px;
                    color: #aaa;
                }

        .menu-item-active {
            width: 100%;
        }

            .menu-item-active .menu-item-wrap {
                background: #6b7192;
                padding: 7px;
                border-radius: 10px;
                box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
            }

                .menu-item-active .menu-item-title {
                    display: none;
                }

            .menu-item-active i {
                font-size: 27px;
                color: #fff;
            }
</style>
