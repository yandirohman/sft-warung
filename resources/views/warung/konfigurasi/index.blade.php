@extends('warung.layout')

@section('content')
    <div class="konfigurasi-head">
        <div class="konfigurasi-head-text">Konfigurasi</div>
    </div>
    {!! Session::get('msg') !!}
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link {{ $page == 'paket' ? 'active' : '' }}" href="{{ route('warung.konfigurasi.index', ['page' => 'paket']) }}">Paket</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ $page == 'list-produk' ? 'active' : '' }}" href="{{ route('warung.konfigurasi.index', ['page' => 'list-produk']) }}">Modal</a>
        </li>
    </ul>
    <div id="konfigurasi"></div>
    @include('warung.component.menu')
@endsection

@push('scripts')
<script type="text/babel">

    class ModalHarga extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                harga: 0
            }
        }

        handleChange = e => {
            this.setState({
                harga: e.target.value
            })
        }

        render() {
            let { show, dataHarga } = this.props
            let { harga } = this.state
            return (
                <div className="konfigurasi-bg" style={ {display: show ? 'flex': 'none'} }>
                    <div className="konfigurasi-box">
                        {
                            dataHarga ? (
                                <React.Fragment>
                                    <div className="konfigurasi-box-title">Harga Modal</div>
                                    <div className="konfigurasi-box-value mb-3">{dataHarga.nama}</div>
                                    <div className="konfigurasi-box-label mb-2">Harga modal {dataHarga.nama} (per 1 {dataHarga.satuan})</div>
                                    <input
                                        className="form-control mb-3"
                                        value={harga === 0 ? (dataHarga.harga_beli ? dataHarga.harga_beli : '') : harga}
                                        onChange={this.handleChange}
                                        placeholder="Masukan harga"
                                        type="number"
                                    />
                                </React.Fragment>
                            ) : ''
                        }
                        <div className="d-flex justify-content-end">
                            <button
                                className="btn btn-sm btn-secondary"
                                onClick={() => {
                                    this.props.closeHandle()
                                    this.setState({harga: 0})
                                }}>
                                BATAL
                            </button>
                            <button
                                className="btn btn-sm btn-primary ml-3"
                                onClick={() => {
                                    this.props.changeHargaBeli({
                                        config_harga_id: dataHarga.config_harga_id,
                                        produk_id: dataHarga.produk_id,
                                        harga_beli: harga
                                    })
                                    this.props.closeHandle()
                                    this.setState({harga: 0})
                                }}>SIMPAN</button>
                        </div>
                    </div>
                </div>
            )
        }
    }

    class ListHarga extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                modalShow: false,
                datas: [],
                dataHarga: false
            }
        }

        componentDidMount() {
            this.getData()
        }

        getData = () => {
            fetch('{{ route("warung.konfigurasi.produk.index") }}')
                .then(response => response.json())
                .then(datas => {
                    this.setState({
                        datas: datas
                    })
                })
        }

        closeHandle = () => {
            this.setState({
                modalShow: false
            })
        }

        openHandle(data) {
            this.setState({
                modalShow: true,
                dataHarga: data
            })
        }

        changeHargaBeli = data => {
            let dataStore = {
                config_harga_id: data.config_harga_id,
                produk_id: data.produk_id,
                harga_beli: data.harga_beli
            }
            let url = '{{ route("warung.konfigurasi.produk.change") }}'
            fetch(url, {
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRF-Token": '{{ csrf_token() }}'
                },
                credentials: "same-origin",
                method: 'POST',
                body: JSON.stringify({
                    data: dataStore
                })
            }).then(res => res.text()).then(data => {
                this.getData()
            })
        }

        uang(x) {
            return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
        }

        render() {
            let { modalShow, datas, dataHarga } = this.state
            return (
                <React.Fragment>
                    <ModalHarga
                        show={modalShow}
                        closeHandle={this.closeHandle}
                        dataHarga={dataHarga}
                        changeHargaBeli={this.changeHargaBeli}
                        getData={this.getData}
                    />
                    { datas.length === 0 ? <div className="text-center mb-3"><i>memuat data...</i></div> : '' }
                    {
                        datas.map(item => (
                            <div className="konfigurasi-item" onClick={() => this.openHandle(item)}>
                                <div className="konfigurasi-item-title">{item.nama}</div>
                                <div className="konfigurasi-item-line"></div>
                                <div className="konfigurasi-item-harga">
                                    <div className="konfigurasi-item-harga-col">
                                        <div className="konfigurasi-item-harga-title">Harga Modal</div>
                                        <div className="konfigurasi-item-harga-value">
                                            { item.harga_beli ? 'Rp. '+this.uang(item.harga_beli) : <i className="text-danger">Belum diatur</i> }
                                        </div>
                                    </div>
                                </div>
                                <div className="konfigurasi-item-edit">
                                    <i className="material-icons">edit</i>
                                </div>
                            </div>
                        ))
                    }
                </React.Fragment>
            )
        }
    }

    class CariProduk extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                datas: [],
                datasBackup: []
            }
        }

        componentDidMount() {
            this.getData()
        }

        getData = () => {
            fetch('{{ route("warung.konfigurasi.paket.produk") }}')
                .then(response => response.json())
                .then(datas => {
                    this.setState({
                        datas: datas,
                        datasBackup: datas
                    })
                })
        }

        addDetailPaket = data => {
            let paketId = this.props.paketId
            let dataStore = {
                harga_beli: data.harga_terendah,
                harga_satuan: data.harga_terendah,
                jml_produk: 1,
                nama_produk: data.nama,
                paket_id: paketId,
                produk_id: data.id,
                sub_total: (data.harga_terendah * 1),
                untung: 0
            }
            let url = '{{ route("warung.konfigurasi.paket.add-detail") }}'
            fetch(url, {
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRF-Token": '{{ csrf_token() }}'
                },
                credentials: "same-origin",
                method: 'POST',
                body: JSON.stringify(dataStore)
            }).then(res => res.text()).then(data => {
                this.props.getDataPaket()
                this.props.getDetailPaket(paketId)
                this.props.toggleCari(false)
            })
        }

        handleSearch = e => {
            let search = e.target.value.toLowerCase()
            let datas = this.state.datasBackup
            let datasResult = [];
            datas.forEach(item => {
                let nama = item.nama.toLowerCase()
                if (nama.match(search)) {
                    datasResult.push(item)
                }
            })
            if (search === '') {
                this.setState({
                    datas: this.state.datasBackup
                })
            } else {
                this.setState({
                    datas: datasResult
                })
            }
        }

        uang(x) {
            return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
        }

        render() {
            let { show } = this.props
            let { datas } = this.state
            return (
                <div className="cari-bg" style={ {display: show ? 'block' : 'none'} }>
                    <div className="cari-input">
                        <div className="cari-input-icon">
                            <i className="material-icons">search</i>
                        </div>
                        <input
                            className="cari-input-value"
                            placeholder="Cari Produk"
                            onChange={this.handleSearch}
                        />
                        <div className="cari-input-close" onClick={() => this.props.toggleCari(false)}>
                            <i className="material-icons">clear</i>
                        </div>
                    </div>
                    <div className="cari-wrap">
                        {
                            datas.map(item => (
                                <div className="cari-item" onClick={() => this.addDetailPaket(item)}>
                                    <div className="cari-item-info">
                                        <div className="cari-item-name">{item.nama}</div>
                                        <div className="cari-item-harga">Rp. {this.uang(item.harga_terendah)} - {this.uang(item.harga_tertinggi)} /{item.satuan}</div>
                                    </div>
                                    <div className="cari-item-add">
                                        <i className="material-icons">add</i>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            )
        }
    }

    class ModalPaket extends React.Component {
        deleteDetailPaket = (idDetailPaket, paketId) => {
            let url = `/warung/konfigurasi/paket/detail/delete/${idDetailPaket}`
            fetch(url, {
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRF-Token": '{{ csrf_token() }}'
                },
                credentials: "same-origin",
                method: 'POST'
            }).then(res => res.text()).then(data => {
                this.props.getDataPaket()
                this.props.getDetailPaket(paketId)
            })
        }

        uang(x) {
            return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
        }

        render() {
            let { show, datas, idPaket } = this.props
            return (
                <div className="konfigurasip-bg" style={ {display: show ? 'flex' : 'none'} }>
                    <div className="konfigurasip-wrap">
                        <div className="konfigurasip-box">
                            <div className="konfigurasip-box-title">List Produk</div>
                            {
                                datas.map((item, index) => (
                                    <div className="konfigurasip-box-item">
                                        <div className="konfigurasip-box-item-title">{item.nama_produk}</div>
                                        <div className="konfigurasip-box-item-harga">
                                            <div>Harga Warung Anda :</div>
                                            <div>Rp. {this.uang(item.harga_satuan)}</div>
                                            <div onClick={() => this.props.toggleChange(true, index)}>
                                                <i className="material-icons">edit</i>
                                            </div>
                                        </div>
                                        <div className="konfigurasip-box-item-line"></div>
                                        <div className="konfigurasip-box-item-total">
                                            <div>Harga Total :</div>
                                            <div>Rp. {this.uang(item.sub_total)}</div>
                                        </div>
                                        <div className="konfigurasip-box-item-jumlah">{item.jml_produk}</div>
                                        <div className="konfigurasip-box-item-inc" onClick={() => this.props.changeJumlahDetailPaket(index, 'inc')}>
                                            <i className="material-icons">add</i>
                                        </div>
                                        <div className="konfigurasip-box-item-dec" onClick={() => this.props.changeJumlahDetailPaket(index, 'dec')}>
                                            <i className="material-icons">remove</i>
                                        </div>
                                        <div
                                            className="konfigurasip-box-item-delete"
                                            onClick={() => this.deleteDetailPaket(item.detail_paket_id, idPaket)}>
                                            <i className="material-icons">delete</i>
                                        </div>
                                    </div>
                                ))
                            }
                            <div className="konfigurasip-item-add" onClick={() => this.props.toggleCari(true)}>
                                <i className="material-icons">add</i>
                                <b>Tambah Produk</b>
                            </div>
                            <div className="d-flex justify-content-end mt-4">
                                <button
                                    className="btn btn-sm"
                                    onClick={() => this.props.toggleModal(false)}>
                                    BATAL
                                </button>
                                <button
                                    className="btn btn-sm btn-primary ml-3"
                                    onClick={() => this.props.storeChangeDetailPaket()}>
                                    SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }

    class ChangeHarga extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                hargaBaru: 0
            }
        }

        handleChange = e => {
            this.setState({
                hargaBaru: e.target.value
            })
        }

        uang(x) {
            return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
        }

        render() {
            let { show, index, datas } = this.props
            let { hargaBaru } = this.state
            let data = datas[index]
            return (
                <div className="charga-bg" style={ {display: show ? 'flex' : 'none'} }>
                    <div className="charga-box">
                        <div className="charga-title">Harga Beras</div>
                        <table>
                            <tr>
                                <td>Harga Minimal</td>
                                <td>&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                <td><b>{data ? `Rp. ${this.uang(data.harga_terendah)}` : ''}</b></td>
                            </tr>
                            <tr>
                                <td>Harga Maksimal</td>
                                <td>&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                <td><b>{data ? `Rp. ${this.uang(data.harga_tertinggi)}` : ''}</b></td>
                            </tr>
                        </table>
                        <input
                            className="form-control"
                            placeholder="Masukan harga warung anda"
                            type="number"
                            onChange={this.handleChange}
                            value={hargaBaru === 0 ? (data ? data.harga_satuan : 0) : hargaBaru}
                        />
                        <div className="d-flex justify-content-end mt-4">
                            <button
                                className="btn btn-sm btn-secondary"
                                onClick={() => {this.props.toggleChange(false); this.setState({hargaBaru: 0})}}>
                                BATAL
                            </button>
                            <button
                                className="btn btn-sm btn-primary ml-3"
                                onClick={() => {this.props.changeHargaDetailPaket(index, hargaBaru); this.setState({hargaBaru: 0})}}>
                                SIMPAN
                            </button>
                        </div>
                    </div>
                </div>
            )
        }
    }

    class AddPaket extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                namaPaket: ''
            }
        }

        handleChange = e => {
            this.setState({
                namaPaket: e.target.value
            })
        }

        handleSubmit = () => {
            let url = '{{ route("warung.konfigurasi.paket.store") }}'
            fetch(url, {
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRF-Token": '{{ csrf_token() }}'
                },
                credentials: "same-origin",
                method: 'POST',
                body: JSON.stringify({
                    nama_paket: this.state.namaPaket === '' ? `Paket ${this.props.jmlPaket + 1}` : this.state.namaPaket
                })
            }).then(res => res.text()).then(() => {
                this.props.toggleAdd(false)
                this.props.getDataPaket()
                this.setState({namaPaket: ''})
            })
        }

        render() {
            let { show } = this.props
            return (
                <div className="apaket-bg" style={ {display: show ? 'flex' : 'none'} }>
                    <div className="apaket-box">
                        <div className="apaket-title">Buat Paket Baru</div>
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Masukan Nama Paket"
                            onChange={this.handleChange}
                            value={this.state.namaPaket === '' ? `Paket ${this.props.jmlPaket + 1}` : this.state.namaPaket}
                        />
                        <div className="d-flex justify-content-end mt-4">
                            <button
                                className="btn btn-sm btn-secondary"
                                onClick={() => {
                                    this.props.toggleAdd(false)
                                    this.setState({namaPaket: ''})
                                }}>
                                BATAL
                            </button>
                            <button
                                className="btn btn-sm btn-primary ml-3"
                                onClick={this.handleSubmit}>
                                SIMPAN
                            </button>
                        </div>
                    </div>
                </div>
            )
        }
    }

    class KonfigurasiPaket extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                modalShow: false,
                cariShow: false,
                changeShow: false,
                addShow: false,
                page: 'harga',
                isConfigExist: 0,

                idPaket: 0,
                dataPaket: [],
                dataPaketLoad: true,
                dataDetailpaket: [],
                indexDetailPaket: 0,
                jmlPaket: 0
            }
        }

        componentDidMount() {
            this.getDataPaket()
            this.checkConfig()
        }

        toggleModal = (show, idPaket = false) => {
            this.setState({
                modalShow: show
            })
            if (show === true && idPaket != false) {
                this.getDetailPaket(idPaket)
                this.setState({
                    idPaket: idPaket
                })
            }
            if (show === false && idPaket === false) {
                this.setState({
                    idPaket: 0,
                    dataDetailpaket: []
                })
            }
        }

        toggleCari = show => {
            this.storeChangeDetailPaket(false);
            this.setState({
                cariShow: show
            })
        }

        toggleChange = (show, index = false) => {
            if (index) {
                this.setState({
                    changeShow: show,
                    indexDetailPaket: index
                })
            } else {
                this.setState({
                    changeShow: show,
                    indexDetailPaket: 0
                })
            }
        }

        toggleAdd = show => {
            let isConfigExist = this.state.isConfigExist
            if (isConfigExist == 1) {
                this.setState({
                    addShow: show
                })
            } else {
                alert('Silahkan masukan nilai modal anda terlebih dahulu, di menu "Modal"!')
            }
        }

        getDataPaket = () => {
            fetch('{{ route("warung.konfigurasi.paket.index") }}')
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        dataPaket: data,
                        dataPaketLoad: false,
                        jmlPaket: data.length
                    })
                })
        }

        deletePaket = (id) => {
            if(confirm('Hapus paket ini?')) {
                let url = `/warung/konfigurasi/paket/delete/${id}`
                fetch(url, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-CSRF-Token": '{{ csrf_token() }}'
                    },
                    credentials: "same-origin",
                    method: 'POST'
                }).then(res => res.text()).then(() => {
                    this.getDataPaket()
                })
            }
        }

        getDetailPaket = idPaket => {
            let url = `/warung/konfigurasi/paket/detail/${idPaket}`
            fetch(url)
                .then(res => res.json())
                .then(datas => {
                    this.setState({
                        dataDetailpaket: datas,
                    })
                })
        }

        changeJumlahDetailPaket = (index, type) => {
            let dataDetailpaket = this.state.dataDetailpaket
            if (type === 'inc') {
                dataDetailpaket[index].jml_produk += 1
            } else {
                if (dataDetailpaket[index].jml_produk != 1) {
                    dataDetailpaket[index].jml_produk -= 1
                }
            }
            dataDetailpaket[index].sub_total = dataDetailpaket[index].harga_satuan * dataDetailpaket[index].jml_produk
            this.setState({
                dataDetailpaket: dataDetailpaket
            })
        }

        changeHargaDetailPaket = (index, hargaBaru) => {
            let dataDetailpaket = this.state.dataDetailpaket
            let hargaTertinggi = dataDetailpaket[index].harga_tertinggi;
            let hargaTerendah = dataDetailpaket[index].harga_terendah;
            if (hargaBaru > hargaTertinggi) {
                alert('Harga tidak boleh melebihi batas maksimal')
            } else if (hargaBaru < hargaTerendah) {
                alert('Harga tidak boleh kurang dari batas minimal')
            } else {
                dataDetailpaket[index].harga_satuan = hargaBaru
                dataDetailpaket[index].sub_total = dataDetailpaket[index].harga_satuan * dataDetailpaket[index].jml_produk
                this.setState({
                    dataDetailpaket: dataDetailpaket
                })
                this.toggleChange(false)
            }
        }

        storeChangeDetailPaket = (reload = true) => {
            let url = '{{ route("warung.konfigurasi.paket.change-detail") }}'
            let dataDetailpaket = this.state.dataDetailpaket
            let len = dataDetailpaket.length
            if (len > 0) {
                let dataStore = dataDetailpaket
                fetch(url, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-CSRF-Token": '{{ csrf_token() }}'
                    },
                    credentials: "same-origin",
                    method: 'POST',
                    body: JSON.stringify({
                        datas: dataStore
                    })
                }).then(res => res.text()).then(data => {
                    if (reload) {
                        this.toggleModal(false)
                        this.getDataPaket()
                    }
                })
            }
        }

        checkConfig = () => {
            let url = '/warung/konfigurasi/produk/config/check'
            fetch(url)
                .then(res => res.text())
                .then(isConfigExist => {
                    this.setState({
                        isConfigExist: isConfigExist
                    })
                })
        }

        uang(x) {
            return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
        }

        render() {
            let { modalShow, cariShow, changeShow, addShow, page } = this.state
            let { dataPaket, dataPaketLoad, idPaket, dataDetailpaket, indexDetailPaket, jmlPaket } = this.state
            return (
                <React.Fragment>
                    <AddPaket
                        show={addShow}
                        toggleAdd={this.toggleAdd}
                        getDataPaket={this.getDataPaket}
                        jmlPaket={jmlPaket}
                    />
                    <ChangeHarga
                        show={changeShow}
                        toggleChange={this.toggleChange}
                        changeHargaDetailPaket={this.changeHargaDetailPaket}
                        index={indexDetailPaket}
                        datas={dataDetailpaket}
                    />
                    <ModalPaket
                        show={modalShow}
                        toggleModal={this.toggleModal}
                        toggleCari={this.toggleCari}
                        toggleChange={this.toggleChange}
                        idPaket={idPaket}
                        datas={dataDetailpaket}
                        getDataPaket={this.getDataPaket}
                        getDetailPaket={this.getDetailPaket}
                        changeJumlahDetailPaket={this.changeJumlahDetailPaket}
                        storeChangeDetailPaket={this.storeChangeDetailPaket}
                    />
                    <CariProduk
                        show={cariShow}
                        toggleCari={this.toggleCari}
                        changeDetailPaket={this.changeDetailPaket}
                        paketId={idPaket}
                        getDataPaket={this.getDataPaket}
                        getDetailPaket={this.getDetailPaket}
                    />
                    { dataPaketLoad ? <div className="small text-center">sedang memuat data...</div> : '' }
                    { (dataPaketLoad == false && dataPaket.length == 0) ? <div className="small text-center">Anda belum memiliki paket</div> : '' }
                    {
                        dataPaket.map((item, key) => (
                            <div className="konfigurasip-item">
                                <div className="konfigurasip-item-title">
                                    <span>{item.nama}</span>
                                    <div className="konfigurasip-item-delete" onClick={() => this.deletePaket(item.id)}>
                                        <i className="material-icons">delete</i>
                                    </div>
                                </div>
                                <div className="konfigurasip-item-line"></div>
                                <div className="konfigurasip-item-produk">
                                    <div className="konfigurasip-item-produk-col">
                                        <div className="konfigurasip-item-produk-title">Jumlah Produk</div>
                                        <div
                                            className="konfigurasip-item-produk-value"
                                            onClick={() => this.toggleModal(true, item.id)}
                                        >
                                            <span>{item.jml_produk} Produk</span>
                                            <i className="material-icons">chevron_right</i>
                                        </div>
                                    </div>
                                    <div className="konfigurasip-item-harga-col">
                                        <div className="konfigurasip-item-harga-title">Total Harga</div>
                                        <div className="konfigurasip-item-harga-value">Rp. {this.uang(item.total_harga)}</div>
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                    <div className="konfigurasip-item-add-parent" onClick={() => this.toggleAdd(true)}>
                        <i className="material-icons">add</i>
                        <b>Tambah Paket</b>
                    </div>
                </React.Fragment>
            )
        }
    }

    ReactDOM.render(
        {!! $page == 'paket' ? '<KonfigurasiPaket/>' : '<ListHarga/>' !!},
        document.getElementById('konfigurasi')
    );
</script>
@endpush

@push('styles')
    <style>
        .konfigurasi-head {
            font-size: 20px;
            color: #6b7192;
            font-weight: 600;
            justify-self: start;
            display: flex;
            align-items: center;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        #konfigurasi {
            padding-bottom: 100px;
            padding: 15px 15px 0 15px;
            background: #fff;
            border-bottom: 1px solid #dee2e6;
            border-right: 1px solid #dee2e6;
            border-left: 1px solid #dee2e6;
            margin-bottom: 100px;
        }

            .konfigurasi-item {
                width: 100%;
                height: max-content;
                background: #fff;
                padding: 15px;
                border-radius: 5px;
                box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                margin-bottom: 15px;
                display: grid;
                grid-template-columns: 1fr 80px;
            }

                .konfigurasi-item-title {
                    width: 100%;
                    font-size: 17px;
                    font-weight: 600;
                    grid-column: 1/2;
                    grid-row: 1/2;
                }

                .konfigurasi-item-line {
                    width: 100%;
                    height: 1px;
                    background: #ccc;
                    margin-top: 5px;
                    margin-bottom: 5px;
                    grid-column: 1/2;
                    grid-row: 2/3;
                }

                .konfigurasi-item-harga {
                    width: 100%;
                    grid-column: 1/2;
                    grid-row: 3/4;
                }

                    .konfigurasi-item-harga-title {
                        font-size: 10px;
                    }

                    .konfigurasi-item-harga-value {
                        font-size: 15px;
                    }

                .konfigurasi-item-edit {
                    grid-column: 2/3;
                    grid-row: 1/4;
                    color: #585d79;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }

                    .konfigurasi-item-edit i {
                        width: 50px;
                        height: 50px;
                        background: #eee;
                        border-radius: 50px;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }

        .konfigurasi-bg {
            position: fixed;
            width: 100%;
            height: 100%;
            background: rgba(0,0,0,0.5);
            top: 0;
            left: 0;
            z-index: 999;
            display: flex;
            align-items: center;
            justify-content: center;
        }

            .konfigurasi-box {
                width: 90%;
                height: max-content;
                background: #fff;
                border-radius: 10px;
                padding: 15px;
                color: #333;
            }

                .konfigurasi-box-title {
                    width: 100%;
                    color: #6b7192;
                    font-size: 17px;
                    font-weight: 600;
                    margin-bottom: 20px;
                }

                .konfigurasi-box-label {
                    font-size: 15px;
                }

                .konfigurasi-box-value {
                    font-size: 23px;
                    font-weight: 600;
                }

                .konfigurasi-box input {
                    font-size: 20px;
                    font-weight: 600;
                    color: #333;
                }

            .konfigurasip-item {
                width: 100%;
                height: max-content;
                background: #fff;
                padding: 15px;
                border-radius: 5px;
                box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                margin-bottom: 15px;
            }

                .konfigurasip-item-title {
                    width: 100%;
                    font-size: 17px;
                    font-weight: 600;
                    display: grid;
                    grid-template-columns: 1fr 50px;
                }

                    .konfigurasip-item-delete {
                        color: #f44336;
                        justify-self: end;
                    }

                .konfigurasip-item-line {
                    width: 100%;
                    height: 1px;
                    background: #ccc;
                    margin-top: 5px;
                    margin-bottom: 5px;
                }

                .konfigurasip-item-produk {
                    width: 100%;
                    display: grid;
                    grid-template-columns: 1fr 1fr;
                }

                    .konfigurasip-item-produk-title {
                        font-size: 10px;
                    }

                    .konfigurasip-item-produk-value {
                        font-size: 15px;
                        color: #03a9f4;
                        font-weight: 600;
                        display: flex;
                        align-items: center;
                        text-decoration: none;
                        outline: none;
                    }

                        .konfigurasip-item-produk-value i {
                            margin-left: 2px;
                            margin-top: 2px;
                            color: #03a9f4;
                            font-size: 18px;
                        }

                .konfigurasip-item-harga {
                    width: 100%;
                    display: grid;
                    grid-template-columns: 1fr 1fr;
                }

                    .konfigurasip-item-harga-title {
                        font-size: 10px;
                    }

                    .konfigurasip-item-harga-value {
                        font-size: 15px;
                    }

            .konfigurasip-item-add {
                width: 100%;
                height: 40px;
                background: #fff;
                color: #6b7192;
                display: flex;
                align-items: center;
                justify-content: center;
                border-radius: 5px;
                box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                margin-bottom: 15px;
                margin-top: 30px;
            }

            .konfigurasip-item-add-parent {
                width: 100%;
                height: 40px;
                color: #fff;
                background: #6b7192;
                display: flex;
                align-items: center;
                justify-content: center;
                border-radius: 5px;
                box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                margin-bottom: 15px;
                margin-top: 30px;
            }

        .konfigurasip-bg {
            position: fixed;
            width: 100%;
            height: 100%;
            background: rgba(0,0,0,0.5);
            top: 0;
            left: 0;
            z-index: 999;
            display: flex;
            align-items: center;
            justify-content: center;
        }

            .konfigurasip-wrap {
                width: 100%;
                max-height: 100%;
                overflow-y: scroll;
                display: flex;
                justify-content: center;
                padding: 20px 0 50px 0;
            }

            .konfigurasip-box {
                width: 90%;
                height: max-content;
                background: #fff;
                border-radius: 10px;
                padding: 15px;
                color: #333;
            }

                .konfigurasip-box-title {
                    width: 100%;
                    color: #6b7192;
                    font-size: 17px;
                    font-weight: 600;
                    margin-bottom: 20px;
                }

                .konfigurasip-box-item {
                    position: relative;
                    width: calc(100% - 20px);
                    height: max-content;
                    padding: 10px 10px 10px 30px;
                    border-radius: 5px;
                    box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                    display: grid;
                    grid-template-columns: 1fr 50px;
                    margin-bottom: 40px;
                    margin-left: 20px;
                }

                    .konfigurasip-box-item-title {
                        grid-column: 1/2;
                        grid-row: 1/2;
                        font-size: 18px;
                        font-weight: 600;
                        margin-bottom: 10px;
                    }

                    .konfigurasip-box-item-harga {
                        grid-column: 1/2;
                        grid-row: 2/3;
                        background: #6b7192;
                        color: #fff;
                        padding: 5px 10px;
                        border-radius: 5px;
                        display: grid;
                        grid-template-columns: 1fr 40px;
                        border: 2px dashed #585d79;
                    }

                        .konfigurasip-box-item-harga div:first-child {
                            font-size: 10px;
                            grid-column: 1/2;
                            grid-row: 1/2;
                        }

                        .konfigurasip-box-item-harga div:nth-child(2) {
                            font-weight: 600;
                            grid-column: 1/2;
                            grid-row: 2/3;
                        }

                        .konfigurasip-box-item-harga div:nth-child(3) {
                            grid-column: 2/3;
                            grid-row: 1/3;
                            width: 40px;
                            display: flex;
                            align-items: center;
                            justify-content: center;
                            color: #fff;
                            background: #585d79;
                            border-radius: 5px;
                        }

                    .konfigurasip-box-item-line {
                        grid-column: 1/2;
                        grid-row: 3/4;
                        width: 100%;
                        height: 1px;
                        background: #ddd;
                        margin-top: 5px;
                        margin-bottom: 5px;
                    }

                    .konfigurasip-box-item-total {
                        grid-column: 1/2;
                        grid-row: 4/5;
                    }

                        .konfigurasip-box-item-total div:first-child {
                            font-size: 10px;
                        }

                        .konfigurasip-box-item-total div:nth-child(2) {
                            font-size: 17px;
                            font-weight: 600;
                        }

                    .konfigurasip-box-item-jumlah {
                        grid-column: 2/3;
                        grid-row: 1/5;
                        width: 100%;
                        height: 100%;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        font-size: 16px;
                        font-weight: 600;
                    }

                    .konfigurasip-box-item-inc {
                        position: absolute;
                        width: 40px;
                        height: 30px;
                        background: #fff;
                        border-radius: 5px;
                        top: 10px;
                        right: 10px;
                        box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }

                    .konfigurasip-box-item-dec {
                        position: absolute;
                        width: 40px;
                        height: 30px;
                        background: #fff;
                        border-radius: 5px;
                        bottom: 10px;
                        right: 10px;
                        box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }

                    .konfigurasip-box-item-delete {
                        position: absolute;
                        width: 40px;
                        height: 40px;
                        background: #f44336;
                        border-radius: 5px;
                        top: calc(50% - 20px);
                        left: -20px;
                        box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        color: #fff;
                    }

                        .konfigurasip-box-item-inc i,
                        .konfigurasip-box-item-dec i {
                            font-size: 16px;
                        }

        .cari-bg {
            position: fixed;
            width: 100%;
            height: 100%;
            background: rgba(0,0,0,0.5);
            top: 0;
            left: 0;
            z-index: 999;
        }

            .cari-input {
                width: 100%;
                height: 50px;
                background: #fff;
                display: grid;
                grid-template-columns: 50px 1fr 50px;
                border-bottom: 1px solid #ddd;
            }

                .cari-input-icon,
                .cari-input-close {
                    width: 50px;
                    height: 50px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    color: #6b7192;
                }

                .cari-input-value {
                    outline: none;
                    border: 0;
                    border-bottom: 1px solid #ddd;
                }

            .cari-wrap {
                max-height: calc(100% - 50px);
                overflow-y: scroll;
            }

                .cari-item {
                    width: 100%;
                    height: 50px;
                    background: #fff;
                    border-bottom: 1px solid #ddd;
                    display: grid;
                    grid-template-columns: 1fr 50px;
                }

                    .cari-item-info {
                        display: flex;
                        justify-content: center;
                        flex-direction: column;
                        padding-left: 20px;
                    }

                        .cari-item-name {
                            font-size: 16px;
                            font-weight: 600;
                        }

                        .cari-item-harga {
                            font-size: 12px;
                        }

                    .cari-item-add {
                        width: 50px;
                        height: 50px;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        color: #4caf50;
                    }

        .charga-bg {
            position: fixed;
            width: 100%;
            height: 100%;
            background: rgba(0,0,0,0.5);
            top: 0;
            left: 0;
            z-index: 1000;
            display: flex;
            justify-content: center;
            align-items: flex-start;
            padding-top: 15px;
        }

            .charga-box {
                width: 90%;
                padding: 15px;
                background: #fff;
                border-radius: 5px;
            }

                .charga-title {
                    width: 100%;
                    font-size: 16px;
                    font-weight: 600;
                    margin-bottom: 5px;
                }

                .charga-box table {
                    font-size: 13px;
                    margin-bottom: 20px;
                }

        .apaket-bg {
            position: fixed;
            width: 100%;
            height: 100%;
            background: rgba(0,0,0,0.5);
            top: 0;
            left: 0;
            z-index: 1000;
            display: flex;
            justify-content: center;
            align-items: flex-start;
            padding-top: 15px;
        }

            .apaket-box {
                width: 90%;
                padding: 15px;
                background: #fff;
                border-radius: 5px;
            }

                .apaket-title {
                    width: 100%;
                    font-size: 16px;
                    font-weight: 600;
                    margin-bottom: 5px;
                }
    </style>
@endpush
