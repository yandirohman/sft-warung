@extends('warung.layout')

@section('content')
    <div class="notif-head">
        <a class="notif-head-back" href="{{ route('warung.home.index') }}">
            <i class="material-icons">arrow_back</i>
        </a>
        <div class="notif-head-text">Notifikasi</div>
    </div>
    <div class="notif">
        @if(count($datas) == 0)
            <div class="notif-item-null">- belum ada notifikasi -</div>
        @endif
        @foreach($datas as $data)
            <div class="notif-item">
                <div class="notif-item-title">{{ $data->judul }}</div>
                <div class="notif-item-text">{{ $data->keterangan }}</div>
                <div class="notif-item-date">{{ $data->created_at->format('d/m/Y H:i') }}</div>
            </div>
        @endforeach
    </div>
    @include('warung.component.menu')
@endsection

@push('script')
@endpush

@push('styles')
    <style>
        .notif-head {
            font-size: 20px;
            color: #6b7192;
            font-weight: 600;
            justify-self: start;
            display: flex;
            align-items: center;
            margin-top: 15px;
        }

            .notif-head-back {
                width: 40px;
                height: 40px;
                display: flex;
                align-items: center;
                justify-content: center;
                color: #6b7192;
                outline: none;
                text-decoration: none;
            }

            .notif-head-back:active,
            .notif-head-back:hover {
                background: #ddd;
                color: #6b7192;
                outline: none;
                text-decoration: none;
            }

        .notif {
            width: 100%;
            padding-top: 20px;
            padding-bottom: 100px;
        }

            .notif-item {
                width: 100%;
                height: max-content;
                padding: 10px 15px;
                background: #fff;
                border-radius: 5px;
                box-shadow: 0 3px 4px 0 rgba(0,0,0,0.14), 0 3px 3px -2px rgba(0,0,0,0.12), 0 1px 8px 0 rgba(0,0,0,0.20);
                margin-bottom: 15px;
            }

                .notif-item-title {
                    font-size: 14px;
                    font-weight: 600;
                }

                .notif-item-text {
                    font-size: 14px;
                }

                .notif-item-date {
                    font-size: 10px;
                    color: #888;
                    font-weight: 600;
                    margin-top: 10px;
                }

            .notif-item-null {
                width: 100%;
                color: #333;
                text-align: center;
                font-size: 13px;
            }

    </style>
@endpush
