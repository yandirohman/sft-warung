@extends('layouts.master-dinsos')

@section('title', 'Produk Dinsos')

@section('log', 'active')

@section('content-card')
  <div class="row mb-2">
    <form action="{{route('dinsos.produk-cari')}}" method="get" class="col-xl-12">

    </form>
  </div>
@stop

@section('content-non-card')
<div class="row">
  <div class="col-xl-12">
    <div class="card-custom">
      <div class="card-header">
          <p class="text-grey font-weight-bold"></p>
      </div>
      <div class="card-body">
        <div class="float-right mb-2">
          <a href="{{$logsPaginate->previousPageUrl()}}" class="btn btn-default" style="border-color: gray"> < </a>
          <span>{{$logsPaginate->currentPage()}}</span>
          <span>Dari</span>
          <span>{{$logsPaginate->lastPage()}}</span>
          <a href="{{$logsPaginate->nextPageUrl()}}" class="btn btn-default" style="border-color: gray"> > </a>
        </div>
        <table class="table table-striped">
          <thead>
            <tr>
              <th style="padding-top: 25px; padding-bottom: 25px;">No</th>
              <th style="padding-top: 25px; padding-bottom: 25px;">Nama</th>
              <th style="padding-top: 25px; padding-bottom: 25px;">Keterangan</th>
              <th style="padding-top: 25px; padding-bottom: 25px;">Dilakukan Saat</th>
              <th style="padding-top: 25px; padding-bottom: 25px;">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($logsPaginate as $index => $item)
                <tr>
                  <td>{{$index + 1}}</td>
                  <td>{{$item->nama}}</td>
                  <td>{!!$item->keterangan!!}</td>
                  <td>{{$item->created_at}}</td>
                  <td>
                    <a href="{{route('dinsos.logs.detail', $item->id)}}" class="btn btn-sm" style="border-radius: 3px; background-color: white; box-shadow: 0px 0px 5px grey;"><span class="fa fa-info"></span></a>
                  </td>
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop
