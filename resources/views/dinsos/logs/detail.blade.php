@extends('layouts.master-dinsos')

@section('title', 'Produk Dinsos')

@section('produk', 'active')

@section('content-card')

@stop

@section('content-non-card')
<div class="row">
  <div class="col-xl-12">
    <div class="card-custom">
      <div class="card-header">
          <p class="text-grey font-weight-bold"></p>
      </div>
      <div class="card-body">
        <div class="row">
          @if (empty($logs->data_sebelum || $logs->data_setelah))
          <span style="font-size: 24px;">{!!$logs->keterangan!!}</span> <br>
          @else
            <div class="col-md 6">
              <h3>Data Sebelum</h3>
              @foreach ($sebelum as $key => $value)
              @if ($key != 'id')
                <span style="font-size: 24px;">{{$key}} : {{$value}}</span> <br>
              @endif
              @endforeach
            </div>
            <div class="col-md 6">
              <h3>Data Sesudah  </h3>
              @foreach ($setelah as $key => $value)
              @if ($key != 'id')
                <span style="font-size: 24px;">{{$key}} : {{$value}}</span> <br>
              @endif
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@stop
