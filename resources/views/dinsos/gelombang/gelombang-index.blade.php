@extends('layouts.master-dinsos')

@section('title', 'Konfigurasi Gelombang')

@section('gelombang', 'active')

@section('content-card')
  <div class="row">
    <div class="col-12">
      <hr />
        <h5 class="text-grey font-weight-bold">Konfigurasi</h5>
      <hr />

      <div class="table-responsive mt-3">
        <table class="table table-hover table-striped">
          <thead>
            <tr class="text-center">
              <th>#</th>
              <th>Tahun</th>
              <th>Bulan</th>
              <th>Besaran Dana</th>
              <th>Status</th>
              <th>Opsi</th>
            </tr>
          </thead>

          <tbody>
            @foreach ($configGelombang as $no => $cg)
                <tr class="text-center">
                  <td>{{ $no + 1 }}</td>
                  <td>{{ $cg->tahun }}</td>
                  <td>{{ $cg->bulan }}</td>
                  <td>Rp {{number_format($cg->besaran_dana, 0, ',', '.')}}</td>
                  <td>
                    @if ($cg->is_active == 0)
                      <span class="badge badge-danger">Non Aktif</span>
                    @else
                      <span class="badge badge-success">Aktif</span>
                    @endif
                  </td>
                  <td>
                    <button type="button" class="btn btn-sm btn-grey-white" onclick="modalEdit({{ $cg->id }})"><i class="fas fa-edit"></i> Edit</button>
                  </td>
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>

    </div>
  </div>

  <!-- Modal Confirm -->
  <div class="modal fade" id="modalEdit">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Form Update</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body text-center">
          <form method="POST">
            @csrf
              <div class="form-group text-left">
                <label class="control-label">Status</label>
                <select name="is_active" id="is_active" class="form-control">
                  <option value="1">Aktif</option>
                  <option value="0">Non Aktif</option>
                </select>
              </div>

              <div class="form-group text-left">
                <label class="control-label">Besaran Dana</label>
                <input type="text" name="besaran_dana" id="besaran_dana" class="form-control">
              </div>
        </div>

        <div class="modal-footer">
            <div class="float-right">
              <button type="button" class="btn btn-sm btn-white-grey" data-dismiss="modal">Batal</button>
              <button type="sumit" class="btn btn-sm btn-grey-white">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop

@push('custom-js')
    <script>
      function modalEdit(id) {
        let urlEdit = "{{ route('dinsos.gelombang.edit', ':gelombang_id') }}"
        let urlUpdate = "{{ route('dinsos.gelombang.update', ':gelombang_id') }}"
        urlEdit = urlEdit.replace(':gelombang_id', id)
        urlUpdate = urlUpdate.replace(':gelombang_id', id)


        $.ajax({
          url : urlEdit,
          method : "GET",
          success : (data) => {
            $('#besaran_dana').val(data.besaran_dana)
            $('#modalEdit').modal('show')
            $('form').attr('action', urlUpdate)
          }
        })


      }
    </script>
@endpush
