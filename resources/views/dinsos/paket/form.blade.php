@extends('layouts.master-dinsos')

@section('title', 'Paket Sembako')

@section('paket', 'active')

@section('content-card')
  <div class="row">
    <div class="col-xl-12">
      <div class="d-flex justify-content-center">
        <div class="card-custom px-2" style="width: 500px">
          <div class="card-header">
              <center><p class="text-grey font-weight-bold" style="font-size: 24px;">Form Tambah Paket</p></center>
          </div>
          <div class="card-body">
              <form action="{{ route('dinsos.paket.store') }}" method="POST">
              @csrf
              <div class="row mb-4">
								<span><b>Nama Paket</b></span>
								<input type="text" class="form-control" placeholder="Masukan Nama Paket" name="nama" required>
              </div>
              <b style="margin-left: -10px">List Produk</b>
              <div class="row" style="position: relative">
                <input type="text" class="form-control col-md-10" placeholder="Cari Produk" id="search-value">
                <div class="btn btn-primary btn-sm col-md-1 ml-2" id="search"><span class="fa fa-search"></span></div>
								<div class="spoiler"></div>
              </div>
							<table id="list-produk" class="table table-sm mt-4"></table>
							<input type="hidden" name="jml_produk" value="0">
							<input type="hidden" name="list_produk" value="[]">
              <div class="row d-flex justify-content-end mt-5">
                <a href="{{ route('dinsos.paket.index') }}" class="btn mr-2" style="box-shadow: 0px 0px 5px grey;">Batal</a>
                <button type="submit" class="btn" style="background-color: gray; color: white;">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@push('custom-js')
	<script>
		$('#search').on('click', function() {
			let value = $('#search-value').val();
			$.ajax({
				url: "{{ route('dinsos.paket.search') }}",
				method: "GET",
				data: {
					str: value
				},
				success: data => {
					$('.spoiler').html('')
					data.forEach(item => {
						$('.spoiler').append(`
							<div 
								class="spoiler-item" 
								data-id="${item.id}" 
								data-nama="${item.nama}"
								data-harga="${item.harga}"
								data-satuan="${item.satuan}"
							>
								${item.nama}
							</div>
						`)
					})
				}
			})
		})

		let list_produk = [];
		let i = 0;
		$('.spoiler').on('click', '.spoiler-item', function() {
			let id = $(this).attr('data-id');
			let nama = $(this).attr('data-nama');
			let harga = $(this).attr('data-harga');
			let satuan = $(this).attr('data-satuan');
			$('.spoiler').html('');

			let item = {
				id_produk: id,
				nama: nama,
				satuan: satuan,
				harga: harga,
				jumlah: 1
			}
			list_produk[i] = item;
			i++;

			generateItem(list_produk);
		})

		function hapusProduk(i) {
			list_produk.splice(i, 1);
			generateItem(list_produk);
			console.log(list_produk)
		}

		$('#list-produk').on('change keyup', '.jumlah-item', function() {
			let jumlah = parseInt($(this).val());
			let index = $(this).attr('data-index');
			list_produk[index].jumlah = jumlah;
			$('input[name="list_produk"]').val(JSON.stringify(list_produk));
		})

		function generateItem() {
			$('#list-produk').html(`
				<tr>
					<th>Nama</th>
					<th>Harga</th>
					<th>Jumlah</th>
					<th>Opsi</th>
				</tr>
			`);
			list_produk.forEach((item, i) => {
				$('#list-produk').append(`
					<tr data-index="${i}">
						<td>${item.nama}</td>
						<td>${item.harga}</td>
						<td class="d-flex">
							<input data-index="${i}" type="number" class="jumlah-item form-control" style="width: 70px;" value="1"/>
						</td>
						<td>
							<div class="btn btn-sm btn-danger" onclick="hapusProduk(${i})">
								<span class="fa fa-trash"></span>
							</div>
						</td>
					</tr>
				`)
			})
			$('input[name="jml_produk"]').val(list_produk.length);
			$('input[name="list_produk"]').val(JSON.stringify(list_produk));
		}
	</script>
@endpush

@push('custom-css')
	<style>
		.spoiler {
			position: absolute;
			top: 42px;
			left: 0;
			width: 80%;
			height: max-content;
			background: #eee;
		}

			.spoiler-item {
				width: 100%;
				color: #333;
				padding: 5px 10px;
				cursor: pointer;
			}

			.spoiler-item:hover {
				background: #ddd;
			}
	</style>
@endpush

@section('content-non-card')
@stop
