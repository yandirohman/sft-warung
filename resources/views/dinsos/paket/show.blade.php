@extends('layouts.master-dinsos')

@section('title', 'Paket Sembako')

@section('paket', 'active')

@section('content-card')
  <div class="row mb-2">
    <div class="col-xl-12">
        <a href="{{ route('dinsos.paket.create') }}" class="btn btn-grey-white" style="border-radius: 3px;"><span class="fa fa-plus"></span> Tambah Paket</a>
    </div>
  </div>
@stop

@section('content-non-card')
	<div class="row">
		<div class="col-xl-12">
			<div class="card-custom">
				<div class="card-header">
                    <p class="text-grey font-weight-bold"></p>
				</div>
				<div class="card-body">
                    <h2>Detail Paket</h2>
					<table class="table table-striped">
						<thead>
							<tr>
								<th style="padding-top: 25px; padding-bottom: 25px;">No</th>
								<th style="padding-top: 25px; padding-bottom: 25px;">Nama Produk</th>
								<th style="padding-top: 25px; padding-bottom: 25px;">Harga</th>
                                <th style="padding-top: 25px; padding-bottom: 25px;">Jumlah</th>
							</tr>
						</thead>
						<tbody>
							@foreach($datas as $data)
								<tr>
									<td>{{ $loop->index + 1 }}</td>
									<td>{{ $data['nama'] }}</td>
									<td>Rp. {{ $data['harga'] }}</td>
                                    <td>{{ $data['jumlah'] }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop
