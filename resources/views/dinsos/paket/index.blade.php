@extends('layouts.master-dinsos')

@section('title', 'Paket Sembako')

@section('paket', 'active')

@section('content-card')
  <div class="row mb-2">
    <div class="col-xl-12">
      <a href="{{ route('dinsos.paket.create') }}" class="btn btn-grey-white" style="border-radius: 3px;"><span class="fa fa-plus"></span> Tambah Paket</a>
		</div>
  </div>
@stop

@section('content-non-card')
	<div class="modal fade" id="modal-hapus">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Anda Yakin !</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<div class="modal-body text-center">
					<p class="font-weight-bold">Anda Ingin Menghapus Data Paket Ini ?</p>
				</div>

				<div class="modal-footer">
					<form action="" method="POST">
						@csrf
						{{ method_field('DELETE') }}

						<div class="float-right">
							<button type="button" class="btn btn-sm btn-white-grey" data-dismiss="modal">Batal</button>
							<button type="sumit" class="btn btn-sm btn-grey-white">Hapus</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-12">
			<div class="card-custom">
				<div class="card-header">
						<p class="text-grey font-weight-bold"></p>
				</div>
				<div class="card-body">
					{!! Session::get('msg') !!}
					<table class="table table-striped">
						<thead>
							<tr>
								<th style="padding-top: 25px; padding-bottom: 25px;">No</th>
								<th style="padding-top: 25px; padding-bottom: 25px;">Nama Paket</th>
								<th style="padding-top: 25px; padding-bottom: 25px;">Total Harga</th>
								<th style="padding-top: 25px; padding-bottom: 25px;">Jumlah Produk</th>
								<th style="padding-top: 25px; padding-bottom: 25px;">Aksi</th>
							</tr>
						</thead>
						<tbody>
							@if(count($datas) == 0)
								<tr>
									<td colspan="5" align="center">- Paket Kosong -</td>
								</tr>
							@endif
							@foreach($datas as $data)
								<tr>
									<td>{{ $loop->index + 1 }}</td>
									<td>{{ $data->nama }}</td>
									<td>Rp. {{ $data->total_harga }}</td>
									<td>
										<a href="{{ route('dinsos.paket.show', ['id' => $data->id]) }}">{{ $data->jml_produk }} produk</a>
									</td>
									<td>
										<a 
											class="btn btn-danger btn-sm"
											href="#"
											onclick="confirmHapus({{ $data->id }})"
										>
											<span class="fa fa-trash"></span>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop

@push('custom-js')
	<script>
		function confirmHapus(id) {
			let url = "{{ route('dinsos.paket.destroy', ':warung_id') }}"
			url = url.replace(':warung_id', id)

			$('#modal-hapus').modal('show')
			$('form').attr('action', url)
		}
	</script>
@endpush
