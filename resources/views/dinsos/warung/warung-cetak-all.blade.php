@extends('layouts.master-dinsos')

@section('title', 'Warung Dinsos')

@section('warung', 'active')

@section('content-card')
<div class="row">
  <div class="col-12">
    <hr />
      <h5 class="text-grey font-weight-bold">Cetak Data Warung</h5>
    <hr />
    <button class="btn btn-sm btn-grey-white" type="button" onclick="cetak()">
      <i class="fa fa-qrcode"></i>
      Cetak Data
    </button>

    <div class="float-right">
      {{ $warungs->links() }}
    </div>
  </div>

  <div class="col-2"></div>

  <div class="col-4 mt-3" id="cetakData">
    <div class="grid-container">
     @foreach ($warungs as $warung)
     <div class="grid-item">
      <table>
        <tr>
          <td><strong>Nama Warung</strong></td>
          <td>:</td>
          <td>{{ $warung->nama_warung }}</td>
        </tr>
        <tr>
          <td><strong>Nama Pemilik</strong></td>
          <td>:</td>
          <td>{{ $warung->nama_pemilik }}</td>
        </tr>
        <tr>
          <td><strong>NIK</strong></td>
          <td>:</td>
          <td>{{ $warung->nik }}</td>
        </tr>
        <tr>
          <td><strong>No. Handphone</strong></td>
          <td>:</td>
          <td>{{ $warung->hp }}</td>
        </tr>
        <tr>
          <td><strong>Username</strong></td>
          <td>:</td>
          <td>{{ $warung->username }}</td>
        </tr>
        <tr>
          <td><strong>Password</strong></td>
          <td>:</td>
          <td>{{ $warung->kode_akses }}</td>
        </tr>
      </table>
     </div>
     @endforeach
    </div>
  </div>
</div>
@stop

@push('custom-js')
<script>
  function cetak() {
      let style = $('#style-data').html();
      printJS({
          printable: 'cetakData',
          type: 'html',
          style: style,
          header: false
      })
  }
</script>
@endpush

@push('custom-css')
<style id="style-data">
  .grid-container {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 20px;
  }

  .grid-item {
    background-color: rgba(255, 255, 255, 0.8);
    border: 1px solid rgba(0, 0, 0, 0.8);
    padding: 20px;
    min-width: 400px;
  }
</style>
@endpush

