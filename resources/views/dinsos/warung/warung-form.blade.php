@extends('layouts.master-dinsos')

@section('title', 'Warung Dinsos Form')

@section('warung', 'active')

@section('content-card')
  <div class="row">
    <div class="col-12">
      <hr />
        <h5 class="text-grey font-weight-bold">Form Warung</h5>
      <hr />
      <form action="{{ $actionUrl }}" method="POST">
        @csrf

        @if ($warung->id)
          {{ method_field('PUT') }}
        @endif

        <div class="row">
          <div class="col-xl-6">
            <x-custom.custom-input type="text" name="nama_warung" label="Nama Warung" value="{{ $warung->nama_warung }}" :required="false" />
          </div>
          <div class="col-xl-6">
            <x-custom.custom-input type="text" name="nama_pemilik" label="Nama Pemilik" value="{{ $warung->nama_pemilik }}" :required="false" />
          </div>
          <div class="col-xl-6">
            <x-custom.custom-input type="number" name="nik" label="NIK" value="{{ $warung->nik }}" :required="false" />
          </div>
          <div class="col-xl-6">
            <x-custom.custom-input type="number" name="hp" label="Nomor HP" value="{{ $warung->hp }}" :required="false" />
          </div>
          <div class="col-12">
            <x-custom.custom-input type="textarea" name="alamat" label="Alamat Lengkap" value="{{ $warung->alamat }}" :required="false" />
          </div>
          <div class="col-xl-4">
            <div class="form-group">
              <div class="col-xl-12">
                <select name="id_kab" id="id_kab" class="form-control">
                  <option hidden>-- Silahkan Pilih Kabupaten --</option>
                  @foreach ($kabupaten as $kab)
                      <option value="{{ $kab->id_kab }}">{{ $kab->nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-xl-4">
            <div class="form-group">
              <div class="col-xl-12">
                <select name="id_kec" id="id_kec" class="form-control">
                  <option hidden>-- Silahkan Pilih Kecamatan --</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xl-4">
            <div class="form-group">
              <div class="col-xl-12">
                <select name="id_kel" id="id_kel" class="form-control">
                  <option hidden>-- Silahkan Pilih Kelurahan --</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-12 mt-3">
            <div class="float-right">
              <a href="{{ route('dinsos.warung.index') }}" class="btn btn-white-grey">Batal</a>
              <button type="submit" class="btn btn-grey-white">Simpan</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@stop

@push('custom-js')
    <script>
      $(() => {
        // Get Kecamatan
        $('#id_kab').on('change', function () {
            let kabId = $(this).val();
            let urlKec = "{{ route('dinsos.get.kecamatan', ':kabId') }}"
            urlKec = urlKec.replace(':kabId', kabId)

            if (kabId) {
                $.ajax({
                    url: urlKec,
                    type: "GET",
                    success: function (response) {
                        $('#id_kec').empty();
                        $('#id_kec').append('<option hidden>-- Silahkan Pilih Kecamatan --</option>');
                        $.each(response, function (key, data) {
                            $('#id_kec').append('<option value="' + data.id_kec + '">' + data.nama + '</option>');
                        });
                    },
                });
            } else {
                $('#id_kec').append('<option value="">Pilih Kecamatan</option>');
            }
        })

        // Get Kelurahan
        $('#id_kec').on('change', function () {
            let kecId = $(this).val();
            let urlKec = "{{ route('dinsos.get.kelurahan', ':kecId') }}"
            urlKec = urlKec.replace(':kecId', kecId)

            if (kecId) {
                $.ajax({
                    url: urlKec,
                    type: "GET",
                    success: function (response) {
                        $('#id_kel').empty();
                        $('#id_kel').append('<option hidden>-- Silahkan Pilih Kelurahan --</option>');
                        $.each(response, function (key, data) {
                            $('#id_kel').append('<option value="' + data.id_kel + '">' + data.nama + '</option>');
                        });
                    },
                });
            } else {
                $('#id_kel').append('<option value="">Pilih Kelurahan</option>');
            }
        })
      })
    </script>
@endpush
