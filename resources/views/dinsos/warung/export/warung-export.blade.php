<table>
    <thead>
        <tr>
            <th style="border:1px solid black; font-weight: bold; text-align: center">ID Warung</th>
            <th style="border:1px solid black; font-weight: bold; text-align: center">Nama Warung</th>
            <th style="border:1px solid black; font-weight: bold; text-align: center">Nama Pemilik</th>
            <th style="border:1px solid black; font-weight: bold; text-align: center">NIK Pemilik</th>
            <th style="border:1px solid black; font-weight: bold; text-align: center">No Whatsapp</th>
            <th style="border:1px solid black; font-weight: bold; text-align: center">Alamat Penerima</th>
            <th style="border:1px solid black; font-weight: bold; text-align: center">Tanggal Terdaftar</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($warung as $item)
            <tr>
                <td style="border:1px solid black; text-align: center">{{$item->id}}</td>
                <td style="border:1px solid black; ">{{$item->nama_warung}}</td>
                <td style="border:1px solid black; ">{{$item->nama_pemilik}}</td>
                <td style="border:1px solid black; ">'{{$item->nik}}</td>
                <td style="border:1px solid black; text-align: center">{{$item->hp}}</td>
                <td style="border:1px solid black; ">{{$item->alamat}}</td>
                <td style="border:1px solid black; ">{{$item->created_at}}</td>
            </tr>
        @empty
            <tr>
                <td colspan="7" style="border:1px solid black; ">Tidak Ada Data Warung</td>
            </tr>
        @endforelse
    </tbody>
</table>
