@extends('layouts.master-dinsos')

@section('title', 'Warung Dinsos')

@section('warung', 'active')

@section('content-card')
  <div class="row">
    <div class="col-12">
      <a href="{{ route('dinsos.warung.create') }}" class="btn btn-sm btn-grey-white"><i class="fas fa-plus"></i> Tambah Warung</a>
      <a href="{{ route('dinsos.warung.export') }}" class="btn btn-sm btn-grey-white"><i class="fas fa-file-excel"></i> Export Data</a>
      <a href="{{ route('dinsos.warung.form-import') }}" class="btn btn-sm btn-grey-white"><i class="fas fa-file-import"></i> Import Data</a>
      <a href="{{ asset('template-excel/template-import-warung.xlsx') }}" class="btn btn-sm btn-grey-white"><i class="fas fa-file-download"></i> Unduh Format</a>

      <form action="{{ route('dinsos.warung.cari-warung') }}" method="GET">
        @csrf

        <div class="input-group mb-3">
          <input type="text" class="form-control form-control-sm" name="dataCari" placeholder="Masukan Nama Warung">
          <div class="input-group-append">
            <button class="btn btn-sm btn-grey-white" type="submit"> <i class="fas fa-search"></i> </button>
          </div>
        </div>
      </form>

      <div class="float-right">
        <div class="control-page">
          <!-- Pagination -->
          <a href="{{ $warung->previousPageUrl() }}" class="btn btn-sm btn-default span-box"> <i class="fas fa-chevron-left"></i> </a>
          <span class="mr-1 ml-1">{{ $warung->currentPage() }}</span>
          <span>dari</span>
          <span class="mr-1 ml-1">{{ $warung->lastPage() }}</span>
          <a href="{{ $warung->nextPageUrl() }}" class="btn btn-sm btn-default span-box"> <i class="fas fa-chevron-right"></i> </a>

          <!-- Filter -->
          <span class="ml-2">Filter</span>
          <button type="button" class="btn btn-sm span-box" id="showFilter"> <i class="fas fa-filter"></i> </button>
        </div>
      </div>

      <a href="{{ route('dinsos.warung.cetak-warung-all') }}" class="btn btn-sm btn-grey-white"><i class="fas fa-qrcode"></i> Cetak Data All</a>
    </div>
  </div>

  <!-- Form filter -->
  <div class="filter" id="filterKel">
    <div class="float-right">
      <button type="button" class="btn btn-sm btn-grey-white mr-2" id="hideFilter"> <i class="fas fa-eye-slash"></i> </button>
    </div>
    <h5 class="mt-2">Filter Warung</h5>

    <div class="row mt-3">
      <div class="col-xl-4">
        <div class="form-group">
          <select name="kab" id="kab" class="form-control form-control-sm">
            <option hidden>-- Silahkan Pilih Kabupaten --</option>
            @foreach ($kabupaten as $kab)
                <option value="{{ $kab->id_kab }}">{{ $kab->nama }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-xl-4">
        <div class="form-group">
          <select name="kec" id="kec" class="form-control form-control-sm">
            <option hidden>-- Silahkan Pilih Kecamatan --</option>
          </select>
        </div>
      </div>
      <div class="col-xl-4">
        <div class="form-group">
          <select name="kel" id="kel" class="form-control form-control-sm">
            <option hidden>-- Silahkan Pilih Kelurahan --</option>
          </select>
        </div>
      </div>
    </div>
  </div>

  <!-- Table Data Warung -->
  <div class="table-responsive mt-3">
    <table class="table table-hover table-striped">
      <thead>
        <tr class="text-center">
          <th>Id Warung</th>
          <th>Nama Warung</th>
          <th>Nama Pemilik</th>
          <th>NIK Pemilik</th>
          <th>No Whatsapp</th>
          <th>Tanggal Terdaftar</th>
          <th>Aksi</th>
        </tr>
      </thead>

      <tbody>
        @forelse ($warung as $no => $w)
          <tr class="text-center">
            <td>{{ $no + 1 }}</td>
            <td>{{ $w->nama_warung }} </td>
            <td>{{ $w->nama_pemilik }}</td>
            <td>{{ $w->nik }}</td>
            <td>{{ $w->hp }}</td>
            <td>{{ date('d-m-Y', strtotime($w->created_at)) }}</td>
            <td>
              <a href="{{ route('dinsos.warung.cetak-warung', $w->id) }}" class="btn btn-sm btn-grey-white f-10"><i class="fas fa-qrcode"></i></a>
              <a href="{{ route('dinsos.warung.show', $w->id) }}" class="btn btn-sm btn-white-grey"><i class="fas fa-info-circle"></i></a>
              <a href="{{ route('dinsos.warung.edit', $w->id) }}" class="btn btn-sm btn-grey-white f-10"><i class="fas fa-edit"></i></a>
              <button type="button" class="btn btn-sm btn-white-grey" onclick="modalConfirm({{ $w->id }})"><i class="fas fa-trash"></i></button>
            </td>
          </tr>
        @empty
            <tr class="text-center">
              <td colspan="7">- Data Warung Belum Ada -</td>
            </tr>
        @endforelse
      </tbody>
    </table>
  </div>

  <!-- Modal Confirm -->
  <div class="modal fade" id="modalConfirm">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Anda Yakin !</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body text-center">
          <p class="font-weight-bold">Anda Ingin Menghapus Data Warung Ini ?</p>
        </div>

        <div class="modal-footer">
          <form action="" method="POST">
            @csrf
            {{ method_field('DELETE') }}

            <div class="float-right">
              <button type="button" class="btn btn-sm btn-white-grey" data-dismiss="modal">Batal</button>
              <button type="sumit" class="btn btn-sm btn-grey-white">Hapus</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop

@push('custom-js')
    <script>
      $(() => {
        if ("{{ $kelId }}") {
          $('#filterKel').show()
        } else {
          $('#filterKel').hide()
        }

        // Show Filter
        $('#showFilter').on('click', function () {
          $('#filterKel').show()
        })

        // Hide Filter
        $('#hideFilter').on('click', function () {
          $('#filterKel').hide()
        })

        // Get Kecamatan
        $('#kab').on('change', function () {
            let kabId = $(this).val();
            let urlKec = "{{ route('dinsos.get.kecamatan', ':kabId') }}"
            urlKec = urlKec.replace(':kabId', kabId)

            if (kabId) {
                $.ajax({
                    url: urlKec,
                    type: "GET",
                    success: function (response) {
                        $('#kec').empty();
                        $('#kec').append('<option hidden>-- Silahkan Pilih Kecamatan --</option>');
                        $.each(response, function (key, data) {
                            $('#kec').append('<option value="' + data.id_kec + '">' + data.nama + '</option>');
                        });
                    },
                });
            } else {
                $('#kec').append('<option value="">Pilih Kecamatan</option>');
            }
        })

        // Get Kelurahan
        $('#kec').on('change', function () {
            let kecId = $(this).val();
            let urlKec = "{{ route('dinsos.get.kelurahan', ':kecId') }}"
            urlKec = urlKec.replace(':kecId', kecId)

            if (kecId) {
                $.ajax({
                    url: urlKec,
                    type: "GET",
                    success: function (response) {
                        $('#kel').empty();
                        $('#kel').append('<option hidden>-- Silahkan Pilih Kelurahan --</option>');
                        $.each(response, function (key, data) {
                            $('#kel').append('<option value="' + data.id_kel + '">' + data.nama + '</option>');
                        });
                    },
                });
            } else {
                $('#kel').append('<option value="">Pilih Kelurahan</option>');
            }
        })

        $('#kel').on('change', function () {
            let val = $(this).val()
            window.location = '/dinsos/warung?kel=' + val
        })

      })

      function modalConfirm(id) {
        let url = "{{ route('dinsos.warung.destroy', ':warung_id') }}"
        url = url.replace(':warung_id', id)

        $('#modalConfirm').modal('show')
        $('form').attr('action', url)
      }
    </script>
@endpush

@push('custom-css')
    <style>
      form {
        display: inline-block;
      }

      .span-box {
        border-color: gray;
        border-radius: 5px;
      }
      .control-page {
        margin: 0 10px;
      }

      @media only screen and (max-width: 600px) {
        .control-page {
          margin: 10px;
        }

        a {
          margin-bottom: 10px;
        }

        form {
          width: 100%;
        }

        .btn-grey-white {
          min-width: 49%;
        }
      }

      @media only screen and (min-width: 1366px) {
        form {
          margin-left: 2px;
          width: 22%;
        }
      }

      @media only screen and (min-width: 1920px) {
        form {
          margin-left: 10px;
          width: 45%;
        }
      }
    </style>
@endpush
