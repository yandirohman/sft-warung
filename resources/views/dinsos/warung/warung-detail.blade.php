@extends('layouts.master-dinsos')

@section('title', 'Warung Dinsos Form')

@section('warung', 'active')

@section('content-card')
  <div class="row">
    <div class="col-12">
      <hr />
        <h5 class="text-grey font-weight-bold">Detail Warung</h5>
      <hr />

      <p><strong>Nama Warung</strong> : {{ $warung->nama_warung }}</p>
      <p><strong>Username Warung</strong> : {{ $warung->username }}</p>
      <p><strong>Nama Pemilik</strong> : {{ $warung->nama_pemilik }}</p>
      <p><strong>NIK Pemilik</strong> : {{ $warung->nik }}</p>
      <p><strong>Nomor HP</strong> : {{ $warung->hp }}</p>
      <p><strong>Tanggal Terdaftar</strong> : {{ date('d-m-Y', strtotime($warung->created_at)) }}</p>
      <p><strong>Alamat</strong> : {{ $warung->alamat }}</p>
    </div>
  </div>
@stop
