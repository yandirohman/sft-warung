@extends('layouts.master-dinsos')

@section('title', 'Produk Dinsos')

@section('produk', 'active')

@section('content-card')
  <div class="row">
    <div class="col-xl-12">
      <div class="d-flex justify-content-center">
        <div class="card-custom px-2" style="width: 500px">
          <div class="card-header">
              <center><p class="text-grey font-weight-bold" style="font-size: 24px;">Form {{empty($id) ? 'Tambah' : 'Ubah'}} Produk</p></center>
          </div>
          <div class="card-body">
            @if (empty($id))
              <form action="{{route('dinsos.produk-insert')}}" method="post" onsubmit="return validForm();">
            @else
              <form action="{{route('dinsos.produk-update', $id)}}" method="post" onsubmit="return validForm();">
            @endif
              @csrf
              <div class="row mb-4">
                <span><b>Nama Produk</b></span>
                <input type="text" class="form-control" placeholder="Masukan Nama Produk" name="nama" value="{{$listProduk->nama ?? ''}}" required>
              </div>
              <div class="row mb-4">
                <span><b>Harga Terendah</b></span>
                <input type="number" min="0" class="form-control" placeholder="Masukan Harga Terendah" value="{{$listProduk->harga_terendah ?? ''}}" name="harga_terendah" required>
              </div>
              <div class="row mb-4">
                <span><b>Harga Tertinggi</b></span>
                <input type="number" min="0" class="form-control" placeholder="Masukan Harga Tertinggi" value="{{$listProduk->harga_tertinggi ?? ''}}" name="harga_tertinggi" required>
              </div>
              <div class="row mb-4">
                <span><b>Satuan Produk</b></span>
                <select class="form-control" placeholder="Masukan Satuan Produk" name="satuan" required>
                  <option value="">--Pilih Satuan--</option>
                  <option value="Kg" {{($listProduk->satuan ?? '') == 'Kg' ? 'selected' : ''}}>Kg</option>
                  <option value="Liter" {{($listProduk->satuan ?? '') == 'Liter' ? 'selected' : ''}}>Liter</option>
                  <option value="Unit" {{($listProduk->satuan ?? '') == 'Unit' ? 'selected' : ''}}>Unit</option>
                </select>
              </div>
              <div class="row mb-4">
                <div class="col-md-12">
                  <center>
                    @foreach ($icon as $item)
                    <label class="mr-4">
                      <input type="radio" name="foto_produk" value="{{$item['icon']}}" {{($listProduk->foto_produk ?? '') == $item['icon'] ? 'checked' : ''}} onclick="valid('{{$item['icon']}}')" class="icon_input">
                      <img src="{{asset('img/icon/'. $item['icon'])}}" alt="img" width="50" class="img_icon">
                      <br>{{ $item['nama'] }}
                    </label>
                    @endforeach
                    <div class="alert alert-danger alert-icon"></div>
                  </center>
                </div>
              </div>
              <div class="row d-flex justify-content-end">
                <a href="{{route('dinsos.produk-view')}}" class="btn mr-2" style="box-shadow: 0px 0px 5px grey;">Batal</a>
                <button type="submit" class="btn" style="background-color: gray; color: white;">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@push('custom-js')
  <script>
    $(document).ready(function(){
      $('.alert-icon').hide()
    });

    let icon = null

    function validForm()
    {
      if (icon == null)
      {
        $('.alert-icon').show()
        $('.alert-icon').html('Silahkan Pilih Icon!')
        setTimeout(function(){
          $('.alert-icon').hide()
        }, 3000);
        return false;
      }
      else
      {
        return true;
      }
    }

    function valid(iconNya)
    {
      icon = iconNya
    }
  </script>
@endpush
@push('custom-css')
  <style>
    .icon_input
    {
      position: absolute;
      opacity: 0;
      width: 0;
      height: 0;
    }

    .icon_input+.img_icon
    {
      cursor: pointer;
    }

    .icon_input:checked+.img_icon
    {
      outline: 2px solid #428bca;
    }
  </style>
@endpush
