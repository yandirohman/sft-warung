@extends('layouts.master-dinsos')

@section('title', 'Produk Dinsos')

@section('produk', 'active')

@section('content-card')
  <div class="row mb-2">
    <form action="{{route('dinsos.produk-cari')}}" method="get" class="col-xl-12">
      @csrf
      <a href="{{route('dinsos.produk-form')}}" class="btn btn-grey-white" style="border-radius: 3px;"><span class="fa fa-plus"></span> Tambah Produk</a>
      <input name="cari" type="text" placeholder="Cari Berdasarkan Nama Produk" style="padding: .375rem .75rem; width:40%; font-weight: 400; line-height: 1.5; color: #6e707e; border: 1px solid #d1d3e2; border-radius: .25rem; width: 60%;" required>
      <button type="submit" class="fa fa-search btn btn-grey-white btn-lg" style="border-radius: 3px;"></button>
      <div class="float-right">
        <a href="{{$listProduk->previousPageUrl()}}" class="btn btn-default" style="border-color: gray"> < </a>
        <span>{{$listProduk->currentPage()}}</span>
        <span>Dari</span>
        <span>{{$listProduk->lastPage()}}</span>
        <a href="{{$listProduk->nextPageUrl()}}" class="btn btn-default" style="border-color: gray"> > </a>
      </div>
    </form>
  </div>
@stop

@section('content-non-card')
  <div class="row">
    <div class="col-xl-12">
      <div class="card-custom">
        <div class="card-header">
            <p class="text-grey font-weight-bold"></p>
        </div>
        <div class="card-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th style="padding-top: 25px; padding-bottom: 25px;">No</th>
                <th style="padding-top: 25px; padding-bottom: 25px;">Nama Produk</th>
                <th style="padding-top: 25px; padding-bottom: 25px;">Harga Terendah</th>
                <th style="padding-top: 25px; padding-bottom: 25px;">Harga Tertinggi</th>
                <th style="padding-top: 25px; padding-bottom: 25px;">Satuan</th>
                <th style="padding-top: 25px; padding-bottom: 25px;">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($listProduk as $index => $item)
                <tr>
                  <td>{{$index + 1}}</td>
                  <td><img src="{{asset('img/icon/'.$item->foto_produk)}}" alt="img" width="50" class="img_icon">{{$item->nama}}</td>
                  <td>Rp {{number_format($item->harga_terendah, 0, ',', '.')}}</td>
                  <td>Rp {{number_format($item->harga_tertinggi, 0, ',', '.')}}</td>
                  <td>{{$item->satuan}}</td>
                  <td>
                    <a href="{{route('dinsos.produk-edit', $item->id)}}" class="btn btn-grey-white btn-sm" style="border-radius: 3px;"><span class="fa fa-edit"></span></a>
                    {{-- <a href="{{route('dinsos.produk-delete', $item->id)}}" class="btn btn-sm" style="border-radius: 3px; background-color: white; box-shadow: 0px 0px 5px grey;"><span class="fa fa-trash"></span></a> --}}
                    <button type="button" data-toggle="modal" data-target="#modalConfirm{{$item->id}}" class="btn btn-sm" style="border-radius: 3px; background-color: white; box-shadow: 0px 0px 5px grey;"><span class="fa fa-trash"></span></button>
                  </td>
                  <div class="modal fade" id="modalConfirm{{$item->id}}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">Anda Yakin !</h5>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body text-center">
                          <p class="font-weight-bold">Anda Ingin Menghapus Produk Dengan Nama {{$item->nama}} ?</p>
                        </div>
                        <div class="modal-footer">
                          <div class="float-right">
                            <button type="button" class="btn btn-sm btn-white-grey" data-dismiss="modal">Batal</button>
                            <a href="{{route('dinsos.produk-delete', $item->id)}}" class="btn btn-sm btn-grey-white">Hapus</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@stop
