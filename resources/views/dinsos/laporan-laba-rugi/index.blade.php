@extends('layouts.master-dinsos', ['laporanLaba' => true])

@section('title', 'Dashboard Dinsos')

@section('laporan-laba-rugi', 'active')

@section('content-card')
    <div class="row">
        <div class="col-12 d-flex">
            {{-- <a href="{{ route('dinsos.warung.export') }}" class="btn btn-sm btn-grey-white"><i class="fas fa-file-excel"></i> Export Data</a> --}}

            <form action="#" method="GET">
                <div class="input-group mb-3">
                    <input type="text" class="form-control form-control-sm" name="dataCari" placeholder="Masukan Nama Warung" id="cari">
                    <div class="input-group-append">
                        <button class="btn btn-sm btn-grey-white" type="button" onclick="cariWarung()"> <i class="fas fa-search"></i> </button>
                    </div>
                </div>
            </form>

            <div class="control-page">
                <!-- Pagination -->
                <a href="{{ $prevPage }}" class="btn btn-sm btn-default span-box"> <i class="fas fa-chevron-left"></i> </a>
                <span class="mr-1 ml-1">{{ $currentPage }}</span>
                <span>dari</span>
                <span class="mr-1 ml-1">{{ $lastPage }}</span>
                <a href="{{ $nextPage }}" class="btn btn-sm btn-default span-box"> <i class="fas fa-chevron-right"></i> </a>

                <!-- Filter -->
                <span class="ml-2">Filter</span>
                <button type="button" class="btn btn-sm span-box" id="showFilter"> <i class="fas fa-filter"></i> </button>
            </div>
        </div>
    </div>

    <!-- Form filter -->
    <div class="filter" id="filterKel">
        <div class="float-right">
            <button type="button" class="btn btn-sm btn-grey-white mr-2" id="hideFilter"> <i class="fas fa-eye-slash"></i> </button>
        </div>
        <h5 class="mt-2">Filter Warung</h5>

        <div class="row mt-3">
        <div class="col-xl-4">
            <div class="form-group">
            <select name="kab" id="kab" class="form-control form-control-sm">
                <option hidden>-- Silahkan Pilih Kabupaten --</option>
                @foreach ($kabupaten as $kab)
                    <option value="{{ $kab->id_kab }}">{{ $kab->nama }}</option>
                @endforeach
            </select>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="form-group">
            <select name="kec" id="kec" class="form-control form-control-sm">
                <option hidden>-- Silahkan Pilih Kecamatan --</option>
            </select>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="form-group">
            <select name="kel" id="kel" class="form-control form-control-sm">
                <option hidden>-- Silahkan Pilih Kelurahan --</option>
            </select>
            </div>
        </div>
        </div>
    </div>

    <!-- Table Data Warung -->
    <div class="table-responsive mt-3">
      <table class="table table-hover table-striped">
        <thead>
          <tr class="text-center">
            <th>No.</th>
            <th>Nama Warung</th>
            <th>Nama Pemilik</th>
            <th>Modal</th>
            <th>Penjualan</th>
            <th>Laba</th>
            <th>Piutang</th>
            <th>Uang Yang Diterima</th>
            <th>Laba Yang Diterima</th>
          </tr>
        </thead>

        <tbody>
          @forelse ($warung as $no => $w)
            <tr class="text-center">
              <td>{{ $no + 1 }}</td>
              <td>{{ $w->nama_warung }} </td>
              <td>{{ $w->nama_pemilik }}</td>
              <td>Rp {{ number_format($w->modal,2,',','.')}}</td>
              <td>RP {{ number_format($w->terjual,2,',','.')}}</td>
              <td>Rp {{ number_format($w->laba_rugi > 0 ?? 0,2,',','.')}}</td>
              <td>Rp {{ number_format($w->piutang,2,',','.')}}</td>
              <td>Rp {{ number_format($w->sudah_cair,2,',','.')}}</td>
              <td>Rp {{ number_format(($w->terima_laba > 0 ? $w->terima_laba : 0) ,2,',','.')}}</td>
            </tr>
          @empty
              <tr class="text-center">
                <td colspan="7">Data Warung Belum Ada</td>
              </tr>
          @endforelse
        </tbody>
      </table>
    </div>
@stop

@section('pie-laporan-laba')
    <div class="card-custom">
        <div class="card-header">
            <h4 class="title pt-2" style="font-size: 16px">Akumulasi Laporan Laba</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <p class="col-5">Total Modal</p>
                <p class="col-7">Rp {{number_format($akumulasi['modal'],0,',','.')}}</p>
            </div>
            <div class="row">
                <p class="col-5">Total Penjualan</p>
                <p class="col-7">Rp {{number_format($akumulasi['terjual'],0,',','.')}}</p>
            </div>
            <div class="row">
                <p class="col-5">Total Laba</p>
                <p class="col-7">Rp {{number_format($akumulasi['laba_rugi'],0,',','.')}}</p>
            </div>
            <div class="row">
                <p class="col-5">Total Piutang</p>
                <p class="col-7">Rp {{number_format($akumulasi['piutang'],0,',','.')}}</p>
            </div>
            <div class="row">
                <p class="col-5">Total Uang Yang Diterima</p>
                <p class="col-7">Rp {{number_format($akumulasi['sudah_cair'],0,',','.')}}</p>
            </div>
            <hr>
            <div id="pie-akumulasi-laporan"></div>
        </div>
    </div>
@endsection

@push('custom-js')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    <script>
        $(() => {
            pieAkumulasi();

            if ("{{ $kelId }}") {
            $('#filterKel').show()
            } else {
            $('#filterKel').hide()
            }

            // Show Filter
            $('#showFilter').on('click', function () {
            $('#filterKel').show()
            })

            // Hide Filter
            $('#hideFilter').on('click', function () {
            $('#filterKel').hide()
            })

            // Get Kecamatan
            $('#kab').on('change', function () {
                let kabId = $(this).val();
                let urlKec = "{{ route('dinsos.get.kecamatan', ':kabId') }}"
                urlKec = urlKec.replace(':kabId', kabId)

                if (kabId) {
                    $.ajax({
                        url: urlKec,
                        type: "GET",
                        success: function (response) {
                            $('#kec').empty();
                            $('#kec').append('<option hidden>-- Silahkan Pilih Kecamatan --</option>');
                            $.each(response, function (key, data) {
                                $('#kec').append('<option value="' + data.id_kec + '">' + data.nama + '</option>');
                            });
                        },
                    });
                } else {
                    $('#kec').append('<option value="">Pilih Kecamatan</option>');
                }
            })

            // Get Kelurahan
            $('#kec').on('change', function () {
                let kecId = $(this).val();
                let urlKec = "{{ route('dinsos.get.kelurahan', ':kecId') }}"
                urlKec = urlKec.replace(':kecId', kecId)

                if (kecId) {
                    $.ajax({
                        url: urlKec,
                        type: "GET",
                        success: function (response) {
                            $('#kel').empty();
                            $('#kel').append('<option hidden>-- Silahkan Pilih Kelurahan --</option>');
                            $.each(response, function (key, data) {
                                $('#kel').append('<option value="' + data.id_kel + '">' + data.nama + '</option>');
                            });
                        },
                    });
                } else {
                    $('#kel').append('<option value="">Pilih Kelurahan</option>');
                }
            })

            $('#kel').on('change', function () {
                cariWarung();
            })

        })

        function cariWarung(){
            var cari = $('#cari').val();
            var kelurahan = $('#kel').val();
            var url  = "{{route('dinsos.laporan-laba-rugi')}}?";

            if(cari){
                url += 'cari=' + cari;
            }

            if(kelurahan != '-- Silahkan Pilih Kelurahan --'){
                url += '&kelurahan=' + kelurahan;
            }
            window.location = url;
        }

        function pieAkumulasi(){
            var options = {
                    series: [{{$akumulasi['modal']}}, {{$akumulasi['terjual']}}, {{$akumulasi['laba_rugi']}}, {{$akumulasi['piutang']}}, {{$akumulasi['sudah_cair']}}],
                    chart: {
                    width: 380,
                    type: 'pie',
                },
                labels: ['Modal', 'Penjualan', 'Laba', 'Piutang', 'Rembes'],
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }],
                tooltip: {
                    y: {
                        formatter: function (val) {
                            return formatRupiah(String(val))
                        }
                    }
                }
            };

            var chart = new ApexCharts(document.querySelector("#pie-akumulasi-laporan"), options);
            chart.render();
        }


        function formatRupiah(angka, prefix = "Rp. "){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split  = number_string.split(','),
            sisa   = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                var separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

    </script>
@endpush

@push('custom-css')
    <style>
    form {
        display: inline-block;
    }

    .span-box {
        border-color: gray;
        border-radius: 5px;
    }
    .control-page {
        margin: 0 10px;
    }

    @media only screen and (max-width: 600px) {
        .control-page {
        margin: 10px;
        }

        a {
        margin-bottom: 10px;
        }

        form {
        width: 100%;
        }

        .btn-grey-white {
        min-width: 49%;
        }
    }

    @media only screen and (min-width: 1366px) {
        form {
        margin-left: 2px;
        width: 22%;
        }
    }

    @media only screen and (min-width: 1920px) {
        form {
        margin-left: 10px;
        width: 45%;
        }
    }
    </style>
@endpush
