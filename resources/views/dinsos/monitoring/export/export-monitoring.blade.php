<table>
    <thead>
        <tr>
            <th style="border:1pt solid black; font-size:12px; background:#D9D9D9; text-align:center;">No</th>
            <th style="border:1pt solid black; font-size:12px; background:#D9D9D9; text-align:center;">Warung</th>
            <th style="border:1pt solid black; font-size:12px; background:#D9D9D9; text-align:center;">Deposit</th>
            <th style="border:1pt solid black; font-size:12px; background:#D9D9D9; text-align:center;">Pencairan</th>
            <th style="border:1pt solid black; font-size:12px; background:#D9D9D9; text-align:center;">Tidak Cair</th>
            <th style="border:1pt solid black; font-size:12px; background:#D9D9D9; text-align:center;">Sisa Pencairan</th>
            <th style="border:1pt solid black; font-size:12px; background:#D9D9D9; text-align:center;">Alamat</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($dataWarung as $warung)
            <tr>
                <td style="border:1pt solid black; font-size:10px; text-align:center">{{$loop->iteration}}</td>
                <td style="border:1pt solid black; font-size:10px">{{$warung->nama_warung}}</td>
                <td style="border:1pt solid black; font-size:10px">{{$warung->saldo_total}}</td>
                <td style="border:1pt solid black; font-size:10px">{{$warung->saldo_rembes}}</td>
                <td style="border:1pt solid black; font-size:10px; text-align:center">-</td>
                <td style="border:1pt solid black; font-size:10px">{{$warung->saldo_total - $warung->saldo_rembes}}</td>
                <td style="border:1pt solid black; font-size:10px">{{$warung->alamat}}</td>
            </tr>
        @empty
            <tr>
                <td colspan="6">Tidak Ada Data</td>   
            </tr>   
        @endforelse
    </tbody>
</table>