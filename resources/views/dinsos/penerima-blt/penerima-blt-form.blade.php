@extends('layouts.master-dinsos')

@section('title', 'Penerima BLT Dinsos Form')

@section('penerima-blt', 'active')

@section('content-card')
  <div class="row">
    <div class="col-12">
      <hr />
        <h5 class="text-grey font-weight-bold">Form Penerima BLT</h5>
      <hr />
      @if ($penerima->id ?? '' )
        <form action="{{route('dinsos.penerima-blt.update', $penerima->id ?? '' )}}" method="POST">
        {{ method_field('PUT') }}
      @else
        <form action="{{route('dinsos.penerima-blt.store')}}" method="POST">
      @endif
        @csrf
        <div class="row">
          <div class="col-xl-6">
            <span><b>Nama Penerima</b></span>
            <input type="text" class="form-control mb-3" placeholder="Masukan Nama Penerima BLT" name="nama" value="{{$penerima->nama ?? ''}}" required>
          </div>
          <div class="col-xl-6">
            <span><b>NIK</b></span>
            <input type="number" min="0" class="form-control mb-3" placeholder="Masukan NIK Penerima BLT" name="nik" value="{{$penerima->nik ?? ''}}" required>
          </div>
          <div class="col-xl-6">
            <span><b>No KK</b></span>
            <input type="number" min="0" class="form-control mb-3" placeholder="Masukan No KK Penerima" name="kk" value="{{$penerima->kk ?? ''}}" required>
          </div>
          <div class="col-xl-6">
            <span><b>No Hp</b></span>
            <input type="number" min="0" class="form-control mb-3" placeholder="Masukan No Hp Penerima" name="hp" value="{{$penerima->hp ?? ''}}" required>
          </div>
          <div class="col-12">
            <span><b>Alamat Penerima</b></span>
            <input type="text" class="form-control mb-3" placeholder="Masukan Alamat Penerima" name="alamat" value="{{$penerima->alamat ?? ''}}" required>
          </div>
          <div class="col-12">
            <span><b>Kabupaten Penerima</b></span>
            <select class="form-control mb-3" name="kabupaten" id="kabupaten" required>
              <option value="">--Pilih Kabupaten--</option>
              @foreach ($kabupaten as $item)
                <option value="{{$item->id_kab}}">{{$item->nama}}</option>
              @endforeach
            </select>
          </div>
          <div class="col-12">
            <span><b>Kecamatan Penerima</b></span>
            <select class="form-control mb-3" name="kecamatan" id="kecamatan" required>
              <option value="">--Pilih Kabupaten dahulu--</option>
            </select>
          </div>
          <div class="col-12">
            <span><b>Desa/Kelurahan Penerima</b></span>
            <select class="form-control mb-3" name="kelurahan" id="kelurahan" required>
              <option value="">--Pilih Kabupaten dan Kecamatan dahulu--</option>
            </select>
          </div>
          <div class="col-12">
            <span><b>Bulan Ke</b></span>
            <select name="gelombang" class="form-control">
              <option value="">--Pilih bulan dahulu--</option>
              <option value="1">Januari</option>
              <option value="2">Februari</option>
              <option value="3">Maret</option>
              <option value="4">April</option>
              <option value="5">Mei</option>
              <option value="6">Juni</option>
              <option value="7">Juli</option>
              <option value="8">Agustus</option>
              <option value="9">September</option>
              <option value="10">Oktober</option>
              <option value="11">November</option>
              <option value="12">Desember</option>
            </select>
          </div>
          <div class="col-12 mt-3">
            <div class="float-right">
              <a href="{{ route('dinsos.penerima-blt.index') }}" class="btn btn-white-grey">Batal</a>
              <button type="submit" class="btn btn-grey-white">Simpan</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@stop

@push('custom-js')
  <script type="text/javascript">
    $(document).ready(function()
    {
        $('#kabupaten').change(function()
        {
            let id = $(this).val();
            $.ajax
            ({
                url : "{{route('dinsos.json.kecamatan')}}",
                method : "POST",
                async : true,
                data :
                {
                  id: id,
                  _token:'{{csrf_token()}}'
                },
                dataType : 'json',
                success: function(data)
                {
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].id_kec+'>'+data[i].nama+'</option>';
                    }
                    $('#kecamatan').html(html);
                }
            });
            return false;
        });

    });

    $(document).ready(function()
    {
        $('#kecamatan').change(function()
        {
            let id = $(this).val();
            $.ajax
            ({
                url : "{{route('dinsos.json.kelurahan')}}",
                method : "POST",
                async : true,
                data :
                {
                  id: id,
                  _token:'{{csrf_token()}}'
                },
                dataType : 'json',
                success: function(data)
                {
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].id_kel+'>'+data[i].nama+'</option>';
                    }
                    $('#kelurahan').html(html);
                }
            });
            return false;
        });

    });
  </script>
@endpush
