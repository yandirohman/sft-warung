@extends('layouts.master-dinsos')

@section('title', 'Detail Penerima BLT Dinsos')

@section('penerima-blt', 'active')

@section('content-card')

@stop

@section('content-non-card')
<div class="row">
  <div class="col-xl-12">
    <div class="d-flex justify-content-center">
      <div class="card-custom px-2" style="width: 800px">
        <div class="card-header">
            <p class="text-grey font-weight-bold"></p>
        </div>
        <div class="card-body">
          <div class="row">
            <table>
              <tr>
                <td>ID Penerima</td>
                <td>:</td>
                <td>{{$penerima->penerima_id}}</td>
              </tr>
              <tr>
                <td>NIK Penerima</td>
                <td>:</td>
                <td>{{$penerima->nik}}</td>
              </tr>
              <tr>
                <td>No KK Penerima</td>
                <td>:</td>
                <td>{{$penerima->kk}}</td>
              </tr>
              <tr>
                <td>Nama Penerima</td>
                <td>:</td>
                <td>{{$penerima->nama}}</td>
              </tr>
              <tr>
                <td>No Hp Penerima</td>
                <td>:</td>
                <td>{{$penerima->hp}}</td>
              </tr>
              <tr style="vertical-align: top;">
                <td>Alamat Penerima</td>
                <td>:</td>
                <td>{{$penerima->alamat}}</td>
              </tr>
              <tr>
                <td>Gelombang&nbsp;Penerima</td>
                <td>:</td>
                <td>{{$penerima->gelombang}}</td>
              </tr>
              <tr>
                <td>Dana Penerima</td>
                <td>:</td>
                <td>{{$penerima->besaran_dana}}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
