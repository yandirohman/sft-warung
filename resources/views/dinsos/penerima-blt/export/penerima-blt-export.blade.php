<table>
  <thead>
      <tr>
          <th style="border:1px solid black; font-weight: bold; text-align: center">N0</th>
          <th style="border:1px solid black; font-weight: bold; text-align: center">Nama</th>
          <th style="border:1px solid black; font-weight: bold; text-align: center">NIK</th>
          <th style="border:1px solid black; font-weight: bold; text-align: center">No KK</th>
          <th style="border:1px solid black; font-weight: bold; text-align: center">No HP</th>
          <th style="border:1px solid black; font-weight: bold; text-align: center">Alamat Penerima</th>
          <th style="border:1px solid black; font-weight: bold; text-align: center">Gelombang Ke</th>
          <th style="border:1px solid black; font-weight: bold; text-align: center">Besaran Dana</th>
      </tr>
  </thead>
  <tbody>
      @forelse ($penerimaBlt as $no => $item)
          <tr>
              <td style="border:1px solid black; text-align: center">{{$no + 1}}</td>
              <td style="border:1px solid black; ">{{$item->nama}}</td>
              <td style="border:1px solid black; text-align: center">{{$item->nik}}</td>
              <td style="border:1px solid black; text-align: center">{{$item->kk}}</td>
              <td style="border:1px solid black; text-align: center">{{$item->hp}}</td>
              <td style="border:1px solid black; ">{{$item->alamat}}</td>
              <td style="border:1px solid black; ">{{$item->gelombang}}</td>
              <td style="border:1px solid black; ">{{$item->besaran_dana}}</td>
          </tr>
      @empty
          <tr>
              <td colspan="7" style="border:1px solid black; ">Tidak Ada Data Penerima</td>
          </tr>
      @endforelse
  </tbody>
</table>
