@extends('layouts.master-dinsos')

@section('title', 'Detail Penerima BLT Dinsos')

@section('penerima-blt', 'active')

@section('content-card')
  <div class="row">
    <div class="col-12">
      <div class="float-right">
        <div class="float-right">
          <div class="control-page">
            <a href="{{$penerima->previousPageUrl()}}" class="btn btn-sm btn-default span-box"> <i class="fas fa-chevron-left"></i> </a>
            <span>{{$penerima->currentPage()}}</span>
            <span>dari</span>
            <span>{{$penerima->lastPage()}}</span>
            <a href="{{$penerima->nextPageUrl()}}" class="btn btn-sm btn-default span-box"> <i class="fas fa-chevron-right"></i> </a>
          </div>
        </div>
      </div>
      <button class="btn btn-sm btn-grey-white" type="button" onclick="cetak()">
        <i class="fa fa-qrcode"></i>
        Cetak QR Code
      </button>
    </div>
  </div>
@stop

@section('content-non-card')
  <div class="row">
    <div class="col-xl-12">
      <div class="d-flex justify-content-center">
        <div class="card-custom px-2" style="width: 850px">
          <div class="card-header">
              <p class="text-grey font-weight-bold"></p>
          </div>
          <div class="card-body" id="cetak">
            <div class="grid-container">
              @foreach ($penerima as $item)
                <div class="grid-item">
                  <div class="img-con">
                    <img class="img1" src="{{asset('img/card/lease.png')}}" alt="" width="70" style="position: relative">
                    <img class="img2" src="{{asset('img/card/sq.png')}}" alt="" width="120" style="margin-top: 25px;">
                    <b class="qr">{!! QrCode::size(80)->generate($gelombang.$item->penerima_id) !!}</b>
                    <img class="logoBdg" src="{{asset('img/card/kab_bdg.png')}}" alt="" width="25">
                    <img class="logoBjb" src="{{asset('img/card/bjb.png')}}" alt="" width="40">
                    <img class="logoDinsos" src="{{asset('img/card/dinsos.png')}}" alt="" width="40">
                    <img class="wali" src="{{asset('img/card/wali.png')}}" alt="" width="100">
                    <p class="text1"><b>KARTU BANDUNG SEJAHTERA</b></p>
                    <span class="textNama"><b>{{strtoupper($item->nama)}}</b></span>
                    <span class="textNik">{{strtoupper($item->nik)}}</span>
                    <span class="textAlamat">{{strtoupper($item->alamat)}}</span>
                    <span class="textFoot1"><b>Penerima harus membawa ktp dan</b></span>
                    <span class="textFoot2"><b>kartu untuk melakukan pencairan</b></span>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@push('custom-css')
  <style id="style-doc">
    @import url('https://rsms.me/inter/inter.css');
    .grid-item { font-family: 'Inter', sans-serif; }
    .img-con {
      position: relative;
      top: 0;
      left: 0;
    }

    .img1 {
      position: absolute;
      top: 0;
      left: 0;
      height: 185px;
      object-fit: cover;
    }

    .img2 {
      position: absolute;
      left: 5px;
    }

    .logoBdg {
      position: absolute;
      left: 75px;
    }

    .logoBjb {
      position: absolute;
      left: 170px;
    }

    .logoDinsos {
      position: absolute;
      left: 280px;
    }

    .wali {
      position: absolute;
      top: 105px;
      left: 223px;
    }

    .text1 {
      position: absolute;
      top: 35px;
      left: 130px;
      color: gray;
      font-size: 8px;
    }

    .textNama {
      position: absolute;
      top: 55px;
      left: 130px;
      color: gray;
      font-size: 8px;
    }

    .textNik {
      position: absolute;
      top: 65px;
      left: 130px;
      color: gray;
      font-size: 8px;
    }

    .textAlamat {
      position: absolute;
      top: 75px;
      left: 130px;
      color: gray;
      font-size: 8px;
    }

    .textFoot1 {
      position: absolute;
      top: 165px;
      left: 78px;
      color: gray;
      font-size: 8px;
    }

    .textFoot2 {
      position: absolute;
      top: 175px;
      left: 78px;
      color: gray;
      font-size: 8px;
    }

    .qr {
      position: absolute;
      top: 42px;
      left: 22px;
    }

    form {
      display: inline-block;
    }

    .span-box {
      border-color: gray;
      border-radius: 5px;
    }
    .control-page {
      margin: 0 10px;
    }

    @media only screen and (max-width: 600px) {
      .control-page {
        margin: 10px;
      }

      a {
        margin-bottom: 10px;
      }

      form {
        width: 100%;
      }

      .btn-grey-white {
        min-width: 49%;
      }
    }

    @media only screen and (min-width: 1366px) {
      form {
        margin-left: 2px;
        width: 22%;
      }
    }

    @media only screen and (min-width: 1920px) {
      form {
        margin-left: 10px;
        width: 45%;
      }
    }

    .grid-container {
      display: grid;
      grid-template-columns: repeat(2, 390px);
    }

    .grid-item {
      background-color: rgba(255, 255, 255, 0.8);
      border: 1px solid rgba(0, 0, 0, 0.8);
      padding: 20px;
      font-size: 22px;
      margin-left: 10px;
      margin-bottom: 10px;
      color: black;
      height: 180px;
    }

    .nama {
      vertical-align: top;
      margin-left: 40px;
    }
  </style>
  <style>
    .grid-item {
      height: 220px;
    }
  </style>
@endpush

@push('custom-js')
<script>
  function cetak() {
      let style = $('#style-doc').html();
      printJS({
          printable: 'cetak',
          type: 'html',
          style: style,
          header: false
      })
  }
</script>
@endpush
