@extends('layouts.master-dinsos')

@section('title', 'Penerima BLT Dinsos')

@section('penerima-blt', 'active')

@section('content-card')
  <div class="row">
    <div class="col-12">
      <a href="{{route('dinsos.penerima-blt.create')}}" class="btn btn-sm btn-grey-white"><i class="fas fa-plus"></i> Tambah Penerima</a>
      <a href="{{ route('dinsos.penerima.export') }}" class="btn btn-sm btn-grey-white"><i class="fas fa-file-excel"></i> Export Data</a>
      <a href="{{ route('dinsos.penerima.form-import') }}" class="btn btn-sm btn-grey-white"><i class="fas fa-file-import"></i> Import Data</a>
      <a href="{{ asset('template-excel/template-import-penerima-blt.xlsx') }}" class="btn btn-sm btn-grey-white"><i class="fas fa-file-download"></i> Unduh Format</a>

      <form action="{{route('dinsos.penerima.cari')}}" method="GET">
        @csrf
        <div class="input-group mb-3">
          <input type="text" class="form-control form-control-sm" name="cari" placeholder="Masukan Nama / NIK">
          <div class="input-group-append">
            <button class="btn btn-sm btn-grey-white" type="submit"> <i class="fas fa-search"></i> </button>
          </div>
        </div>
      </form>
      <div class="float-right">
        <div class="control-page">
          <!-- Pagination -->
          <a href="{{$penerima->previousPageUrl()}}" class="btn btn-sm btn-default span-box"> <i class="fas fa-chevron-left"></i> </a>
          <span>{{$penerima->currentPage()}}</span>
          <span>dari</span>
          <span>{{$penerima->lastPage()}}</span>
          <a href="{{$penerima->nextPageUrl()}}" class="btn btn-sm btn-default span-box"> <i class="fas fa-chevron-right"></i> </a>

          <!-- Filter -->
          <span class="ml-2">Filter</span>
          <button class="btn btn-sm span-box" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-filter"></i>
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 1)}}">Januari</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 2)}}">Februari</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 3)}}">Maret</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 4)}}">April</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 5)}}">Mei</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 6)}}">Juni</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 7)}}">Juli</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 8)}}">Agustus</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 9)}}">September</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 10)}}">Oktober</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 11)}}">November</a>
            <a class="dropdown-item" href="{{route('dinsos.penerima.filter', 12)}}">Desember</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      @if (isset($id))
        <a href="{{route('dinsos.penerima.cairkan', $id)}}" class="btn btn-sm btn-grey-white" style="color: white;">
          <i class="fas fa-money-check"></i>
          Cairkan Dana Gelombang {{$id}}
        </a>
      @endif
      <div class="float-right">
        <button class="btn btn-sm btn-grey-white" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-qrcode"></i>
          Cetak QR Code
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 1)}}">Januari</a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 2)}}">Februari</a></a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 3)}}">Maret</a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 4)}}">April</a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 5)}}">Mei</a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 6)}}">Juni</a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 7)}}">Juli</a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 8)}}">Agustus</a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 9)}}">September</a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 10)}}">Oktober</a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 11)}}">November</a>
          <a class="dropdown-item" href="{{route('dinsos.penerima.cetak', 12)}}">Desember</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Table Data Warung -->
  <div class="table-responsive mt-3" id="print">
    <table class="table table-hover table-striped">
      <thead>
        <tr class="text-center">
          <th>No</th>
          <th>NIK Penerima</th>
          <th>No Kartu Keluarga</th>
          <th>Nama Penerima</th>
          <th>No Whatsapp</th>
          <th>Besaran Dana</th>
          <th>Aksi</th>
        </tr>
      </thead>

      <tbody>
        @if(count($penerima) == 0)
          <tr>
            <td colspan="7" class="text-center">- tidak ada penerima -</td>
          </tr>
        @else
          @foreach ($penerima as $no => $item)
            <tr class="text-center">
              <td>{{ (10 * ($penerima->currentPage() - 1)) + ($no + 1) }}</td>
              <td>{{ $item->nik }} </td>
              <td>{{ $item->kk }}</td>
              <td>{{ $item->nama }}</td>
              <td>{{ $item->hp }}</td>
              <td>Rp {{ number_format($item->besaran_dana,0,',','.') }}</td>
              <td>
                <a href="{{route('dinsos.penerima.cetak-individu', $item->penerima_id)}}" class="btn btn-sm btn-grey-white"><i class="fas fa-qrcode"></i></a>
                <a href="{{route('dinsos.penerima-blt.show', $item->penerima_id)}}" class="btn btn-sm btn-white-grey"><i class="fas fa-info-circle"></i></a>
                <a href="{{route('dinsos.penerima-blt.edit', $item->penerima_id)}}" class="btn btn-sm btn-grey-white"><i class="fas fa-edit"></i></a>
                <button type="button" class="btn btn-sm btn-white-grey" data-toggle="modal" data-target="#modalConfirm{{$item->penerima_id}}"><i class="fas fa-trash"></i></button>
              </td>
              <div class="modal fade" id="modalConfirm{{$item->penerima_id}}">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Anda Yakin !</h5>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form action="{{route('dinsos.penerima-blt.destroy', $item->penerima_id)}}" method="POST">
                      @csrf
                      {{ method_field('DELETE') }}
                      <div class="modal-body text-center">
                        <p class="font-weight-bold">Anda Ingin Menghapus Penerima Dengan Nama {{$item->nama}} ?</p>
                      </div>
                      <div class="modal-footer">
                        <div class="float-right">
                          <button type="button" class="btn btn-sm btn-white-grey" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-sm btn-grey-white">Hapus</button>
                        </div>
                      </div>
                  </form>
                  </div>
                </div>
              </div>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
@stop

@push('custom-css')
    <style>
      form {
        display: inline-block;
      }

      .span-box {
        border-color: gray;
        border-radius: 5px;
      }
      .control-page {
        margin: 0 10px;
      }

      @media only screen and (max-width: 600px) {
        .control-page {
          margin: 10px;
        }

        a {
          margin-bottom: 10px;
        }

        form {
          width: 100%;
        }

        .btn-grey-white {
          min-width: 49%;
        }
      }

      @media only screen and (min-width: 1366px) {
        form {
          margin-left: 2px;
          width: 22%;
        }
      }

      @media only screen and (min-width: 1920px) {
        form {
          margin-left: 10px;
          width: 45%;
        }
      }
    </style>
@endpush
