@extends('layouts.master-dinsos')

@section('title', 'Penerima BLT Dinsos Import')

@section('penerima-blt', 'active')

@section('content-card')
  <div class="row">
    <div class="col-xl-12">
      <hr />
        <h5 class="text-grey font-weight-bold">Import Data Penerima BLT</h5>
      <hr />

      <div class="col-xl-12">
        <div class="alert-danger alert">
          <p class="text-center font-weight-bold m-0">PERHATIAN !</p>
          <p class="text-center m-0">Diharapahkan Format Excel Sama Dengan Template Excel Yang Sudah Kami Sediakan</p>
          <p class="text-center m-0">Anda Bisa Download Template Excel Dihalaman Index Penerima BLT</p>
          <p class="text-center font-weight-bold">Dan Data Penerima BLT Harus Per Kelurahan</p>
        </div>
      </div>

      <form action="{{ route('dinsos.penerima.import') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
          <div class="col-xl-4">
            <div class="form-group">
              <div class="col-xl-12">
                <select name="id_kab" id="id_kab" class="form-control">
                  <option hidden>-- Silahkan Pilih Kabupaten --</option>
                  @foreach ($kabupaten as $kab)
                      <option value="{{ $kab->id_kab }}">{{ $kab->nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-xl-4">
            <div class="form-group">
              <div class="col-xl-12">
                <select name="id_kec" id="id_kec" class="form-control">
                  <option hidden>-- Silahkan Pilih Kecamatan --</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xl-4">
            <div class="form-group">
              <div class="col-xl-12">
                <select name="id_kel" id="id_kel" class="form-control">
                  <option hidden>-- Silahkan Pilih Kelurahan --</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="form-group">
              <div class="col-xl-12">
                <input type="file" name="excel_penerima" class="form-control" required>
              </div>
            </div>
          </div>
          <div class="col-12 mt-3">
            <div class="float-right">
              <a href="{{ route('dinsos.penerima-blt.index') }}" class="btn btn-white-grey">Batal</a>
              <button type="submit" class="btn btn-grey-white">Simpan</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@stop

@push('custom-js')
    <script>
      $(() => {
        // Get Kecamatan
        $('#id_kab').on('change', function () {
            let kabId = $(this).val();
            let urlKec = "{{ route('dinsos.get.kecamatan', ':kabId') }}"
            urlKec = urlKec.replace(':kabId', kabId)

            if (kabId) {
                $.ajax({
                    url: urlKec,
                    type: "GET",
                    success: function (response) {
                        $('#id_kec').empty();
                        $('#id_kec').append('<option hidden>-- Silahkan Pilih Kecamatan --</option>');
                        $.each(response, function (key, data) {
                            $('#id_kec').append('<option value="' + data.id_kec + '">' + data.nama + '</option>');
                        });
                    },
                });
            } else {
                $('#id_kec').append('<option value="">Pilih Kecamatan</option>');
            }
        })

        // Get Kelurahan
        $('#id_kec').on('change', function () {
            let kecId = $(this).val();
            let urlKec = "{{ route('dinsos.get.kelurahan', ':kecId') }}"
            urlKec = urlKec.replace(':kecId', kecId)

            if (kecId) {
                $.ajax({
                    url: urlKec,
                    type: "GET",
                    success: function (response) {
                        $('#id_kel').empty();
                        $('#id_kel').append('<option hidden>-- Silahkan Pilih Kelurahan --</option>');
                        $.each(response, function (key, data) {
                            $('#id_kel').append('<option value="' + data.id_kel + '">' + data.nama + '</option>');
                        });
                    },
                });
            } else {
                $('#id_kel').append('<option value="">Pilih Kelurahan</option>');
            }
        })
      })
    </script>
@endpush

