@extends('layouts.master-dinsos', [
    'dashboard' => true
])

@section('title', 'Dashboard Dinsos')

@section('e-post', 'active')

@section('content-card')
    <div class="row">
        <div class="col-xl-6">
            <div class="card-custom">
                <div class="card-header pb-2">
                    <h4 class="title">Besaran Penyaluran Dana Perbulan</h4>
                    <h4 class="sub-title">Dana Bantuan vs Penyaluran</h4>
                </div>
                <div class="card-body py-2">
                    <div id="dana-vs-realisasi"></div>
                </div>
            </div>
        </div>

        <div class="col-xl-3">
            <div class="card-custom">
                <div class="card-header pb-2">
                    <h4 class="title">Persentasi Penyaluran Sembako</h4>
                    <h4 class="sub-title">Tersalurkan vs Belum Tersalurkan</h4>
                </div>
                <div class="card-body py-2">
                    <canvas id="pie-deposit" width="400" height="300"></canvas>
                </div>
            </div>
        </div>

        <div class="col-xl-3">
            <div class="card-custom">
                <div class="card-header pb-2">
                    <h4 class="title">Persentasi Dana Rembes Warung</h4>
                    <h4 class="sub-title">Rembes vs Belum Rembes</h4>
                </div>
                <div class="card-body py-2">
                    <canvas id="pie-dana-blt" width="400" height="300"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-3">
            <div class="card-custom">
                <div class="card-header p-3">
                    <h4 class="title">Deposit Warung</h4>
                    <div class="row">
                        <div class="col-7 nominal-info">
                            <p class="info-title">Total Rembes</p>
                            <p class="info-nominal">{{GeneralHelper::toRupiah($totalRembes)}}</p>
                        </div>
                        <div class="col-5 pl-0">
                            <i class="fas fa-search input-search-icon" style="cursor: pointer" onclick="getDeposit('cari')"></i>
                            <input type="text" class="form-control search small-search" placeholder="search..." id="table-deposit-search">
                        </div>
                    </div>
                </div>
                <div class="card-body p-2">
                    <div class="table-responsive">
                        <table class="card-table text-center table-striped" id="table-deposit">
                            <thead>
                                <tr>
                                    <th class="col-4">Warung</th>
                                    <th class="col-4">Jml Penerima</th>
                                    <th class="col-4">Nominal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($warungTerlaris as $warung)
                                    <tr>
                                        <td class="col-4">{{$warung->nama_warung}}</td>
                                        <td class="col-4">{{$warung->jumlah_penerima ?? 0}}</td>
                                        <td class="col-4">{{GeneralHelper::toRupiah($warung->nominal ?? 0)}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="col-12">Data Tidak Ada</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                    @if (count($warungTerlaris) > 0)
                        <div class="text-center my-2">
                            <button class="btn btn-grey-white btn-sm" onclick="getDeposit('next')"><</button>
                            <span class="mx-2">1 dari 1</span>
                            <button class="btn btn-grey-white btn-sm" onclick="getDeposit('prev')">></button>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-xl-3">
            <div class="card-custom">
                <div class="card-header p-3">
                    <h4 class="title">Harga Per Produk</h4>
                    <div class="row pt-3">
                        <div class="col-7 nominal-info pr-5">
                        </div>
                        <div class="col-5 pl-0" style="height:fit-content; margin:auto">
                            <i class="fas fa-search input-search-icon" style="cursor: pointer" onclick="getHargaProduk('cari')"></i>
                            <input type="text" class="form-control search small-search" placeholder="search..." id="table-harga-search">
                        </div>
                    </div>
                </div>
                <div class="card-body p-2">
                    <div class="table-responsive">
                        <table class="card-table text-center table-striped" id="table-harga">
                            <thead>
                                <tr>
                                    <th class="col-4">Produk</th>
                                    <th class="col-4">Harga Terendah</th>
                                    <th class="col-4">Harga Tertinggi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($produk as $p)
                                    <tr>
                                        <td class="col-4"><a href="#" onclick="hargaProdukPerWarung('{{$p->id}}')">{{$p->nama}}</a></td>
                                        <td class="col-4">{{GeneralHelper::toRupiah($p->harga_terendah ?? 0)}}</td>
                                        <td class="col-4">{{GeneralHelper::toRupiah($p->harga_tertinggi ?? 0)}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="col-12">Data Tidak Ada</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                    @if (count($produk) > 0)
                        <div class="text-center my-2">
                            <button class="btn btn-grey-white btn-sm" onclick="getHargaProduk('next')"><</button>
                            <span class="mx-2">1 dari 1</span>
                            <button class="btn btn-grey-white btn-sm" onclick="getHargaProduk('prev')">></button>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="card-custom">
                <div class="card-header">
                    <h4 class="title">Riwayat Pencairan Dana BLT (Real Time)</h4>
                </div>
                <div class="card-body">
                    <div class="card-tabs">
                        <div class="row">
                            <div class="col-9">
                                <div class="tabs">
                                    <label class="tab" id="realtime-tab" onclick="bukaTab('realtime')">Hari Ini</label>
                                    <label class="tab" id="minggu-ini-tab" onclick="bukaTab('minggu-ini')">Minggu Ini</label>
                                    <label class="tab" id="bulan-ini-tab" onclick="bukaTab('bulan-ini')">Bulan Ini</label>
                                </div>
                            </div>
                            <div class="col-3 pl-0">
                                <div id="hari-ini-search">
                                    <i class="fas fa-search input-search-icon" style="cursor: pointer" onclick="getRiwayat('hari-ini', null, 'cari')"></i>
                                    <input type="text" class="form-control search small-search" placeholder="cari hari ini" id="table-riwayat-hari-ini-search">
                                </div>

                                <div id="minggu-ini-search">
                                    <i class="fas fa-search input-search-icon" style="cursor: pointer" onclick="getRiwayat('minggu-ini', null, 'cari')"></i>
                                    <input type="text" class="form-control search small-search" placeholder="cari minggu ini" id="table-riwayat-minggu-ini-search">
                                </div>

                                <div id="bulan-ini-search">
                                    <i class="fas fa-search input-search-icon" style="cursor: pointer" onclick="getRiwayat('bulan-ini', null,'cari')"></i>
                                    <input type="text" class="form-control search small-search" placeholder="cari bulan ini" id="table-riwayat-bulan-ini-search">
                                </div>
                            </div>
                        </div>
                        <div class="panels">
                            <div class="panel" id="realtime-panel">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover text-center" id="table-riwayat-hari-ini">
                                        <thead>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Warung</th>
                                                <th>ID Warung</th>
                                                <th>Penerima</th>
                                                <th>NIK</th>
                                                <th>Nominal</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($riwayatTransaksiHariIni as $transaksi)
                                                <tr>
                                                    <td>{{$transaksi->created_at}}</td>
                                                    <td>{{$transaksi->nama_warung}}</td>
                                                    <td>{{$transaksi->warung_id}}</td>
                                                    <td>{{$transaksi->nama}}</td>
                                                    <td>{{$transaksi->nik}}</td>
                                                    <td>{{GeneralHelper::toRupiah($transaksi->total_harga ?? 0)}}</td>
                                                    <td>
                                                        <button class="btn btn-primary">Detail</button>
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="7">Tidak Ada Data</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>

                                @if (count($riwayatTransaksiHariIni) > 0)
                                    <div class="text-center my-2">
                                        <button class="btn btn-grey-white btn-sm" onclick="getRiwayat('hari-ini', nextPageRiwayatHariIni,'next')"><</button>
                                        <span class="mx-2" id="riwayat-hari-ini">1 dari 1</span>
                                        <button class="btn btn-grey-white btn-sm" onclick="getRiwayat('hari-ini', prevPageRiwayatHariIni,'prev')">></button>
                                    </div>
                                @endif
                            </div>
                            <div class="panel" id="bulan-ini-panel">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover text-center" id="table-riwayat-bulan-ini">
                                        <thead>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Warung</th>
                                                <th>ID Warung</th>
                                                <th>Penerima</th>
                                                <th>NIK</th>
                                                <th>Nominal</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($riwayatTransaksiBulanIni as $transaksi)
                                                <tr>
                                                    <td>{{$transaksi->created_at}}</td>
                                                    <td>{{$transaksi->nama_warung}}</td>
                                                    <td>{{$transaksi->warung_id}}</td>
                                                    <td>{{$transaksi->nama}}</td>
                                                    <td>{{$transaksi->nik}}</td>
                                                    <td>{{GeneralHelper::toRupiah($transaksi->total_harga)}}</td>
                                                    <td>
                                                        <button class="btn btn-primary">Detail</button>
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="7">Tidak Ada Data</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>

                                @if (count($riwayatTransaksiBulanIni) > 0)
                                    <div class="text-center my-2">
                                        <button class="btn btn-grey-white btn-sm" onclick="getRiwayat('bulan-ini', nextPageRiwayatBulanIni, 'next')"><</button>
                                        <span class="mx-2" id="riwayat-bulan-ini">1 dari 1</span>
                                        <button class="btn btn-grey-white btn-sm" onclick="getRiwayat('bulan-ini', prevPageRiwayatBulanIni, 'prev')">></button>
                                    </div>
                                @endif
                            </div>
                            <div class="panel" id="minggu-ini-panel">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover text-center" id="table-riwayat-minggu-ini">
                                        <thead>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Warung</th>
                                                <th>ID Warung</th>
                                                <th>Penerima</th>
                                                <th>NIK</th>
                                                <th>Nominal</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($riwayatTransaksiMingguIni as $transaksi)
                                                <tr>
                                                    <td>{{$transaksi->created_at}}</td>
                                                    <td>{{$transaksi->nama_warung}}</td>
                                                    <td>{{$transaksi->warung_id}}</td>
                                                    <td>{{$transaksi->nama}}</td>
                                                    <td>{{$transaksi->nik}}</td>
                                                    <td>{{$transaksi->total_harga}}</td>
                                                    <td>
                                                        <button class="btn btn-primary">Detail</button>
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="7">Tidak Ada Data</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>

                                @if (count($riwayatTransaksiMingguIni) > 0)
                                    <div class="text-center my-2">
                                        <button class="btn btn-grey-white btn-sm" onclick="getRiwayat('minggu-ini', nextPageRiwayatMingguIni, 'next')"><</button>
                                        <span class="mx-2" id="riwayat-minggu-ini">1 dari 1</span>
                                        <button class="btn btn-grey-white btn-sm" onclick="getRiwayat('minggu-ini', prevPageRiwayatMingguIni, 'prev')">></button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-produk" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="title">Daftar Harga Terendah Dan Tertinggi Per Warung</h4>
                    <div class="info-warung mt-4">
                        <div class="row">
                            <div class="col-3">
                                <p class="mb-1">Nama Produk</p>
                                <p class="mb-1">Batas Harga</p>
                            </div>
                            <div class="col-8">
                                <p class="mb-1" id="nama-produk"></p>
                                <p class="mb-1" id="batas-harga"></p>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-4">
                        <table class="table table-striped table-hover text-center" id="table-harga-perwarung">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Warung</th>
                                    <th class="text-center">Harga Terendah</th>
                                    <th class="text-center">Harga Tertinggi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-sm btn-secondary" data-dismiss="modal">TUTUP</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('progress-bar')
    <div class="card-custom">
        <div class="card-header">
            <h4 class="title mt-4">Jumlah Saldo Transaksi dan Rembes Warung</h4>
        </div>
        <div class="pr-4">
            <div style="overflow: auto; max-height:1000px">
                <div id="progress-bar"></div>
            </div>
        </div>
    </div>
@endsection

@push('custom-js')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.min.js"></script>

    <script>

        var currentPageDeposit  = {{$warungTerlaris->currentPage()}};
        var lastPageDeposit     = {{$warungTerlaris->lastPage()}};
        var nextPageDeposit     = currentPageDeposit + 1 <= lastPageDeposit ? currentPageDeposit + 1 : 0;
        var prevPageDeposit     = currentPageDeposit - 1 > 0 ? currentPageDeposit - 1 : 0;
        var pathDeposit         = '{{route("dinsos.dashboard.get-deposit")}}';

        var currentPageHargaProduk  = {{$produk->currentPage()}};
        var lastPageHargaProduk     = {{$produk->lastPage()}};
        var nextPageHargaProduk     = currentPageHargaProduk + 1 <= lastPageHargaProduk ? currentPageHargaProduk + 1 : 0;
        var prevPageHargaProduk     = currentPageHargaProduk - 1 > 0 ? currentPageHargaProduk - 1 : 0;
        var pathHargaProduk         = '{{route("dinsos.dashboard.get-produk")}}';

        var pathRiwayat= '{{route("dinsos.dashboard.get-riwayat")}}';

        var currentPageRiwayatHariIni  = {{$riwayatTransaksiHariIni->currentPage()}};
        var lastPageRiwayatHariIni     = {{$riwayatTransaksiHariIni->lastPage()}};
        var nextPageRiwayatHariIni     = currentPageRiwayatHariIni + 1 <= lastPageRiwayatHariIni ? currentPageRiwayatHariIni + 1 : 0;
        var prevPageRiwayatHariIni     = currentPageRiwayatHariIni - 1 > 0 ? currentPageRiwayatHariIni - 1 : 0;

        var currentPageRiwayatMingguIni  = {{$riwayatTransaksiMingguIni->currentPage()}};
        var lastPageRiwayatMingguIni     = {{$riwayatTransaksiMingguIni->lastPage()}};
        var nextPageRiwayatMingguIni     = currentPageRiwayatMingguIni + 1 <= lastPageRiwayatMingguIni ? currentPageRiwayatMingguIni + 1 : 0;
        var prevPageRiwayatMingguIni     = currentPageRiwayatMingguIni - 1 > 0 ? currentPageRiwayatMingguIni - 1 : 0;

        var currentPageRiwayatBulanIni  = {{$riwayatTransaksiMingguIni->currentPage()}};
        var lastPageRiwayatBulanIni     = {{$riwayatTransaksiMingguIni->lastPage()}};
        var nextPageRiwayatBulanIni     = currentPageRiwayatBulanIni + 1 <= lastPageRiwayatBulanIni ? currentPageRiwayatBulanIni + 1 : 0;
        var prevPageRiwayatBulanIni     = currentPageRiwayatBulanIni - 1 > 0 ? currentPageRiwayatBulanIni - 1 : 0;

        // $("#table-deposit-search").on("keyup", function() {
        //     var value = $(this).val().toLowerCase();
        //     $("#table-deposit tbody tr").filter(function() {
        //         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        //     });
        // });

        // $("#table-harga-search").on("keyup", function() {
        //     var value = $(this).val().toLowerCase();
        //     $("#table-harga tbody tr").filter(function() {
        //         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        //     });
        // });

        // $("#table-riwayat-hari-ini-search").on("keyup", function() {
        //     var value = $(this).val().toLowerCase();
        //     $("#table-riwayat-hari-ini tbody tr").filter(function() {
        //         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        //     });
        // });

        // $("#table-riwayat-bulan-ini-search").on("keyup", function() {
        //     var value = $(this).val().toLowerCase();
        //     $("#table-riwayat-bulan-ini tbody tr").filter(function() {
        //         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        //     });
        // });

        // $("#table-riwayat-minggu-ini-search").on("keyup", function() {
        //     var value = $(this).val().toLowerCase();
        //     $("#table-riwayat-minggu-ini tbody tr").filter(function() {
        //         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        //     });
        // });
        bukaTab('realtime');

        var url = '{{route("dinsos.get.data")}}';
        $.ajax({
            method: 'POST',
            url     : url,
            data : {
                _token : '{{csrf_token()}}'
            },
            success : function(res){
                chartDanaRealisasi(res.chartDanaVsRealisasi);

                var saldoWarung     = parseInt(res.saldoWarung ?? 0);
                var saldoPenerima   = parseInt(res.saldoPenerima ?? 0);
                chartDeposit(saldoWarung, saldoPenerima);

                chartDanaBantuan(res.saldoTotal,res.saldoRembes);

                chartPendapatanWarung(res.pendapatanPerwarung.transaksi, res.pendapatanPerwarung.rembes, res.pendapatanPerwarung.namaWarung);
            }
        });

        function chartDanaRealisasi(data){
            var options = {
                series: data,
                chart: {
                    type: 'bar',
                    height: 350
                },
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: '55%',
                        endingShape: 'rounded'
                    },
                },
                dataLabels: {
                    enabled: false
                    },
                    stroke: {
                        show: true,
                        width: 2,
                        colors: ['transparent']
                    },
                    xaxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agust', 'Sep', 'Oct', 'Nov', 'Des'],
                    },
                    fill: {
                        opacity: 1
                    },
                    tooltip: {
                        y: {
                            formatter: function (val) {
                            return formatRupiah(String(val));
                            }
                        }
                    }
                };

                var chart = new ApexCharts(document.querySelector("#dana-vs-realisasi"), options);
                chart.render();
        }

        function chartDeposit(saldoWarung, saldoPenerima){

            var pieDeposit = new Chart($('#pie-deposit'),{
                type: 'doughnut',
                data: {
                    labels: [
                        'Tersalurkan',
                        'Belum Tersalurakan',
                    ],
                    datasets: [{
                        label: 'Saldo',
                        data: [saldoWarung, saldoPenerima],
                        backgroundColor: [
                            'rgb(107, 113, 146)',
                            'rgb(173, 182, 230)'
                        ],
                        hoverOffset: 5
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'bottom',
                        },
                    }
                },
            });
        }

        function chartDanaBantuan(saldoWarung, danaBantuan){
            var pieDanaBLT = new Chart($('#pie-dana-blt'),{
                type: 'doughnut',
                data: {
                    labels: [
                        'Belum Rembes',
                        'Rembes',
                    ],
                    datasets: [{
                        label: 'Realisasi',
                        data: [saldoWarung, danaBantuan],
                        backgroundColor: [
                            'rgb(2,138,222)',
                            'rgb(99,207,255)'
                        ],
                        hoverOffset: 5
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'bottom',
                        },
                    }
                },
            });
        }

        function bukaTab(tab){
            $('.panels').children().css('display', 'none');
            $(`#${tab}-panel`).css("display", "block");

            $('.tabs').children().removeAttr('style');
            $(`#${tab}-tab`).css("color", "#757575");
            $(`#${tab}-tab`).css("font-weight", "bold");
            $(`#${tab}-tab`).css("box-shadow", "0px -3px 3px 0px rgb(0 0 0 / 10%)");

            $('#hari-ini-search').hide();
            $('#bulan-ini-search').hide();
            $('#minggu-ini-search').hide();
            if (tab == 'realtime') {
                $('#hari-ini-search').show();
            } else if (tab == 'bulan-ini') {
                $('#bulan-ini-search').show();
            } else if (tab == 'minggu-ini') {
                $('#minggu-ini-search').show();
            }
        }

        function formatRupiah(angka, prefix = "Rp. "){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split  = number_string.split(','),
            sisa   = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                var separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        function chartPendapatanWarung(dataTransaksi, dataRembes, namaWarung){
            var options = {
                series: [
                  {
                    name : 'Transaksi',
                    data: dataTransaksi
                  },
                  {
                    name : 'Rembes',
                    data: dataRembes
                  },
                ],
                chart: {
                type: 'bar',
                height: 430
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        dataLabels: {
                        position: 'top',
                        },
                    }
                },
                dataLabels: {
                    enabled: false,
                    offsetX: -6,
                    style: {
                        fontSize: '12px',
                        colors: ['#fff']
                    }
                },
                stroke: {
                    show: true,
                    width: 1,
                    colors: ['#fff']
                },
                tooltip: {
                    y: {
                        formatter: function (val) {
                        return formatRupiah(String(val));
                        }
                    }
                },
                xaxis: {
                    categories: namaWarung,
                },
            };

            var chart = new ApexCharts(document.querySelector("#progress-bar"), options);
            chart.render();
        }

        function hargaProdukPerWarung(id){
            $.ajax({
                url : '{{route("dinsos.dashboard.get-produk-berdasarkan-harga", "")}}' + '/' + id,
                method : 'GET',
                success : function(res){
                    $('#nama-produk').text(res.produk.nama);
                    $('#batas-harga').text(formatRupiah(String(res.produk.harga_terendah)) + ' - ' + formatRupiah(String(res.produk.harga_tertinggi)));
                    var data = res.produkPerWarung;
                    var body = '';
                    if(data.length > 0){
                        var i = 1;
                        data.forEach(val => {
                            body += `<tr>
                                        <td>${i++}</td>
                                        <td>${val.nama_warung}</td>
                                        <td>${formatRupiah(String(val.harga_terendah))}</td>
                                        <td>${formatRupiah(String(val.harga_tertinggi))}</td>
                                    </tr>`;
                        });
                    }else{
                        body += `<tr>
                                    <td colspan="4">Tidak ada data.</td>
                                </tr>`;
                    }
                    $('#table-harga-perwarung').children('tbody').children().remove();
                    $('#table-harga-perwarung').children('tbody').append(body);
                    $('#modal-produk').modal('show');
                }
            })
        }

        function getDeposit(tipe){
            var url = pathDeposit + '?';
            var cari = $('#table-deposit-search').val();

            if(tipe == 'next'){
                if(nextPageDeposit == 0){
                    return;
                }
                url += 'page=' + nextPageDeposit;
                url +=  '&cari=' + cari;
            }else if(tipe == 'prev'){
                if(prevPageDeposit == 0){
                    return;
                }
                url += 'page=' + prevPageDeposit;
                url +=  '&cari=' + cari;
            }else if(tipe == 'cari'){
                url += 'cari=' + cari;
            }

            $.ajax({
                url : url,
                method : 'GET',
                success : function(res){
                    currentPageDeposit = res.current_page;
                    lastPageDeposit    = res.last_page;
                    nextPageDeposit    = currentPageDeposit + 1 <= lastPageDeposit ? currentPageDeposit + 1 : 0;
                    prevPageDeposit    = currentPageDeposit - 1 > 0 ? currentPageDeposit - 1 : 0;

                    var body = '';
                    if(res.data.length > 0){
                        res.data.forEach(val => {
                            body += `<tr>
                                        <td class="col-4">${val.nama_warung}</td>
                                        <td class="col-4">${val.jumlah_penerima ?? 0}</td>
                                        <td class="col-4">${formatRupiah(String(val.nominal ?? 0))}</td>
                                    </tr>`;
                        });
                    }else{
                        body += `<tr>
                                    <td class="col-12">Tidak Ada Data</td>
                                </tr>`;
                    }
                    $('#table-deposit').children('tbody').children().remove();
                    $('#table-deposit').children('tbody').append(body);
                }
            });
        }

        function getHargaProduk(tipe){
            var url = pathHargaProduk + '?';
            var cari = $('#table-harga-search').val();

            if(tipe == 'next'){
                if(nextPageHargaProduk == 0){
                    return;
                }
                url += 'page=' + nextPageHargaProduk;
                url +=  '&cari=' + cari;
            }else if(tipe == 'prev'){
                if(prevPageHargaProduk == 0){
                    return;
                }
                url += 'page=' + prevPageHargaProduk;
                url +=  '&cari=' + cari;
            }else if(tipe == 'cari'){
                url += 'cari=' + cari;
            }

            $.ajax({
                url : url,
                method : 'GET',
                success : function(res){
                    currentPageHargaProduk = res.current_page;
                    lastPageHargaProduk    = res.last_page;
                    nextPageHargaProduk    = currentPageHargaProduk + 1 <= lastPageHargaProduk ? currentPageHargaProduk + 1 : 0;
                    prevPageHargaProduk    = currentPageHargaProduk - 1 > 0 ? currentPageHargaProduk - 1 : 0;

                    var body = '';
                    if(res.data.length > 0){
                        res.data.forEach(val => {
                            body += `<tr>
                                        <td class="col-4"><a href="#" onclick="hargaProdukPerWarung('${val.id}')">${val.nama}</a></td>
                                        <td class="col-4">${formatRupiah(String(val.harga_terendah ?? 0))}</td>
                                        <td class="col-4">${formatRupiah(String(val.harga_tertinggi ?? 0))}</td>
                                    </tr>`;
                        });
                    }else{
                        body += `<tr>
                                    <td class="col-12">Data Tidak Ada</td>
                                </tr>`;
                    }
                    $('#table-harga').children('tbody').children().remove();
                    $('#table-harga').children('tbody').append(body);
                }
            });
        }

        function getRiwayat(waktu, page, tipe){
            var url = pathRiwayat + '?waktu='+waktu;
            var cari = $(`#table-${waktu}-search`).val();

            if(tipe == 'next' || tipe == 'prev'){
                if(page == 0){
                    return;
                }
                url += '&page=' + page;
                url +=  '&cari=' + cari;
            }else if(tipe == 'cari'){
                url += '&cari=' + cari;
            }

            var waktuu =
            $.ajax({
                url : url,
                method : 'GET',
                success : function(res){
                    if(waktu == 'hari-ini'){
                        currentPageRiwayatHariIni = res.current_page;
                        lastPageRiwayatHariIni    = res.last_page;
                        nextPageRiwayatHariIni    = currentPageRiwayatHariIni + 1 <= lastPageRiwayatHariIni ? currentPageRiwayatHariIni + 1 : 0;
                        prevPageRiwayatHariIni    = currentPageRiwayatHariIni - 1 > 0 ? currentPageRiwayatHariIni - 1 : 0;
                    } else if(waktu == 'minggu-ini'){
                        currentPageRiwayatMingguIni = res.current_page;
                        lastPageRiwayatMingguIni    = res.last_page;
                        nextPageRiwayatMingguIni    = currentPageRiwayatMingguIni + 1 <= lastPageRiwayatMingguIni ? currentPageRiwayatMingguIni + 1 : 0;
                        prevPageRiwayatMingguIni    = currentPageRiwayatMingguIni - 1 > 0 ? currentPageRiwayatMingguIni - 1 : 0;
                    } else if(waktu == 'bulan-ini'){
                        currentPageRiwayatBulanIni = res.current_page;
                        lastPageRiwayatBulanIni    = res.last_page;
                        nextPageRiwayatBulanIni    = currentPageRiwayatBulanIni + 1 <= lastPageRiwayatBulanIni ? currentPageRiwayatBulanIni + 1 : 0;
                        prevPageRiwayatBulanIni    = currentPageRiwayatBulanIni - 1 > 0 ? currentPageRiwayatBulanIni - 1 : 0;
                    }

                    var body = '';
                    if(res.data.length > 0){
                        res.data.forEach(val => {
                            body += `<tr>
                                        <td>${val.created_at}</td>
                                        <td>${val.nama_warung}</td>
                                        <td>${val.warung_id}</td>
                                        <td>${val.nama}</td>
                                        <td>${val.nik}</td>
                                        <td>${formatRupiah(String(val.total_harga))}</td>
                                        <td>
                                            <button class="btn btn-primary">Detail</button>
                                        </td>
                                    </tr>`;
                        });
                    }else{
                        body += `<tr>
                                    <td colspan="7">Tidak Ada Data</td>
                                </tr>`;
                    }
                    $(`#table-riwayat-${waktu}`).children('tbody').children().remove();
                    $(`#table-riwayat-${waktu}`).children('tbody').append(body);
                }
            });
        }

    </script>
@endpush

@push('custom-css')
    <style>
        .small-select{
            font-size:12px;
        }

        .btn-warung-lainnya{
            background: #6B7192 !important;
            font-size: 12px;
            padding: 2.5px 10px;
            color: white !important;
        }

        table thead tr th, table tbody tr td{
            margin: auto;
        }
    </style>
@endpush
