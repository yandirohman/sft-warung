@extends('layouts.master-dinsos')

@section('title', 'Best Template Dinsos')

@section('e-post', 'active')

@section('content-card')
  <div class="row">
    <div class="col-xl-8">
        <div class="card-custom">
            <div class="card-header">
                <p class="text-grey font-weight-bold">Kontent Didalam Card</p>
            </div>
            <div class="card-body"></div>
        </div>
    </div>
    <div class="col-xl-4">
      <div class="card-custom">
          <div class="card-header">
              <p class="text-grey font-weight-bold">Kontent Didalam Card</p>
          </div>
          <div class="card-body"></div>
      </div>
    </div>
  </div>

  <hr />

  <div class="row mt-3">
    <div class="col-xl-12">
      <p class="font-weight-bold">Base Button</p>

      <button type="button" class="btn btn-grey-white"><i class="fas fa-edit"></i></button>
      <button type="button" class="btn btn-white-grey"><i class="fas fa-trash"></i></button>
      <button type="button" class="btn btn-grey-white">Simpan</button>
    </div>
  </div>
@stop

@section('content-non-card')
  <div class="row mt-3">
    <div class="col-xl-8">
        <div class="card shadow">
            <div class="card-header">
                <p class="text-grey font-weight-bold">Kontent Diluar Card</p>
            </div>
            <div class="card-body"></div>
        </div>
    </div>
    <div class="col-xl-2">
      <div class="card shadow">
          <div class="card-header">
              <p class="text-grey font-weight-bold">Kontent Diluar Card</p>
          </div>
          <div class="card-body"></div>
      </div>
    </div>
    <div class="col-xl-2">
      <div class="card shadow">
          <div class="card-header">
              <p class="text-grey font-weight-bold">Kontent Diluar Card</p>
          </div>
          <div class="card-body"></div>
      </div>
    </div>
  </div>
@stop
