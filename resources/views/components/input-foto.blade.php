<div class="form-group custom-container">
  <div class="wrapper">
      <div class="image">
          <img {{$src ? 'src="$src"' : ''}} id="{{$imageId}}">
      </div>
      <div class="content">
          <div class="icon">
              <i class="fas fa-cloud-upload-alt"></i>
          </div>
          <div class="text"></div>
      </div>
  </div>
  @if ($errors->has($name))
  <div class="text-danger">
      <ul class="list-unstyled">
      @foreach ($errors->get($name) as $error)
      <li>{{ $error }}</li>
      @endforeach
      </ul>
  </div>
  @endif
  <label class="btn-buka-kamera" for="{{$id}}">
    <i class="material-icons">photo_camera</i>
    <span id="label-upload">Buka Kamera</span>
  </label>
  <button type="submit" class="btn-selesai">Selesai</button>

  <input type="file" id="{{$id}}" accept="image/*" style="display: none;">
  <textarea name="{{$name}}" id="{{$name}}" style="display: none"></textarea>
</div>


@push('scripts')
  @include('components.change-foto-to-tiny-base64')
  <script>
      changeFotoToTinyBase64("input[id='{{$id}}']", "textarea[id='{{$name}}']", "img[id='{{$imageId}}']");
  </script>
@endpush

@push('styles')
  <style>
      @import url('https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap');
      .custom-container {
          height: 360px;
          width: 100%;
          position: relative;
      }
      .custom-container label {
          cursor: pointer;
          margin-top: 10px;
      }
      .custom-container .wrapper{
          position: relative;
          height: 300px;
          width: 100%;
          border-radius: 20px;
          background: #fff;
          display: flex;
          align-items: center;
          justify-content: center;
          overflow: hidden;
          margin-bottom: 30px;
      }
      .wrapper.active{
          border: none;
      }
      .wrapper .image{
          position: absolute;
          height: 100%;
          width: 100%;
          display: flex;
          align-items: center;
          justify-content: center;
      }
      .wrapper img{
          height: 100%;
          width: 100%;
          object-fit: cover;
      }
      .wrapper .icon{
          font-size: 100px;
          color: #9658fe;
      }
      .wrapper .text{
          font-size: 20px;
          font-weight: 500;
          color: #5B5B7B;
      }
      .content {
          display: grid;
          justify-items: center;
      }
      .btn-buka-kamera, .btn-selesai {
        width: 100%;
        background: #fff;
        padding: 10px;
        box-shadow: 1px 2px 3px rgba(0,0,0,.3);
        border-radius: 10px;
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
        border: none;
        margin-bottom: 15px;
      }
      .btn-selesai {
        display: none;
        background-color: #6b7192;
        color: #fff !important;
      }
      .btn-selesai:active {
        border: none;
        outline: none;
      }
  </style>
@endpush
