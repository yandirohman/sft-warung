<script src="{{ asset('/assets/dinsos/canvas/binaryajax.js') }}"></script>
<script src="{{ asset('/assets/dinsos/canvas/jquery.exif.js') }}"></script>
<script src="{{ asset('/assets/dinsos/canvas/exif.js') }}"></script>
<script src="{{ asset('/assets/dinsos/canvas/jquery.canvasResize.js') }}"></script>
<script src="{{ asset('/assets/dinsos/canvas/canvasResize.js') }}"></script>
<script>
    function changeFotoToTinyBase64(source, target, img) {
        $(source).change(function(e) {
            if($(source).val().length) {
                $('.loading').css('display', 'flex')
                $('.loading-text').html('Tunggu Sebentar')
                var file = e.target.files[0];
                canvasResize(file, {
                width: 360,
                height: 0,
                crop: false,
                quality: 85,
                //rotate: 90,
                callback:function(data, width, height) {
                    $(target).text(data);
                    $('.loading').fadeOut()
                    $(img).attr('src', data);
                    $("#label-upload").text("Foto Ulang");
                    $(".btn-selesai").css("display", "flex");
                }
                });
            } else {
                $(target).text('');
                $(img).attr('src', '');
            }
        });
    }
</script>
