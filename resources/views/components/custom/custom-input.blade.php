<div class="form-group">
  <label for="" class="col-lg-12">{{$label}}</label>
  <div class="control col-lg-12">
      @if ($type == 'textarea')
          <textarea class="form-control" {{ ($required) ? 'required=required' : '' }} name="{{ $name }}" placeholder="{{$placeholder}}">{{ old($name, $value) }}</textarea>
      @elseif ($type == 'number')
          <input type="number" min="0" name="{{ $name }}" value="{{ old($name, $value) }}" id="{{ $name }}" class="form-control" placeholder="{{$placeholder}}" {{ ($required) ? 'required=required' : '' }} />
      @else
          <input type="{{$type}}" name="{{$name}}" value="{{ old($name, $value) }}"  id="{{$name}}" class="form-control" placeholder="{{$placeholder}}" {{ ($required) ? 'required=required' : '' }} />
      @endif

      @if ($errors->has($name))
          <div class="text-danger">
              <ul class="list-unstyled">
                  @foreach ($errors->get($name) as $error)
                  <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
  </div>
</div>
