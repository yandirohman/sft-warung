<div class="form-group">
  <label>{{$label}}</label>
  <div>
    <select name="{{$name}}" id="{{$name}}" class="form-control" required>
      <option disabled value="" selected>Pilih {{$label}}</option>
      @foreach ($options as $key => $caption)
      <option {{($value == $key ) ? 'selected' : '' }} value="{{$key}}">{{$caption}}</option>
      @endforeach
    </select>
  </div>
</div>

@if ($errors->has($name))
  <div class="text-danger">
    <ul class="list-unstyled">
      @foreach ($errors->get($name) as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
