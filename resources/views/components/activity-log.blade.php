<div class="card-custom">
  <div class="card-header">
    <img src="{{ asset('assets/dinsos/img/heart-rate.png') }}" alt="heart-rate.img" width="50">
    <span class="text-grey font-weight-bold ml-2">Activity Log </span>
    <a href="{{route('dinsos.logs.index')}}" class="float-right">Lihat Semua</a>
  </div>
  <div class="card-body">
      @forelse ($logs as $item)
        <a href="{{route('dinsos.logs.detail', $item->id)}}" style="color: #757575;" class="logs mb-3">
          <div class="row {{ $item->id == Request::segment(4) ? 'aktif' : '' }}">
              <div class="col-2">
                  <div class="user-circle">
                      <i class="fas fa-user"></i>
                  </div>
              </div>
              <div class="col-10">
                  <div class="text-log">
                      <p class="m-0">
                          <strong>{{$item->nama}}</strong> {!!$item->keterangan!!}
                      </p>
                      <p class="text-small mt-1">{{$item->created_at}}</p>
                  </div>
              </div>
          </div>
        </a>
      @empty
        Belum ada data
      @endforelse
  </div>
</div>
<style>
  .aktif{
    background-color: #ccc;
    border-radius: 5px;
    padding-top: 15px;
    padding-bottom: 15px;
    margin-bottom: 5px;
  }
</style>
