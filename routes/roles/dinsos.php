<?php
# CUSTOM ROUTE!
# ==============

# prefix : dinsos
# middleware : authDinsos
# namespace : App\Http\Controller\Dinsos
# name : dinsos.

Route::prefix('dashboard')->namespace('Dashboard')->group(function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::post('get-data', 'DashboardController@getDataChart')->name('get.data');
    Route::get('produk', 'DashboardController@getProduk')->name('dashboard.get-produk');
    Route::get('produk/{id}', 'DashboardController@getProdukPerWarung')->name('dashboard.get-produk-berdasarkan-harga');
    Route::get('deposit', 'DashboardController@getDeposit')->name('dashboard.get-deposit');
    Route::get('riwayat', 'DashboardController@getRiwayat')->name('dashboard.get-riwayat');
});

Route::resource('penerima-blt', 'PenerimaBlt\PenerimaBltController');
Route::prefix('penerima-blt')->namespace('PenerimaBlt')->group(function () {
    Route::post('/json/json-kecamatan', 'PenerimaBltController@jsonKecamatan')->name('json.kecamatan');
    Route::post('/json/json-kelurahan', 'PenerimaBltController@jsonKelurahan')->name('json.kelurahan');
    Route::get('/penerima/cari', 'PenerimaBltController@cari')->name('penerima.cari');
    Route::get('/penerima/filter/{id}', 'PenerimaBltController@filter')->name('penerima.filter');
    Route::get('/penerima/cetak/{id}', 'PenerimaBltController@cetakCode')->name('penerima.cetak');
    Route::get('/penerima/cetak_individu/{id}', 'PenerimaBltController@cetakCodeIndividu')->name('penerima.cetak-individu');
    Route::get('/penerima/cairkan/{id}', 'PenerimaBltController@cairkanDana')->name('penerima.cairkan');
    // Import Export Excel
    Route::get('data-penerima/form/import', 'PenerimaBltController@formImport')->name('penerima.form-import');
    Route::get('data-penerima/export', 'PenerimaBltController@exportPenerima')->name('penerima.export');
    Route::post('data-penerima/import', 'PenerimaBltController@importPenerima')->name('penerima.import');
});

Route::prefix('produk')->namespace('Produk')->group(function () {
    Route::get('/', 'ProdukController@index')->name('produk-view');
    Route::get('/form-produk', 'ProdukController@form')->name('produk-form');
    Route::get('/form-edit-produk/{id}', 'ProdukController@form')->name('produk-edit');
    Route::post('/insert-produk', 'ProdukController@simpan')->name('produk-insert');
    Route::post('/update-produk/{id}', 'ProdukController@ubah')->name('produk-update');
    Route::get('/delete-produk/{id}', 'ProdukController@hapus')->name('produk-delete');
    Route::get('/cari-produk', 'ProdukController@cari')->name('produk-cari');
});

Route::prefix('paket')->namespace('Paket')->group(function () {
    Route::get('/', 'PaketController@index')->name('paket.index');
    Route::get('create', 'PaketController@create')->name('paket.create');
    Route::post('store', 'PaketController@store')->name('paket.store');
    Route::delete('destroy/{id}', 'PaketController@destroy')->name('paket.destroy');
    Route::get('show/{id}', 'PaketController@show')->name('paket.show');
    Route::get('search', 'PaketController@search')->name('paket.search');
});

Route::resource('warung', 'Warung\WarungController');
Route::prefix('warung')->namespace('Warung')->group(function () {
    Route::get('data-warung/cari', 'WarungController@cariWarung')->name('warung.cari-warung');
    Route::get('data-warung/form/import', 'WarungController@formImport')->name('warung.form-import');
    Route::get('data-warung/export', 'WarungController@exportWarung')->name('warung.export');
    Route::post('data-warung/import', 'WarungController@importWarung')->name('warung.import');
    Route::get('data-warung/cetak/{warung}', 'WarungController@cetakDataWarung')->name('warung.cetak-warung');
    Route::get('data-warung/cetak', 'WarungController@cetakDataWarungAll')->name('warung.cetak-warung-all');
});

Route::prefix('gelombang')->namespace('Gelombang')->group(function () {
    Route::get('/', 'GelombangController@index')->name('gelombang.index');
    Route::get('{gelombang}', 'GelombangController@edit')->name('gelombang.edit');
    Route::post('update/{gelombang}', 'GelombangController@update')->name('gelombang.update');
});

Route::prefix('monitoring')->namespace('Monitoring')->group(function () {
    Route::get('/', 'MonitoringController@index')->name('monitoring.index');
    // Route::get('get-data', 'MonitoringController@getData')->name('monitoring.get-data');
    // Route::get('get-data-warung', 'MonitoringController@getDataWarung')->name('monitoring.get-data-warung');
    // Route::get('export-data-warung', 'MonitoringController@exportMonitoring')->name('monitoring.export-data-warung');
    // Pindah ke api
});

Route::prefix('laporan-laba-rugi')->namespace('LaporanLabaRugi')->group(function(){
    Route::get('/', 'LaporanLabaRugiController@index')->name('laporan-laba-rugi');
});

Route::prefix('logs')->namespace('Logs')->group(function () {
    Route::get('/', 'LogsController@index')->name('logs.index');
    Route::get('/detail/{id}', 'LogsController@detail')->name('logs.detail');
});

// Filter Daerah
Route::prefix('filter-daerah')->group(function () {
    Route::get('kabupaten/{id}', 'FilterDaerahController@getKabupaten')->name('get.kabupaten');
    Route::get('kecamatan/{id}', 'FilterDaerahController@getKecamatan')->name('get.kecamatan');
    Route::get('kelurahan/{id}', 'FilterDaerahController@getKelurahan')->name('get.kelurahan');
});
