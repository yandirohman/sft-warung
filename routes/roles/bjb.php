<?php
# CUSTOM ROUTE!
# ==============

# prefix : bjb
# middleware : authBjb
# namespace : App\Http\Controller\Bjb
# name : bjb.

Route::prefix('dashboard')->namespace('Dashboard')->group(function () {
    Route::get('/', 'BjbDashboardController@index')->name('dashboard');
    Route::post('get-data', 'BjbDashboardController@getDataChart')->name('get.data');
    Route::get('produk/{id}', 'BjbDashboardController@getProdukPerWarung')->name('dashboard.get-produk-berdasarkan-harga');
});

Route::prefix('pencairan-rembes')->namespace('PencairanRembes')->group(function () {
    Route::get('/', 'PencairanRembesController@index')->name('pencairan-rembes');
    Route::get('semua', 'PencairanRembesController@semua')->name('pencairan-rembes.semua');
    Route::get('histori/{id}', 'PencairanRembesController@history')->name('pencairan-rembes.history');
    Route::get('histori/detail/{id}/{warungId}', 'PencairanRembesController@detailHistory')->name('pencairan-rembes.detail-history');
    Route::get('cari-transaksi/{warungId}', 'PencairanRembesController@cariTransaksi')->name('pencairan-rembes.cari-transaksi');
    Route::get('cari-transaksi-range/{warungId}', 'PencairanRembesController@cariTransaksiRange')->name('pencairan-rembes.cari-transaksi-range');
    Route::post('pencairan-rembes', 'PencairanRembesController@pencairanRembes')->name('pencairan-rembes.pencairan-rembes');
    Route::get('cetak-warung-rembes/{warungId}', 'PencairanRembesController@cetakSemuaWarungRembes')->name('pencairan-rembes.cetak-semua-warung-rembes');
    Route::get('transaksi/detail/{id}', 'PencairanRembesController@getDetailTransaksi');
    Route::get('transaksi/detail_penerima/{id}', 'PencairanRembesController@getDetailTransaksiPenerima');
});

Route::prefix('laporan-rembes')->namespace('LaporanRembes')->group(function () {
    Route::get('/', 'LaporanRembesController@index')->name('laporan-rembes');
});

Route::prefix('activity-log')->namespace('ActivityLog')->group(function () {
    Route::get('/', 'BjbActivityLogController@index')->name('activity-log');
    Route::get('/detail/{id}', 'BjbActivityLogController@detail')->name('activity-log.detail');
});
