<?php
# CUSTOM ROUTE!
# ==============

# prefix : warung
# middleware : authWarung
# namespace : App\Http\Controller\Warung
# name : warung.


Route::prefix('belanja')->namespace('Belanja')->group(function() {
    Route::get('/', 'IndexController@index')->name('belanja.index');
    Route::get('verifying', 'VerificationController@verifying')->name('belanja.verifying');
    Route::get('detail-transaksi/{id}', 'VerificationController@getdetailTransaksi')->name('belanja.detail-transaksi');
    Route::get('beli/{idPaket?}', 'BeliController@index')->name('belanja.beli.index');
    Route::post('beli/paket/{idPaket}', 'BeliController@beliPaket')->name('belanja.beli.paket');
    Route::post('beli/custom/true', 'BeliController@beliCustom')->name('belanja.beli.custom');
    Route::get('upload-foto-selfie', 'UploadFotoSelfieController@index')->name('belanja.upload-foto-selfie');
    Route::post('upload-foto-selfie/store', 'UploadFotoSelfieController@store')->name('belanja.upload-foto-selfie.store');
    Route::get('checkout', 'BeliController@checkout')->name('belanja.checkout.index');
    Route::get('catat-transaksi', 'BeliController@catatTransaksi')->name('belanja.checkout.trx');
});

Route::prefix('riwayat')->namespace('Riwayat')->group(function() {
    Route::get('/', 'IndexController@index')->name('riwayat.index');
    Route::get('get-detail-transaksi/{id}', 'IndexController@getDetailTransaksi')->name('riwayat.detail');
});

Route::prefix('home')->namespace('Home')->group(function() {
    Route::get('/', 'IndexController@index')->name('home.index');
});

Route::prefix('rembes')->namespace('Rembes')->group(function() {
    Route::get('/{tipe}', 'IndexController@index')->name('rembes.index');
    Route::get('/detail/{id}', 'IndexController@detailTransaksi')->name('rembes.detail');
});

Route::prefix('profile')->namespace('Profile')->group(function() {
    Route::get('/', 'IndexController@index')->name('profile.index');
});

Route::prefix('notif')->namespace('Notif')->group(function() {
    Route::get('/', 'IndexController@index')->name('notif.index');
});

Route::prefix('konfigurasi')->namespace('Konfigurasi')->group(function() {
    Route::get('/', 'KonfigurasiController@index')->name('konfigurasi.index');
    Route::get('/setHarga/{id}', 'KonfigurasiController@setHargaProdukWarung')->name('konfigurasi.config');
    Route::get('/paket', 'KonfigurasiController@getDataPaket')->name('konfigurasi.paket.index');
    Route::post('/paket', 'KonfigurasiController@storeDataPaket')->name('konfigurasi.paket.store');
    Route::post('/paket/delete/{id}', 'KonfigurasiController@deleteDataPaket')->name('konfigurasi.paket.delete');
    Route::get('/paket/detail/{id}', 'KonfigurasiController@getDetailPaket')->name('konfigurasi.paket.detail');
    Route::get('/paket/produk', 'KonfigurasiController@getDataProduk')->name('konfigurasi.paket.produk');
    Route::post('/paket/detail', 'KonfigurasiController@storeDataDetailPaket')->name('konfigurasi.paket.add-detail');
    Route::post('/paket/detail/delete/{idDetailPaket}', 'KonfigurasiController@deleteDataDetailPaket')->name('konfigurasi.paket.delete-detail');
    Route::post('/paket/detail/change/data', 'KonfigurasiController@storeChangeDetailPaket')->name('konfigurasi.paket.change-detail');
    Route::get('/produk', 'KonfigurasiController@getDataProdukForList')->name('konfigurasi.produk.index');
    Route::post('/produk/config/change', 'KonfigurasiController@changeConfigHarga')->name('konfigurasi.produk.change');
    Route::get('/produk/config/check', 'KonfigurasiController@checkConfigHarga')->name('konfigurasi.produk.config-check');
});
