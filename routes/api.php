<?php

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/monitoring/get-data', 'Dinsos\Monitoring\MonitoringController@getData')->name('monitoring.get-data');
Route::get('/monitoring/get-data-warung', 'Dinsos\Monitoring\MonitoringController@getDataWarung')->name('monitoring.get-data-warung');
Route::get('/monitoring/export-data-warung', 'Dinsos\Monitoring\MonitoringController@exportMonitoring')->name('monitoring.export-data-warung');
