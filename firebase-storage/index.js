const express = require('express');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');
const admin = require("firebase-admin");
const stream = require('stream');
const cors = require('cors')
const config = require('./config');


app.use(cors())
app.use(bodyParser.json({limit: '50mb'})) // for parsing application/json
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }))


// CHANGE: The path to your service account
var serviceAccount = config.serviceAccount;

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    storageBucket: config.storageBucket
});

var bucket = admin.storage().bucket();

const uploadPicture = function(base64, content_type, filename) {
  return new Promise((resolve, reject) => {
    if (!base64) {
      reject("news.provider#uploadPicture - Could not upload picture because at least one param is missing.");
    }

    let bufferStream = new stream.PassThrough();
    bufferStream.end(new Buffer.from(base64, 'base64'));

    // let file = bucket.file(`test/2.png`);
    let file = bucket.file(filename);

    bufferStream.pipe(file.createWriteStream({
      metadata: {
        contentType: content_type
      }
    }));
    const config = {
      action: 'read',
      expires: '03-01-2500'
    };
    let downloadUrl = file.getSignedUrl(config, (error, url) => {
      if (error) {
        reject(error);
      }
      resolve(url);
    });
  })
};

app.post('/upload_to_firebase', async (req, res) => {
    var base64 = req.body.base64;
    var folder = req.body.folder;
    var uri = base64.split(',')[0];
    uri = uri.split(';')[0];
    uri = uri.split(':')[1];
    var content_type = uri;
    var extension = content_type.split('/')[1];
    var filename = folder +'/'+Math.random().toString(36).substring(2, 15)+ '.'+extension;
    var data = base64.split(',')[1]; 
    console.log(filename, ' filename')
    uploadPicture(data, content_type, filename)
      .then(url => {
        res.send(url)
      })
      .catch(console.error);
    
})

app.get('/', (req, res) => {
  res.send('storage ok')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})