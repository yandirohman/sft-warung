<?php

namespace App\View\Components;

use Illuminate\View\Component;

class InputFoto extends Component
{
    public $id;
    public $name;
    public $label;
    public $imageId;
    public $src;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        $id, # required
        $name, # required
        $label, # required
        $imageId, # required
        $src = '' # jika untuk form edit maka isi
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->label = $label;
        $this->imageId = $imageId;
        $this->src = $src;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.input-foto');
    }
}
