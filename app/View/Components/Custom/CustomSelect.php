<?php

namespace App\View\Components\Custom;

use Illuminate\View\Component;

class CustomSelect extends Component
{
    public $name ;
    public $label ;
    public $value ;
    public $options ;
    public $classIcon ;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        $name,
        $label,
        $value,
        $options = []
    )
    {
        $this->name = $name;
        $this->label = $label;
        $this->value = old($name, $value);
        $this->options = $options;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.custom.custom-select');
    }
}
