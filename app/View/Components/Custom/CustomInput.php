<?php

namespace App\View\Components\Custom;

use Illuminate\View\Component;

class CustomInput extends Component
{
    public $type;
    public $name;
    public $id;
    public $placeholder;
    public $label;
    public $value;
    public $required;
    public $description;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        $type, # required
        $id = NULL,
        $name, # required
        $placeholder = '',
        $label, # required
        $value = '', # jika untuk form edit maka isi
        $required = true,
        $description = NULL # untuk deskripsi input
    )
    {
        # wajib
        $this->type = $type;
        $this->name = $name;
        $this->label = $label;

        # optional
        $this->value = $value;
        $this->placeholder = $placeholder;
        $this->id = $id;
        $this->required = $required;
        $this->description = $description;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.custom.custom-input');
    }
}
