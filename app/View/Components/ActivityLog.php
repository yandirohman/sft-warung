<?php

namespace App\View\Components;

use App\Models\Logs;
use Illuminate\View\Component;

class ActivityLog extends Component
{
    public $logs;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $select = [
            'id',
            'nama',
            'keterangan',
            'created_at'
        ];

        $this->logs = Logs::pilih($select);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.activity-log');
    }
}
