<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{

    public const HOME = '/warung/home/';

    const DINSOS = 'Dinsos';
    const DINSOS_PREFIX = 'dinsos';
    const WARUNG = 'Warung';
    const WARUNG_PREFIX = 'warung';
    const BJB = 'Bjb';
    const BJB_PREFIX = 'bjb';

    protected $namespace = 'App\\Http\\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();

        $this->mapRoleRoutes($role = self::DINSOS, $prefix = self::DINSOS_PREFIX);
        $this->mapRoleRoutes($role = self::WARUNG, $prefix = self::WARUNG_PREFIX);
        $this->mapRoleRoutes($role = self::BJB, $prefix = self::BJB_PREFIX);
    }

    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    protected function mapApiRoutes()
    {
        Route::prefix('api')
            //  ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapRoleRoutes($role, $rolePrefix)
    {
        $roleNamespace = "App\Http\Controllers\\$role";
        Route::prefix($rolePrefix)
            ->middleware(["web", "auth$role"])
            ->namespace($roleNamespace)
            ->name("$rolePrefix.")
            ->group(base_path("routes/roles/$rolePrefix.php"));
    }


    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
