<?php
namespace App\Helper;

class RoleHelper
{
    const DINSOS_ROLE = 1;
    const WARUNG_ROLE = 2;
    const BJB_ROLE = 3;

    public static function getRoleName(int $roleId)
    {
        if ($roleId == self::DINSOS_ROLE) {
            return 'dinsos';
        } elseif($roleId == self::WARUNG_ROLE) {
            return 'warung';
        } elseif($roleId == self::BJB_ROLE) {
            return 'bjb';
        }
        return 'unauthenticated';
    }
}
