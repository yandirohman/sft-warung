<?php
namespace App\Helper;
use DateTime;
use DatePeriod;
use DateInterval;

class TimeHelper{

    public static function jumlahHariBerdasarkanBulan($bulan, $tahun){
        // $jumlah = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
        $jumlah = date('t', mktime(0, 0, 0, $bulan, 1, $tahun));
        return $jumlah;
    }

    public static function jumlahHariBerdasarkanRangeTanggal(string $tanggal1, string $tanggal2){
        $start_date = new DateTime($tanggal1);
        $end_date   = new DateTime($tanggal2);
        $interval   = $start_date->diff($end_date);
        $jumlah     = ($interval->days+1);
        return $jumlah;
    }

    public static function getBulanIndo(string $month) { # 01
        $bulan = [
            '01' =>   	'Januari',
            '02' =>		'Februari',
            '03' =>		'Maret',
            '04' =>		'April',
            '05' =>		'Mei',
            '06' =>		'Juni',
            '07' =>		'Juli',
            '08' =>		'Agustus',
            '09' =>		'September',
            '10' =>		'Oktober',
            '11' =>		'November',
            '12' =>		'Desember'
        ];
        $bulanIndo = $bulan[$month]; # Januari
        return $bulanIndo;
    }

    public static function getTanggalSebulan($jumlahHari, $bulan, $tahun){
        for ($i = 1; $i <= $jumlahHari; $i++) {

            $date = $tahun.'/'.$bulan.'/'.$i; //format date
            $get_name = date('l', strtotime($date)); //get week day
            $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars

            //if not a weekend add day to array
            if($day_name != 'Sun' && $day_name != 'Sat'){
                $workdays[] = [
                    "tanggal"   => $tahun.'-'.$bulan.'-'.sprintf("%02d", $i),
                    "jenis"     => "Tidak Libur"];
            }else{
                $workdays[] = [
                    "tanggal"   => $tahun.'-'.$bulan.'-'.sprintf("%02d", $i),
                    "jenis"     => "Libur"];
            }
        }
        return $workdays;
    }

    public static function getTanggalDariRangeTanggal($tanggalAwal, $tanggalAkhir){
        $tanggal         = new DatePeriod(
            new DateTime($tanggalAwal),
            new DateInterval('P1D'),
            new DateTime($tanggalAkhir)
        );
        $kumpulanTanggal = [];
        foreach ($tanggal as $key => $value) {
            array_push($kumpulanTanggal, $value->format('d/m/Y'));  
        }
        return $kumpulanTanggal;
    }

    public static function jumlahHariKerjaBerdasarkanBulan($bulan, $tahun){
        $hari = self::jumlahHariBerdasarkanBulan($bulan, $tahun);
        $hari = self::getTanggalSebulan($hari, $bulan, $tahun);
        $jumlahHari = 0;
        foreach($hari as $row){
            if($row['jenis'] != 'Libur'){
                $jumlahHari ++;
            }
        }
        return $jumlahHari;
    }

    public static function jumlahHariKerjaBerdasarkanRangeTanggal($tanggal1, $tanggal2){
        $startDate = new DateTime($tanggal1);
        $endDate   = new DateTime($tanggal2);
        $workingDays = 0;

        $startTimestamp = strtotime($startDate);
        $endTimestamp = strtotime($endDate);

        for($i=$startTimestamp; $i<=$endTimestamp; $i = $i+(60*60*24) ){
            if(date("N",$i) <= 5){
                $workingDays++;
            }
        }

        return $workingDays;
    }
}