<?php
namespace App\Helper;

use Session;

class UserHelper
{
    public static function setSession()
    {
        Session::put('hasLogin', true);
        $roleId = auth()->user()->role;
        $name = auth()->user()->name;
        $username = auth()->user()->username;
        $id = auth()->user()->id;
        $roleName = RoleHelper::getRoleName($roleId);
        Session::put('loginRole', $roleName);
        Session::put('name', $name);
        Session::put('username', $username);
        Session::put('id', $id);
    }
}
