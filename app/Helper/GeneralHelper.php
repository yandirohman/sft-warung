<?php
namespace App\Helper;

class GeneralHelper
{

    public static function gelombang(string $char)
    {
        switch ($char) {
            case 'A' : return 1; break;
            case 'B' : return 2; break;
            case 'C' : return 3; break;
            case 'D' : return 4; break;
            case 'E' : return 5; break;
            case 'F' : return 6; break;
            case 'G' : return 7; break;
            case 'H' : return 8; break;
            case 'I' : return 9; break;
            case 'J' : return 10; break;
            case 'K' : return 11; break;
            case 'L' : return 12; break;
            case 'M' : return 13; break;
            case 'N' : return 14; break;
            case 'O' : return 15; break;
            case 'P' : return 16; break;
            case 'Q' : return 17; break;
            case 'R' : return 18; break;
            case 'S' : return 19; break;
            case 'T' : return 20; break;
            case 'U' : return 21; break;
            case 'W' : return 22; break;
            case 'X' : return 23; break;
            case 'Y' : return 24; break;
            case 'Z' : return 25; break;
        }
    }

    public static function char(int $int)
    {
        switch ($int) {
            case 1 : return 'A'; break;
            case 2 : return 'B'; break;
            case 3 : return 'C'; break;
            case 4 : return 'D'; break;
            case 5 : return 'E'; break;
            case 6 : return 'F'; break;
            case 7 : return 'G'; break;
            case 8 : return 'H'; break;
            case 9 : return 'I'; break;
            case 10: return 'J'; break;
            case 11: return 'K'; break;
            case 12: return 'L'; break;
            case 13: return 'M'; break;
            case 14: return 'N'; break;
            case 15: return 'O'; break;
            case 16: return 'P'; break;
            case 17: return 'Q'; break;
            case 18: return 'R'; break;
            case 19: return 'S'; break;
            case 20: return 'T'; break;
            case 21: return 'U'; break;
            case 22: return 'V'; break;
            case 23: return 'W'; break;
            case 24: return 'X'; break;
            case 25: return 'Y'; break;
            case 26: return 'Z'; break;
        }
    }

    public static function toRupiah($angka)
    {
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }
}
