<?php

namespace App\Library;

use App\Helper\Curl;

class Upload
{
    public static function uploadBase64($base64, $folder) {
        $data = [
            'base64' => $base64,
            'folder' => $folder
        ];
        $uploadToFirebase = Curl::api_post('upload_to_firebase', $data);
        return $uploadToFirebase; // https://storage.googleapis.com/...
    }
}
