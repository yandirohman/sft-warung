<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Dompet extends Model
{
    use HasFactory;

    protected $table = 'dompet';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public static function decSaldo($penerimaId, $nominal) {
        $saldoExist = self::where('penerima_id', $penerimaId)->first();
        if ($saldoExist) {
            $jumlahSaldoBefore = $saldoExist->saldo;
            $jumlahSaldoAfter = $jumlahSaldoBefore - $nominal;
            self::where('penerima_id', $penerimaId)->update([
                'saldo' => $jumlahSaldoAfter,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            return true;
        } else {
            return false;
        }
    }

    public static function getDataById($id) {
        return self::find($id);
    }

    public static function getDataByIdPenerima($penerimaId) {
        $data = self::where('penerima_id', $penerimaId)->first();
        return $data;
    }

    public static function pilih($id)
    {
        return self::select('gelombang')
        ->where('gelombang', $id)->get();
    }

    public static function store($data)
    {
        foreach ($data as $key => $value)
        {
            unset($data[$key]['is_cair']);
            unset($data[$key]['created_at']);
        }

        return self::insert($data);
    }
}
