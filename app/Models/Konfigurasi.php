<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Konfigurasi extends Model
{
    use HasFactory;
    protected $table = 'konfigurasi';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public static function getData()
    {
        $data = self::select('id', 'tahun', 'bulan',
                             'besaran_dana', 'is_active')
                    ->get();

        return $data;
    }

    public static function getDataActive()
    {
        $data = self::where('is_active', 1)
                    ->select('id', 'tahun', 'bulan',
                             'besaran_dana', 'is_active')
                    ->first();

        return $data;
    }

    public static function updateData($id, $data)
    {
        self::where('is_active', 1)->update(['is_active' => 0]);
        self::where('id', $id)->update([
            'is_active' => $data->is_active,
            'besaran_dana' => $data->besaran_dana
        ]);
    }

}
