<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Hash;
use Str;

class Warung extends Model
{
    use HasFactory;
    protected $table = 'warung';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    // Funcation for Role Dinsos

    public static function getData($paginate = true)
    {
        $data = self::where('deleted_at', null)
                    ->select('id', 'id_kel', 'username', 'nama_warung',
                             'nama_pemilik', 'alamat', 'nik', 'hp', 'created_at',
                             'deleted_at');

        if($paginate == true){
            $data = $data->paginate(10);
        }else{
            $data = $data->get();
        }


        return $data;
    }

    public static function getDataById($warungId)
    {
        $dataWarung = self::where('id', $warungId)
                    ->select('id', 'id_kel', 'username', 'nama_warung',
                             'nama_pemilik', 'alamat', 'nik', 'hp', 'created_at',
                             'deleted_at')
                    ->first();

        return $dataWarung;
    }

    public static function getDataByIdKel($kelId)
    {
        $dataWarung = self::where('id_kel', $kelId)
                    ->select('id', 'id_kel', 'username', 'nama_warung',
                             'nama_pemilik', 'alamat', 'nik', 'hp', 'created_at',
                             'deleted_at')
                    ->paginate(10);

        return $dataWarung;
    }

    public static function cariWarung($data)
    {
        $dataWarung = self::where('deleted_at', null)
                    ->where('nama_warung', 'like', '%'.$data.'%')
                    ->select('id', 'id_kel', 'username', 'nama_warung',
                             'nama_pemilik', 'alamat', 'nik', 'hp', 'created_at',
                             'deleted_at')
                    ->paginate(10);

        return $dataWarung->appends(['data' => $data]);
    }

    public static function store($data, $warungId = null)
    {
        if ($warungId) {
            unset($data['username']);
            $data = self::where('id', $warungId)->update($data);

            return true;
        } else {
            $data['kode_akses'] = Str::random(8);
            $data['created_at'] = date('Y-m-d H:i:s');
            $id = self::insertGetId($data);

            // Insert to Table Users
            DB::table('users')->insert([
                'name' => $data['nama_warung'],
                'username' => $data['username'],
                'password' => Hash::make($data['kode_akses']),
                'role' => 2,
            ]);

            return $id;
        }
    }

    public static function updateUsername($warungId)
    {
        $data = self::where('id', $warungId)->first();
        $username = strtolower(str_replace(' ', '', $data->nama_warung)).$data->id;

        self::where('id', $warungId)->update([
            'username' => $username
        ]);
    }

    public static function deleteWarung($warungId)
    {
        $data = self::where('id', $warungId)->first();

        self::where('id', $warungId)->update([
            'deleted_at' => date('Y-m-d H:i:s')
        ]);
    }

    // Function for Role Warung

    public static function getTotalSaldoSemuaWarungGelombang($gelombang){
        return self::leftjoin('rembes', 'rembes.warung_id', 'warung.id')
                    ->select(DB::raw('SUM(rembes.saldo_total) as saldo_total, SUM(rembes.saldo_rembes) as saldo_rembes'))
                    ->where('rembes.gelombang', $gelombang)
                    ->whereNull('deleted_at')
                    ->first();
    }

    public static function getTotalSaldoSemuaWarungSemuaGelombang(){
        return self::leftjoin('rembes', 'rembes.warung_id', 'warung.id')
                    ->select(DB::raw('SUM(rembes.saldo_total) as saldo_total'))
                    ->first();
    }

    public static function getTotalSaldoWarung(String $username){
        return self::leftjoin('rembes', 'rembes.warung_id', 'warung.id')->select(DB::raw('SUM(rembes.saldo_total - rembes.saldo_rembes) as saldo'))->where('warung.username', $username)->first()->saldo;
    }

    public static function getTotalSaldoRembesWarung(String $username)
    {
        return self::leftjoin('rembes', 'rembes.warung_id', 'warung.id')->select('rembes.saldo_rembes as saldo')->where('warung.username', $username)->first()->saldo;
    }

    public static function getPendapatanWarungByUsername(String $username){
        $data = self::leftJoin('rembes as r', 'warung.id', 'r.warung_id')
                    ->where('warung.username', $username)
                    ->select('warung.id', 'warung.nama_warung', 'warung.alamat', 'r.saldo_total', 'r.saldo_rembes')
                    ->first();
        return $data;
    }

    public static function getPendapatanSemuaWarungGelombang($gelombang, $search = null, $pencairanFrom = null, $pencairanTo = null, $paginate = 'Ya'){
        $data = self::leftJoin('rembes as r', 'warung.id', 'r.warung_id')
                    ->select('warung.id', 'warung.nama_warung', 'warung.alamat', 'r.saldo_total', 'r.saldo_rembes')
                    ->where('r.gelombang', $gelombang)
                    ->where('warung.nama_warung', 'like', '%'.$search.'%')
                    ->whereNull('deleted_at');

        if(!empty($pencairanFrom) && !empty($pencairanTo)){
            $data = $data->whereBetween('r.saldo_rembes', [$pencairanFrom, $pencairanTo]);
        }

        if($paginate == 'tidak'){
            return $data->get();
        }

        return $data->paginate(5);
    }

    public static function getPendapatanSemuaWarungBerdasarkanRangeTanggal($tanggalAwal, $tanggalAkhir, $search = null, $pencairanFrom = null, $pencairanTo = null, $paginate = 'Ya'){
        $data = self::leftJoin('rembes as r', 'warung.id', 'r.warung_id')
                    ->select('warung.id', 'warung.nama_warung', 'warung.alamat', 'r.saldo_total', 'r.saldo_rembes')
                    ->whereBetween('r.created_at', [$tanggalAwal.' 00:00:00', $tanggalAkhir.' 00:00:00'])
                    ->where('warung.nama_warung', 'like', '%'.$search.'%');

        if(!empty($pencairanFrom) && !empty($pencairanTo)){
            $data = $data->whereBetween('r.saldo_rembes', [$pencairanFrom, $pencairanTo]);
        }

        return $data->paginate(5);
    }

    public static function getSemuaWarungTerlaris($cari = null){
        return self::leftJoin('transaksi_pembelian as tp', 'tp.warung_id', 'warung.id')
                    ->select(DB::raw('warung.id, warung.nama_warung, COUNT(distinct tp.penerima_id) as jumlah_penerima, SUM(tp.total_harga) as nominal'))
                    ->where('warung.nama_warung', 'like', '%'.$cari.'%')
                    ->whereNull('deleted_at')
                    ->groupBy('warung.id')
                    ->orderBy('nominal', 'DESC')
                    ->paginate(10);
    }

    public static function checkWarung(String $username) {
        return self::select('id', 'nama_warung', 'username')->where('username', $username)->first();
    }

    public static function ambilWarungDenganId(int $id)
    {
        return self::select('id', 'nama_warung', 'username')->where('id', $id)->first();
    }

    public static function ambilWarungDenganUsername(String $username)
    {
        return self::select('id', 'nama_warung', 'username', 'hp', 'alamat', 'nama_pemilik')->where('username', $username)->first();
    }

    public static function cari($cari)
    {
        $data = self::leftjoin('penerima', 'penerima.id', 'transaksi_pembelian.penerima_id')
        ->select('transaksi_pembelian.id', 'penerima.nik', 'penerima.nama', 'transaksi_pembelian.total_harga', 'transaksi_pembelian.created_at')
        ->where('transaksi_pembelian.warung_id', $cari);

        return $data->get();
    }

    public static function getMyId() {
        $userId = Session::get('id');
        $userExist = DB::table('users')->where('id', $userId)->select(['username'])->first();
        if ($userExist) {
            $username = $userExist->username;
            $warungExist = DB::table('warung')->where('username', $username)->select(['id'])->first();
            if ($warungExist) {
                $idWarung = $warungExist->id;
                return $idWarung;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function getLaporanLabaRugi($cari = null, $kelurahan = null, $pagination = 'ya'){
        $data = self::selectRaw('warung.id, warung.nama_warung, warung.nama_pemilik,
                                SUM(ch.harga_beli * dtp.jumlah_produk) AS modal,
                                SUM(dtp.sub_total) AS terjual,
                                SUM( dtp.sub_total - (ch.harga_beli * dtp.jumlah_produk)) AS laba_rugi,
                                MAX(r.saldo_total - r.saldo_rembes) AS piutang,
                                MAX(r.saldo_rembes) AS sudah_cair,
                                (r.saldo_rembes - SUM(ch.harga_beli * dtp.jumlah_produk)) AS terima_laba
                                ')
                    ->join('rembes as r', 'r.warung_id', 'warung.id')
                    ->join('config_harga as ch', 'ch.warung_id', 'warung.id')
                    ->join('produk as p', 'p.id', 'ch.produk_id')
                    ->join('detail_transaksi_pembelian as dtp', function($join){
                        $join->on('dtp.produk_id', 'p.id');
                        $join->on('dtp.warung_id', 'warung.id');
                    })
                    ->where('warung.nama_warung', 'like', '%'.$cari.'%');

        if(!is_null($kelurahan)){
            $data = $data->where('warung.id_kel', $kelurahan);
        }

        $data = $data->groupBy('warung.id');

        if($pagination == 'tidak'){
            return $data->get();
        }

        return $data->paginate(10);

    }
}
