<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\DetailPaket;
use DB;
use Session;

class Paket extends Model
{
    use HasFactory;
    protected $table = 'paket';
    protected $guarded = ['id'];

    public static function getData() {
        $warungId = Warung::getMyId();
        return self::where('warung_id', $warungId)->get();
    }

    public static function store($data) {
        self::insert($data);
    }

    public static function destroy($id) {
        self::where('id', $id)->delete();
    }

    public static function getDataById($id) {
        $data = self::find($id);
        $listProduk = DetailPaket::join('produk', 'detail_paket.produk_id', 'produk.id')
            ->where('detail_paket.paket_id', $id)
            ->select([
                'detail_paket.jumlah_barang as jumlah',
                'detail_paket.harga_satuan as harga',
                'detail_paket.sub_total',
                'produk.id as produk_id',
                'produk.nama',
                'produk.satuan'
            ])->get();
        $data->list_produk = $listProduk;
        return $data;
    }

    public static function getDataByWarungId($warungId) {
        return self::where('warung_id', $warungId)
            ->select([
                'id',
                'nama',
                'jml_produk',
                'total_harga'
            ])
            ->get();
    }

    public static function storeData($data) {
        self::insert([
            'nama' => $data['nama'],
            'total_harga' => $data['total_harga'],
            'jml_produk' => $data['jml_produk'],
            'warung_id' => $data['warung_id'],
            'list_produk' => '[]',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function deleteData($id) {
        DetailPaket::deleteByIdPaket($id);
        self::where('id', $id)->delete();
    }
}
