<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Helper\GeneralHelper;
use App\Models\DetailTransaksiPembelian;
use App\Models\RiwayatDompet;
use App\Models\Dompet;
use App\Models\Rembes;
use Throwable;
use DateTime;
use DB;

class TransaksiPembelian extends Model
{
    use HasFactory;

    protected $table = 'transaksi_pembelian';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public static function getTransaksiSemuaWarung($filter = 'hari ini', $cari = null){
        $data = self::leftJoin('penerima as p', 'p.id', 'transaksi_pembelian.penerima_id')
                    ->leftJoin('warung as w', 'w.id', 'transaksi_pembelian.warung_id')
                    ->select(DB::raw('w.id as warung_id'),'w.nama_warung', 'p.nama', 'p.nik', 'transaksi_pembelian.created_at', 'transaksi_pembelian.id', 'transaksi_pembelian.total_harga')
                    ->where('p.nama', 'like', '%'.$cari.'%')
                    ->orderBy('transaksi_pembelian.id', 'DESC');

        if($filter == 'hari ini'){
            $data = $data->where(DB::raw('DATE(transaksi_pembelian.created_at)'), date('Y-m-d'));
        }elseif($filter == 'bulan ini'){
            $data = $data->where(DB::raw('MONTH(transaksi_pembelian.created_at)'), date('m'));
        }elseif($filter == 'minggu ini'){
            $senin = new DateTime(date('Y-m-d'));
            $minggu = new DateTime(date('Y-m-d'));
            $senin = $senin->modify('Last Monday');
            $minggu = $minggu->modify('Next Sunday');
            $data = $data->where('transaksi_pembelian.created_at', [$senin, $minggu]);
        }

        return $data->paginate(10);
    }

    public static function getRiwayatTransaksiWarung(String $username, String $tanggal){
        $warungID = Warung::ambilWarungDenganUsername($username);
        $data = self::leftjoin('penerima', 'penerima.id', 'transaksi_pembelian.penerima_id')
                        ->select('transaksi_pembelian.id', 'penerima.nik', 'penerima.nama', 'transaksi_pembelian.total_harga', 'transaksi_pembelian.created_at')
                        ->where('transaksi_pembelian.warung_id', $warungID->id);
        if($tanggal != 'null'){
            $tanggalArr = explode(' - ', $tanggal);
            $tanggalFrom = explode('/', $tanggalArr[0]);
            $tanggalTo = explode('/', $tanggalArr[1]);
            $tanggalFromTgl = $tanggalFrom[1];
            $tanggalFromBln = $tanggalFrom[0];
            $tanggalFromThn = $tanggalFrom[2];
            $tanggalToTgl = $tanggalTo[1];
            $tanggalToBln = $tanggalTo[0];
            $tanggalToThn = $tanggalTo[2];
            $tanggalFromFix = $tanggalFromThn.'-'.$tanggalFromBln.'-'.$tanggalFromTgl;
            $tanggalToFix = $tanggalToThn.'-'.$tanggalToBln.'-'.$tanggalToTgl;
            $data->whereBetween('transaksi_pembelian.created_at', [$tanggalFromFix, $tanggalToFix]);
        }

        return $data->get();
    }

    public static function getRiwayatTransaksiPenerima(Int $penerimaId){
        return self::leftjoin('penerima as p', 'p.id', 'transaksi_pembelian.penerima_id')
                    ->select('p.nama','transaksi_pembelian.id','transaksi_pembelian.gelombang', 'transaksi_pembelian.total_harga', 'transaksi_pembelian.created_at')->where('transaksi_pembelian.penerima_id', $penerimaId)->paginate(5);
    }

    public static function getTransaksiRembesWarung(String $username, String $tipe, String $cari, String $tanggal)
    {
        $warungID = Warung::ambilWarungDenganUsername($username);
        $data = self::leftjoin('penerima', 'penerima.id', 'transaksi_pembelian.penerima_id')
                        ->select('transaksi_pembelian.id', 'penerima.nik', 'penerima.nama', 'transaksi_pembelian.total_harga', 'transaksi_pembelian.created_at')
                        ->where('transaksi_pembelian.warung_id', $warungID->id);
        if($tipe == 'cair'){
            $data->where('transaksi_pembelian.rembesed_at', '!=', null);
        } else {
            $data->where('transaksi_pembelian.rembesed_at', null);
        }
        if ($tanggal != 'null') {
            $tanggalArr = explode(' - ', $tanggal);
            $tanggalFrom = explode('/', $tanggalArr[0]);
            $tanggalTo = explode('/', $tanggalArr[1]);
            $tanggalFromTgl = $tanggalFrom[1];
            $tanggalFromBln = $tanggalFrom[0];
            $tanggalFromThn = $tanggalFrom[2];
            $tanggalToTgl = $tanggalTo[1];
            $tanggalToBln = $tanggalTo[0];
            $tanggalToThn = $tanggalTo[2];
            $tanggalFromFix = $tanggalFromThn.'-'.$tanggalFromBln.'-'.$tanggalFromTgl;
            $tanggalToFix = $tanggalToThn.'-'.$tanggalToBln.'-'.$tanggalToTgl;
            if($cari != 'null' && $tanggal != 'null'){
                if(is_numeric($cari)){
                    $data->where('penerima.nik', $cari);
                    $data->whereBetween('transaksi_pembelian.created_at', [$tanggalFromFix, $tanggalToFix]);
                } else {
                    $data->where('penerima.nama', 'like', '%' .  $cari . '%');
                    $data->whereBetween('transaksi_pembelian.created_at', [$tanggalFromFix, $tanggalToFix]);
                }
            } else {
                $data->whereBetween('transaksi_pembelian.created_at', [$tanggalFromFix, $tanggalToFix]);
            }
        }

        return $data->get();
    }

    public static function getTotalHargaTransaksiPerBulan(){
        $awal = new DateTime(date('Y').'-01-01');
        $akhir = new DateTime(date('Y').'-12-31');
        return self::select(DB::raw('SUM(total_harga) as total_harga, MONTH(created_at) as bulan'))
                    ->groupBy(DB::raw('MONTH(created_at)'))
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->get();
    }

    public static function getTotalHargaTransaksiPerGelombang(){
        return self::select(DB::raw('SUM(total_harga) as total_harga, gelombang'))
                    ->groupBy('gelombang')
                    ->get();
    }

    public static function getTotalHargaTransaksiBerdasarkanRangeTanggal($tanggalAwal, $tanggalAkhir, $perTanggal = false){
        if($perTanggal == true){
            return self::select(DB::raw('SUM(total_harga) as total_harga, created_at'))
                        ->whereBetween('created_at', [$tanggalAwal.' 00:00:00', $tanggalAkhir.' 00:00:00'])
                        ->groupBy('created_at')
                        ->get();
        }else{
            return self::select(DB::raw('SUM(total_harga) as total_harga'))
                        ->whereBetween('created_at', [$tanggalAwal.' 00:00:00', $tanggalAkhir.' 00:00:00'])
                        ->get();
        }
    }

    public static function getDetailTransaksi(int $id){
        return self::leftJoin('detail_transaksi_pembelian as dtp', 'dtp.transaksi_pembelian_id', 'transaksi_pembelian.id')
                    ->leftJoin('produk as p', 'p.id', 'dtp.produk_id')
                    ->select('p.nama', 'dtp.jumlah_produk', 'dtp.harga_satuan', 'dtp.sub_total', 'transaksi_pembelian.total_harga')
                    ->where('transaksi_pembelian.id', $id)
                    ->get();
    }

    public static function getDetailTransaksiRiwayat(int $id){
        return self::leftJoin('detail_transaksi_pembelian as dtp', 'dtp.transaksi_pembelian_id', 'transaksi_pembelian.id')
                    ->leftJoin('produk as p', 'p.id', 'dtp.produk_id')
                    ->leftJoin('penerima', 'penerima.id', 'transaksi_pembelian.penerima_id')
                    ->leftJoin('warung as w', 'w.id', 'transaksi_pembelian.warung_id')
                    ->select('w.nama_warung',  'w.id as warung_id','p.nama as nama_produk', 'transaksi_pembelian.foto_transaksi', 'dtp.jumlah_produk', 'dtp.harga_satuan', 'dtp.sub_total', 'transaksi_pembelian.total_harga', 'transaksi_pembelian.id', 'penerima.nama', 'penerima.nik', 'transaksi_pembelian.created_at', 'transaksi_pembelian.foto_transaksi')
                    ->where('transaksi_pembelian.id', $id)
                    ->whereNotNull('p.nama')
                    ->get();
    }

    private static function preDataTransaksiPembelian($request, $paket) {
        $warungId = Warung::getMyId();
        $gelombang = $request->gelombang;
        $dataStore = [
            'warung_id' => $warungId,
            'penerima_id' => $request->penerima_id,
            'gelombang' => $gelombang,
            'total_harga' => $paket->total_harga,
            'foto_transaksi' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
        return $dataStore;
    }

    private static function preDataRiwayatDompet($request, $paket, $trxId) {
        $penerimaId = $request->penerima_id;
        $gelombang = $request->gelombang;
        $dataGelombang = Gelombang::getDataByPenerimaAndGelombang($penerimaId, $gelombang);
        $dataGelombangId = $dataGelombang->id;
        $sumber = $request->sumber;
        $dataStore = [
            'penerima_id' => $request->penerima_id,
            'transaksi_pembelian_id' => $trxId,
            'gelombang_id' => $dataGelombangId,
            'nominal' => $paket->total_harga,
            'sumber' => $sumber,
            'jenis' => 'keluar',
            'created_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s')
        ];
        return $dataStore;
    }

    private static function preDataDetailTransaksiPembelian($request, $paket, $trxId) {
        $dataStore = [];
        $penerimaId = $request->penerima_id;
        $warungId = Warung::getMyId();
        $listProduk = $paket->list_produk;
        foreach($listProduk as $produk) {
            $subTotal = $produk->harga * $produk->jumlah;
            $itemStore = [
                'transaksi_pembelian_id' => $trxId,
                'penerima_id' => $penerimaId,
                'warung_id' => $warungId,
                'produk_id' => $produk->produk_id,
                'jumlah_produk' => $produk->jumlah,
                'harga_satuan' => $produk->harga,
                'sub_total' => $subTotal,
                'created_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ];
            array_push($dataStore, $itemStore);
        }
        return $dataStore;
    }

    public static function beli($request, $paket) {
        DB::beginTransaction();
        try {

            $totalHarga = $paket->total_harga;
            $penerimaId = $request->penerima_id;
            $warungId = Warung::getMyId();
            $gelombang = $request->gelombang;

            # step 1 insert transaksi pembelian
            $dataTransaksiPembelian = self::preDataTransaksiPembelian($request, $paket);
            // dd($dataTransaksiPembelian);
            $trxId = TransaksiPembelian::insertGetId($dataTransaksiPembelian);

            # step 2 insert riwayat dompet
            $dataRiwayatDompet = self::preDataRiwayatDompet($request, $paket, $trxId);
            RiwayatDompet::insert($dataRiwayatDompet);

            # step 3 insert batch detail setiap transaksi
            $dataDetailTransaksiPembelian = self::preDataDetailTransaksiPembelian($request, $paket, $trxId);
            DetailTransaksiPembelian::insert($dataDetailTransaksiPembelian);

            # step 4 update (kurangi) dompet penerima
            Dompet::decSaldo($penerimaId, $totalHarga);

            # step 5 update (tambah saldo) rembes warung
            Rembes::incSaldo($warungId, $totalHarga, $gelombang);

            DB::commit();
            return $trxId;

        } catch (Throwable $err) {
            DB::rollBack();
            return $err;
        }
    }

    public static function updateFotoPenerima(int $id, $urlFirebase) {
        return self::where('id', $id)->update([
            'foto_transaksi' => $urlFirebase
        ]);
    }

    public static function selectMultipleIdTransaksiPembelian(array $ids){
        return self::join('penerima as p', 'p.id', 'transaksi_pembelian.penerima_id')
                    ->whereIn('transaksi_pembelian.id', $ids)
                    ->select('transaksi_pembelian.id', 'transaksi_pembelian.foto_transaksi', 'transaksi_pembelian.total_harga', 'transaksi_pembelian.created_at', 'p.nama')
                    ->get();
    }

    public static function getSemuaTransaksiWarung($warungId, $cariPenerima = null, $bulan = null, $tahun = null){
        $data = self::join('penerima as p', 'p.id', 'transaksi_pembelian.penerima_id')
                    ->select('transaksi_pembelian.id', 'transaksi_pembelian.foto_transaksi', 'p.nama', 'p.nik', 'transaksi_pembelian.total_harga', 'transaksi_pembelian.rembesed_at', 'transaksi_pembelian.created_at')
                    ->where('transaksi_pembelian.warung_id', $warungId)
                    ->whereYear('transaksi_pembelian.created_at', $tahun)
                    ->whereMonth('transaksi_pembelian.created_at', $bulan)
                    ->where('p.nama', 'like', '%'.$cariPenerima.'%')
                    ->paginate(10);
        return $data;
    }

    public static function getSemuaTransaksiWarungRange($warungId, $req){
        $tanggalFrom = $req->tanggal_from;
        $bulanFrom = $req->bulan_from;
        $tahunFrom = $req->tahun_from;
        $tanggalTo = $req->tanggal_to;
        $bulanTo = $req->bulan_to;
        $tahunTo = $req->tahun_to;
        $dateFrom = $tahunFrom.'-'.$bulanFrom.'-'.$tanggalFrom;
        $dateTo = $tahunTo.'-'.$bulanTo.'-'.$tanggalTo;
        $cariPenerima = $req->penerima ?? null;
        $data = self::join('penerima as p', 'p.id', 'transaksi_pembelian.penerima_id')
                    ->select('transaksi_pembelian.id', 'transaksi_pembelian.foto_transaksi', 'p.nama', 'p.nik', 'transaksi_pembelian.total_harga', 'transaksi_pembelian.rembesed_at', 'transaksi_pembelian.created_at')
                    ->where('transaksi_pembelian.warung_id', $warungId)
                    ->whereBetween('transaksi_pembelian.created_at', [$dateFrom, $dateTo])
                    ->where('p.nama', 'like', '%'.$cariPenerima.'%')
                    ->paginate(10);
        return $data;
    }

    public static function getTransaksiBelumRembes($request) {
        $ids = !empty($request->ids) ? explode(',', $request->ids) : null;
        $trxIds = !empty($request->trxIds) ? explode(',', $request->trxIds) : null;
        $isExist = self::select(DB::raw("COUNT(transaksi_pembelian.id) as count"))
            ->whereNull('transaksi_pembelian.rembesed_at')
            ->whereYear('transaksi_pembelian.created_at', $request->tahun)
            ->whereMonth('transaksi_pembelian.created_at', $request->bulan);
        if ($ids) {
            $isExist->whereIn('warung_id', $ids);
        }
        if ($trxIds) {
            $isExist->whereIn('transaksi_pembelian.id', $trxIds);
        }
        if ($request->transaksiId) {
            $isExist->where('transaksi_pembelian.id', $request->transaksiId);
        }
        if($request->filterRembes != null) {
            $isExist->leftJoin('rembes', 'rembes.warung_id', 'transaksi_pembelian.warung_id')
                ->whereRaw("(rembes.saldo_total - rembes.saldo_rembes) >= $request->rembesTerendah")
                ->whereRaw("(rembes.saldo_total - rembes.saldo_rembes) <= $request->rembesTerbesar");
        }
        $isExist = $isExist->first()->count;
        if (!$isExist) {
            return 0;
        } else {
            $belumRembes = self::select(
                    'transaksi_pembelian.warung_id',
                    DB::raw('
                        GROUP_CONCAT(IF(rembesed_at IS NULL, transaksi_pembelian.id, NULL) ORDER BY transaksi_pembelian.id ASC) as transaksi_pembelian_ids,
                        COUNT(IF(rembesed_at IS NULL, transaksi_pembelian.id, NULL)) as jumlah_transaksi,
                        SUM(total_harga) as total_saldo,
                        SUM(IF(rembesed_at IS NULL, total_harga, 0)) as total_rembes,
                        CURRENT_TIMESTAMP() as created_at,
                        CURRENT_TIMESTAMP() as updated_at
                    '),
                )
                ->whereYear('transaksi_pembelian.created_at', $request->tahun)
                ->whereMonth('transaksi_pembelian.created_at', $request->bulan);
            if ($ids) {
                $belumRembes->whereIn('transaksi_pembelian.warung_id', $ids);
            }

            if($request->filterRembes != null) {
                $belumRembes->leftJoin('rembes', 'rembes.warung_id', 'transaksi_pembelian.warung_id')
                    ->whereRaw("(rembes.saldo_total - rembes.saldo_rembes) >= $request->rembesTerendah")
                    ->whereRaw("(rembes.saldo_total - rembes.saldo_rembes) <= $request->rembesTerbesar");
            }
            return $belumRembes->groupBy('transaksi_pembelian.warung_id')
                ->get()
                ->toArray(); # untuk insertBatch dengan array
        }
    }
    public static function rembeskanTransaksi($request) {
        $data = [
            'rembesed_at' => date('Y-m-d H:i:s')
        ];
        return self::whereNull('rembesed_at')
            ->whereYear('created_at', $request->tahun)
            ->whereMonth('created_at', $request->bulan)
            ->update($data);
    }

    public static function getDetailPenerimaTransaksi(int $id){
        return self::leftJoin('penerima', 'penerima.id', 'transaksi_pembelian.penerima_id')
                    ->select('penerima.nama', 'penerima.nik', 'penerima.hp', 'penerima.alamat', 'transaksi_pembelian.id', 'transaksi_pembelian.foto_transaksi', 'transaksi_pembelian.created_at', 'transaksi_pembelian.rembesed_at')
                    ->where('transaksi_pembelian.id', $id)
                    ->first();
    }

    public static function getTransaksiBelumRembesByIdWarung($request, $paginate = true){
        $cari = $request->cari ?? null;
        $bulan = $request->bulan ?? null;
        $tahun = $request->tahun ?? null;
        $rembesTerendah = $request->rembesTerendah ?? null;
        $rembesTerbesar = $request->rembesTerbesar ?? null;
        $filterRembes = $request->filterRembes ?? null;
        $ids = !empty($request->ids) ? explode(',', $request->ids) : null;

        $data = self::join('warung as w', 'w.id', 'transaksi_pembelian.warung_id')
                    ->join('penerima as p', 'p.id', 'transaksi_pembelian.penerima_id')
                    ->select('transaksi_pembelian.created_at', 'transaksi_pembelian.id', 'p.nik', 'p.nama', 'transaksi_pembelian.total_harga');

        if($cari != null){
            $data->where('p.nama', 'like', '%' .  $cari . '%');
        }

        if($bulan != null && $tahun != null){
            $data->whereMonth('transaksi_pembelian.created_at', sprintf("%02d", $bulan));
            $data->whereYear('transaksi_pembelian.created_at', $tahun);
        }

        if($filterRembes != null) {
            $data->havingRaw("(transaksi_pembelian.total_harga) >= $rembesTerendah");
            $data->havingRaw("(transaksi_pembelian.total_harga) <= $rembesTerbesar");
        }
        $data->orderByRaw('(transaksi_pembelian.total_harga) DESC');

        if(!empty($ids)){
            $data = $data->whereIn('transaksi_pembelian.id', $ids);
        }

        if($paginate == false){
            return $data->get();
        }

        return $data->paginate(10);
    }
}
