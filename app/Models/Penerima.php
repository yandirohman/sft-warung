<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Penerima extends Model
{
    use HasFactory;
    protected $table = 'penerima';
    protected $guarded = ['id'];

    public static function getTotalSaldoSemuaPenerima()
    {
        return self::leftJoin('dompet as d', 'd.penerima_id', 'penerima.id')
                    ->select(DB::raw('SUM(d.saldo) as saldo'))
                    ->first();
    }

    public static function getDataById($id)
    {
        $dataExist = self::leftJoin('dompet', 'penerima.id', '=', 'dompet.penerima_id')
            ->where('penerima.id', $id)
            ->select([
                'penerima.*',
                'dompet.saldo'
            ])->first();
        return $dataExist;
    }

    public static function getDataByIdAndGelombang($id, $gelombang) {
        $dataExist = self::leftJoin('dompet', 'penerima.id', '=', 'dompet.penerima_id')
            ->join('gelombang', 'penerima.id', 'gelombang.penerima_id')
            ->where('penerima.id', $id)
            ->where('gelombang.gelombang', $gelombang)
            ->select([
                'penerima.*',
                'dompet.saldo'
            ])->first();
        return $dataExist;
    }

    public static function simpanGetId($data)
    {
        return self::insertGetId($data);
    }

    public static function pilih($select)
    {
        return self::select($select)->leftJoin('gelombang', 'gelombang.penerima_id', '=', 'penerima.id')
        ->where('penerima.deleted_at', NULL)->orderBy('gelombang.created_at', 'DESC')->paginate(10);
    }

    public static function filter($select, $id)
    {
        return self::select($select)->leftJoin('gelombang', 'gelombang.penerima_id', '=', 'penerima.id')
        ->where('penerima.deleted_at', NULL)
        ->where('gelombang.gelombang', $id)
        ->orderBy('gelombang.created_at', 'DESC')->paginate(10);
    }

    public static function cairkan($select, $id)
    {
        return self::select($select)->leftJoin('gelombang', 'gelombang.penerima_id', '=', 'penerima.id')
        ->where('penerima.deleted_at', NULL)
        ->where('gelombang.gelombang', $id)
        ->get();
    }

    public static function cari($select, $cari)
    {
        $penerima = self::select($select)->leftJoin('gelombang', 'gelombang.penerima_id', '=', 'penerima.id')
        ->where('penerima.deleted_at', NULL)
        ->where('penerima.nama', 'like', '%'.$cari.'%')
        ->orWhere('penerima.nik', 'like', '%'.$cari.'%')
        ->orderBy('gelombang.created_at', 'DESC')->paginate(10);
        return $penerima->appends(['cari' => $cari]);
    }

    public static function pilihFirstJoin($select, $id)
    {
        return self::select($select)->leftJoin('gelombang', 'gelombang.penerima_id', '=', 'penerima.id')
        ->where('penerima_id', $id)->first();
    }

    public static function pilihFirst($select, $id)
    {
        return self::select($select)->where('id', $id)->first();
    }

    public static function ubah($data, $id)
    {
        return self::where('id', $id)->update($data, $id);
    }

    public static function getDataForExport()
    {
        $data = self::leftJoin('gelombang', 'penerima.id', 'gelombang.penerima_id')
            ->select('penerima.id', 'penerima.nama', 'penerima.nik',
                     'penerima.kk', 'penerima.hp', 'penerima.alamat',
                     'gelombang.gelombang', 'gelombang.besaran_dana')
            ->where('penerima.deleted_at', null)
            ->get();

        return $data;
    }
}
