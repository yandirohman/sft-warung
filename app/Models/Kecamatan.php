<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class Kecamatan extends Authenticatable
{
    protected $table = 'kecamatan';
    protected $guarded = ['id_kec'];
    protected $primaryKey = 'id_kec';

    public static function pilih($select, $id)
    {
        return self::select($select)->where('id_kab', $id)->get();
    }

    public static function getDataByKabId($kabId)
    {
        $data = self::where('id_kab', $kabId)
            ->select('id_kec', 'nama')
            ->get();

        return response()->json($data);
    }
}
