<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use DateTime;

class Gelombang extends Model
{
    use HasFactory;

    protected $table = 'gelombang';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public static function getTotalDanaPerBulan(){
        $awal = new DateTime(date('Y').'-01-01');
        $akhir = new DateTime(date('Y').'-12-31');
        return self::select(DB::raw('SUM(besaran_dana) as total_dana, MONTH(created_at) as bulan'))
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->groupBy(DB::raw('MONTH(created_at)'))
                    ->get();
    }

    public static function getTotalDanaPerGelombang(){
        return self::select(DB::raw('SUM(besaran_dana) as total_dana, gelombang'))
                    ->groupBy('gelombang')
                    ->get();
    }

    public static function getTotalDanaGelombang($gelombang){
        return self::select(DB::raw('SUM(besaran_dana) as total_dana'))
                    ->where('gelombang', $gelombang)
                    ->first();
    }

    public static function getTotalDanaBerdasarkanRangeTanggal(string $tanggalAwal, string $tanggalAkhir, $perTanggal = false){
        if($perTanggal == true){
            $data = self::select(DB::raw('SUM(besaran_dana) as total_dana, created_at'))
                        ->whereBetween('created_at', [new DateTime($tanggalAwal), new DateTime($tanggalAkhir)])
                        ->groupBy('created_at')
                        ->get();
        }else{
            $data = self::select(DB::raw('SUM(besaran_dana) as total_dana'))
                        ->whereBetween('created_at', [new DateTime($tanggalAwal), new DateTime($tanggalAkhir)])
                        ->first();
        }

        return $data;
    }

    public static function simpan($data)
    {
        return self::insert($data);
    }

    public static function ubah($id)
    {
        return self::where('is_cair', 0)
        ->whereYear('gelombang.created_at', date('Y'))
        ->where('gelombang', $id)->where('deleted_at', null)->update(array('is_cair' => 1));
    }

    public static function softDelete($data, $id)
    {
        return self::where('penerima_id', $id)->update($data, $id);
    }

    public static function getDataByPenerimaAndGelombang($penerimaId, $gelombang) {
        $dataExist = self::where('penerima_id', $penerimaId)->where('gelombang', $gelombang)->first();
        return $dataExist ? $dataExist : false;
    }

    public static function getDataCairkan($gelombang)
    {
        return self::leftJoin('penerima as p', 'p.id', '=', 'gelombang.penerima_id')
                ->where('gelombang.gelombang', $gelombang)
                ->where('p.deleted_at', NULL)
                ->where('gelombang.is_cair', 0)
                ->whereYear('gelombang.created_at', date('Y'))
                ->select('gelombang.penerima_id', 'gelombang.besaran_dana as saldo', 'gelombang.is_cair', 'gelombang.created_at', 'gelombang.gelombang')
                ->get()->toArray();
    }

}
