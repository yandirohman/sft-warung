<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class Kelurahan extends Authenticatable
{
    protected $table = 'kelurahan';
    protected $guarded = ['id_kel'];
    protected $primaryKey = 'id_kel';

    public static function pilih($select, $id)
    {
        return self::select($select)->where('id_kec', $id)->get();
    }

    public static function getDataByKecId($kecId)
    {
        $data = self::where('id_kec', $kecId)
            ->select('id_kel', 'nama')
            ->get();

        return response()->json($data);
    }
}
