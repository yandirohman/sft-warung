<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    use HasFactory;
    protected $table = 'provinsi';
    protected $guarded = ['id_prov'];
    protected $primaryKey = 'id_prov';

    public static function getProv()
    {
        $data = self::select('id_prov', 'nama')->get();
        return $data;
    }
}
