<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

class Notifikasi extends Model
{
    protected $table = 'notifikasi';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public static function getData() {
        $userId = Session::get('id');
        $datas = self::where('user_id', $userId)->limit(10)->get();
        return $datas;
    }

    public static function sendNotif($idUser, $idWarung, $data) {
        $insert = self::insert([
            'warung_id' => $idWarung,
            'user_id' => $idUser,
            'judul' => $data['judul'],
            'keterangan' => $data['keterangan'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
