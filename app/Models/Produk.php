<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Warung;
use DB;

class Produk extends Authenticatable
{
    protected $table = 'produk';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public static function simpan($data)
    {
        return self::create($data);
    }

    public static function ubah($data, $id)
    {
        return self::where('id', $id)->update($data, $id);
    }

    public static function pilih($select)
    {
        return self::select($select)->where('deleted_at', NULL)->orderBy('created_at', 'DESC')->paginate(10);
    }

    public static function cari($select, $cari)
    {
        $produk = self::select($select)
        ->where('deleted_at', NULL)
        ->where('nama', 'like', '%'.$cari.'%')->paginate(10);
        return $produk->appends(['cari' => $cari]);
    }

    public static function pilihFirst($select, $id)
    {
        return self::select($select)->where('id', $id)->first();
    }

    public static function getSemuaProdukTerlaris(){
        $data = self::leftJoin('detail_transaksi_pembelian as dtp', 'dtp.produk_id', 'produk.id')
                    ->select(DB::raw('produk.id, produk.nama, produk.satuan, SUM(dtp.jumlah_produk) as jumlah, SUM(sub_total) as nominal'))
                    ->groupBy('produk.id')
                    ->orderBy('jumlah', 'DESC')
                    ->get();
        return $data;
    }

    public static function getDataByName($str) {
        $datas = self::where('nama', 'like', '%'.$str.'%')
            ->limit(10)
            ->get();
        return $datas;
    }

    public static function getWarungProdukTerlaris($username){
        $idWarung = Warung::ambilWarungDenganUsername($username)->id;
        return self::leftjoin('detail_transaksi_pembelian as dtp', 'dtp.produk_id', 'produk.id')
                    ->select(DB::raw('produk.nama, SUM(dtp.jumlah_produk) as jumlah, SUM(sub_total) as nominal, produk.satuan, produk.foto_produk'))
                    ->where('dtp.warung_id', $idWarung)
                    ->groupBy('produk.id')
                    ->orderBy('jumlah', 'DESC')
                    ->get();
    }

    public static function getData($data = null, $id = null) {
        $query = self::whereNull('deleted_at');
        if ($data) {
            $query->selectRaw($data);
        }
        if ($id) {
            $query->where('id', $id);
            return $query->first();
        }
        return $query->get();
    }

    public static function countData() {
        return self::whereNull('deleted_at')->count();
    }

    public static function getProdukBerdasarkanHarga($cari = null){
        return self::select('produk.id', 'produk.nama', DB::raw('MIN(dp.harga_satuan) as harga_terendah, MAX(dp.harga_satuan) as harga_tertinggi'))
                    ->join('detail_paket as dp', 'dp.produk_id', 'produk.id')
                    ->join('paket as pk', 'pk.id', 'dp.paket_id')
                    ->where('produk.nama', 'like', '%'.$cari.'%')
                    ->whereNull('deleted_at')
                    ->groupBy('produk.id')
                    ->paginate(10);
    }

    public static function getProdukPerWarung($id){
        return self::select('produk.id', 'produk.nama', DB::raw('MIN(dp.harga_satuan) as harga_terendah, MAX(dp.harga_satuan) as harga_tertinggi'), 'w.id', 'w.nama_warung')
                    ->join('detail_paket as dp', 'dp.produk_id', 'produk.id')
                    ->join('paket as pk', 'pk.id', 'dp.paket_id')
                    ->join('warung as w', 'w.id', 'pk.warung_id')
                    ->where('produk.id', $id)
                    ->groupBy('produk.id', 'w.id')
                    ->get();
    }

    public static function getDataLeftJoinConfigHarga() {
        $idWarung = Warung::getMyId();
        $datas = self::leftJoin('config_harga', function($leftJoin) use ($idWarung) {
                $leftJoin->on('produk.id', 'config_harga.produk_id');
                $leftJoin->where('config_harga.warung_id', $idWarung);
            })->whereNull('produk.deleted_at')
            ->select([
                'produk.id as produk_id',
                'config_harga.warung_id',
                'config_harga.id as config_harga_id',
                'produk.nama',
                'produk.harga_terendah',
                'produk.harga_tertinggi',
                'produk.satuan',
                'config_harga.harga_beli'
            ])->whereNull('deleted_at')
            ->get();
        return $datas;
    }
}
