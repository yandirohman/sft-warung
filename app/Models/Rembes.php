<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use DateTime;

class Rembes extends Model
{
    use HasFactory;

    protected $table = 'rembes';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public static function insertInWarung($warungId)
    {
        $getGelombang = DB::table('konfigurasi')
                          ->where('is_active', 1)
                          ->pluck('bulan')->first();

        self::insert([
            'warung_id' => $warungId,
            'gelombang' => $getGelombang,
            'saldo_total' => 0,
            'saldo_rembes' => 0,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getTotalRembesPerGelombang(){
        return self::select(DB::raw('SUM(saldo_rembes) as total, gelombang'))
                    ->groupBy('gelombang')
                    ->get();
    }

    public static function getTotalRembesGelombang($gelombang){
        return self::select(DB::raw('SUM(saldo_rembes) as total'))
                    ->where('gelombang', $gelombang)
                    ->first();
    }

    public static function getTotalRembesBerdasarkanRangeTanggal(string $tangalAwal, string $tanggalAkhir, $perTanggal = false){
        if($perTanggal == true){
            return self::select(DB::raw('SUM(saldo_rembes) as total, created_at'))
                                    ->whereBetween('created_at', [new DateTime($tangalAwal), new DateTime($tanggalAkhir)])
                                    ->groupBy('created_at')
                                    ->get();
        }else{
            return self::select(DB::raw('SUM(saldo_rembes) as total'))
                        ->whereBetween('created_at', [new DateTime($tangalAwal), new DateTime($tanggalAkhir)])
                        ->first();
        }
    }

    public static function incSaldo($warungId, $nominal, $gelombang) {
        $saldoExist = self::where('warung_id', $warungId)->first();
        if ($saldoExist) {
            $jumlahSaldoBefore = $saldoExist->saldo_total;
            $jumlahSaldoAfter = $jumlahSaldoBefore + $nominal;
            self::where('warung_id', $warungId)
                ->where('gelombang', $gelombang)
                ->update([
                    'saldo_total' => $jumlahSaldoAfter,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            return true;
        } else {
            return false;
        }
    }

    public static function incRembes($warungId, $nominal){
        self::where('warung_id', $warungId)
        ->update([
            'saldo_rembes' => $nominal,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getRembesSemuaWarung($request, $paginate = true){
        $cari = $request->cari ?? null;
        $bulan = $request->bulan ?? null;
        $tahun = $request->tahun ?? null;
        $rembesTerendah = $request->rembesTerendah ?? null;
        $rembesTerbesar = $request->rembesTerbesar ?? null;
        $filterRembes = $request->filterRembes ?? null;
        $ids = !empty($request->ids) ? explode(',', $request->ids) : null;

        $data = self::leftjoin('warung', 'warung.id', 'rembes.warung_id')
                        ->select('warung.id as id_warung', 'warung.nama_warung as nama_warung', 'warung.nama_pemilik', 'warung.nik as nik_pemilik', 'warung.hp as no_whatsapp', 'saldo_total as total_rembes', 'saldo_rembes as sudah_rembes', 'warung.created_at');
        if($cari != null){
            $data->where('warung.nama_warung', 'like', '%' .  $cari . '%');
        }

        if($bulan != null && $tahun != null){
            $data->whereMonth('rembes.created_at', sprintf("%02d", $bulan));
            $data->whereYear('rembes.created_at', $tahun);
        }

        if($filterRembes != null) {
            $data->havingRaw("(rembes.saldo_total - rembes.saldo_rembes) >= $rembesTerendah");
            $data->havingRaw("(rembes.saldo_total - rembes.saldo_rembes) <= $rembesTerbesar");
        }
        $data->orderByRaw('(rembes.saldo_total - rembes.saldo_rembes) DESC');

        if(!empty($ids)){
            $data = $data->whereIn('warung.id', $ids);
        }

        if($paginate == false){
            return $data->get();
        }

        return $data->paginate(10);
    }

    public static function getBelumRembes($request) {
        return self::whereYear('created_at', $request->tahun)
            ->whereMonth('created_at', $request->bulan)
            ->get()
            ->toArray();
    }

    public static function setujuiRembes($request) {
        $data = self::getBelumRembes($request);
        if (count($data) > 0) {
            # Update Batch
            $caseString = 'case id';
            $ids = '';
            foreach ($data as $value) {
                $id = $value['id'];
                $displayIndex = $value['saldo_total'];
                $caseString .= " when $id then $displayIndex";
                $ids .= " $id,";
            }
            $ids = trim($ids, ',');
            return DB::update("update rembes set updated_at = CURRENT_TIMESTAMP() , saldo_rembes = $caseString end where id in ($ids)");
            # End Update Batch
        }
    }
}
