<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Paket;
use DB;

class DetailPaket extends Model
{
    use HasFactory;
    protected $table = 'detail_paket';
    protected $guarded = ['id'];

    public static function getDataByIdPaket($paketId) {
        $datas = self::where('paket_id', $paketId)
            ->join('produk', 'detail_paket.produk_id', 'produk.id')
            ->select([
                'detail_paket.id as detail_paket_id',
                'detail_paket.paket_id as paket_id',
                'detail_paket.produk_id as produk_id',
                'detail_paket.harga_beli as harga_beli',
                'detail_paket.untung as untung',
                'produk.nama as nama_produk',
                'produk.harga_terendah as harga_terendah',
                'produk.harga_tertinggi as harga_tertinggi',
                'detail_paket.harga_satuan as harga_satuan',
                'detail_paket.jumlah_barang as jml_produk',
                'detail_paket.sub_total as sub_total'
            ])
            ->get();
        return $datas;
    }

    public static function deleteByIdPaket($paketId) {
        self::where('paket_id', $paketId)->delete();
    }

    public static function store($data) {
        $paketId = $data['paket_id'];
        Paket::where('id', $paketId)->update([
            'jml_produk' => (Paket::find($paketId)->jml_produk + 1)
        ]);
        self::insert($data);
        self::ubahHargaPaket($paketId);
    }

    public static function deleteData($id) {
        $paketId = self::find($id)->paket_id;
        Paket::where('id', $paketId)->update([
            'jml_produk' => (Paket::find($paketId)->jml_produk - 1)
        ]);
        self::where('id', $id)->delete();
        self::ubahHargaPaket($paketId);
    }

    public static function changeJumlahAll($datas) {
        foreach($datas as $data) {
            $id = $data['detail_paket_id'];
            self::where('id', $id)->update([
                'jumlah_barang' => $data['jml_produk'],
                'harga_satuan' => $data['harga_satuan'],
                'sub_total' => $data['sub_total']
            ]);
        }
        self::ubahHargaPaket($datas[0]['paket_id']);
    }

    private static function ubahHargaPaket($idPaket) {
        $details = self::where('paket_id', $idPaket)->get();
        $totalHarga = $details->sum('sub_total');
        Paket::where('id', $idPaket)->update([
            'total_harga' => $totalHarga
        ]);
    }
}
