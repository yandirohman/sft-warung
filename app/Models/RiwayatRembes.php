<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiPembelian;

class RiwayatRembes extends Model
{
    use HasFactory;

    protected $table = 'riwayat_rembes';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public static function getRiwayatRembesByWarungId($id){
        return self::where('warung_id', $id)
                    ->select('id', 'jumlah_transaksi', 'created_at', 'total_rembes', 'warung_id')
                    ->get();
    }

    public static function getDetailRiwayatRembesById($id){
        $ids = self::where('id', $id)->select('transaksi_pembelian_ids')->first()->transaksi_pembelian_ids;
        $ids = explode ( ',' , $ids);
        return TransaksiPembelian::selectMultipleIdTransaksiPembelian($ids);
    }

    public static function simpanRiwayatRembes($transakiBelumRembes) {
        return self::insert($transakiBelumRembes);
    }
}
