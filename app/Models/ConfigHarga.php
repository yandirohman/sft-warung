<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Session\Middleware\StartSession;
use App\Models\Warung;
use DB;

class ConfigHarga extends Model
{
    use HasFactory;
    protected $table = 'config_harga';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public static function getConfigHargaWarung($warungId, $produkId){
        return self::where('warung_id', $warungId)
                    ->where('produk_id', $produkId)
                    ->select('harga_beli')
                    ->first();
    }

    public static function getConfigById($id){
        return self::where('id', $id)
                    ->select('harga_beli')
                    ->first();
    }

    public static function insertOrUpdateHarga($request){
        if($request->status == 'update'){
            return self::where('id', $request->id)
                        ->update($request->except('warung_id, produk_id, status'));
        }

        return self::create($request->except('warung_id, produk_id, status'));
    }

    public static function ubahConfigHarga($data) {
        $warungId = Warung::getMyId();
        $dataStore = [
            'warung_id' => $warungId,
            'produk_id' => $data['produk_id'],
            'harga_beli' => $data['harga_beli'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
        if ($data['config_harga_id'] == null) {
            self::insert($dataStore);
        } else {
            self::where('id', $data['config_harga_id'])->update($dataStore);
        }
    }

    public static function checkConfigHargaExist() {
        // $warungId = Warung::getMyId();
        // $jmlProduk = Produk::countData();
        // $jmlConfigHarga = self::selectRaw('COUNT(id) as jumlah')
        //     ->where('warung_id', $warungId)->first()->jumlah;
        // if ($jmlConfigHarga >= $jmlProduk) {
        //     return 1;
        // } else {
        //     return 0;
        // }
        $warungId = Warung::getMyId();
        $datas = Produk::leftJoin('config_harga', function($leftJoin) use ($warungId) {
                $leftJoin->on('produk.id', 'config_harga.produk_id');
                $leftJoin->where('config_harga.warung_id', $warungId);
            })->whereNull('produk.deleted_at')->select([
                'produk.id as produk',
                'config_harga.id as config'
            ])->get();

        $passed = 1;
        foreach($datas as $data) {
            if ($data->config == null) {
                $passed = 0;
            }
        }

        return $passed;
    }
}
