<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class RiwayatDompet extends Model
{
    use HasFactory;

    protected $table = 'riwayat_dompet';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

}
