<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Logs extends Authenticatable
{
    protected $table = 'logs';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public static function simpan($data)
    {
        return self::insert($data);
    }

    public static function pilih($select)
    {
        return self::select($select)
            ->orderBy('created_at', 'DESC')
            ->where('user_id', auth()->user()->id)
            ->get();
    }

    public static function pilihPaginate($select)
    {
        return self::select($select)
            ->orderBy('created_at', 'DESC')
            ->where('user_id', auth()->user()->id)
            ->paginate(10);
    }

    public static function pilihFirst($select, $id)
    {
        return self::select($select)->where('id', $id)->first();
    }
}
