<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class Kabupaten extends Authenticatable
{
    protected $table = 'kabupaten';
    protected $guarded = ['id_kab'];
    protected $primaryKey = 'id_kab';

    public static function pilih($select)
    {
        return self::select($select)->where('id_prov', 32)->get();
    }

    public static function getKabJabar()
    {
        $data = self::where('id_prov', 32)
            ->select('id_kab', 'nama')
            ->get();

        return $data;
    }

    public static function getDataByProvId($provId)
    {
        $data = self::where('id_prov', $provId)
            ->select('id_kab', 'nama')
            ->get();

        return response()->json($data);
    }
}
