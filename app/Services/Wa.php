<?php

namespace App\Services;

class Wa {

    private $url;
    private $key;
    private $phone_no;
    private $message;

    public function __construct($url, $key, $phone_no = null, $message = null)
    {
        $this->url = $url;
        $this->key = $key;
        $this->phone_no = $phone_no;
        $this->message = $message;
    }

    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    public function setPhoneNo($phoneNo)
    {
        $this->phone_no = $phoneNo;
        return $this;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    // SEND WA
    public function send() {

        $data = array(
            "key" => $this->key,
            "phone_no" => $this->phone_no,
            "message" => $this->message
        );

        $data_string = json_encode($data);

        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 360);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $res = curl_exec($ch);
        curl_close($ch);
    }
}
