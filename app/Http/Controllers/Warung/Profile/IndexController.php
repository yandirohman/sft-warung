<?php

namespace App\Http\Controllers\Warung\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Warung;

class IndexController extends Controller
{
    public function index()
    {
        $username = auth()->user()->username;
        $data['warung'] = Warung::ambilWarungDenganUsername($username);
        return view('warung.profile.index', $data);
    }
}
