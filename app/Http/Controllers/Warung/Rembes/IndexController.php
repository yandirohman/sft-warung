<?php

namespace App\Http\Controllers\Warung\Rembes;

use App\Http\Controllers\Controller;
use App\Models\TransaksiPembelian;
use App\Models\Warung;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request, $tipe)
    {
        // return $request;
        if ($tipe == 'cair' || $tipe == 'pending') {
            $cari = $request->cari;
            $tgl  = $request->tanggal;
            $user = Auth()->user()->username;
            $data['tgl'] = date($request->tanggal);
            $data['aktif'] = $tipe == 'cair' ? 'data-action-item-active' : '';
            $data['pending'] = $tipe == 'pending' ? 'data-action-item-active' : '';
            $data['saldo'] = Warung::getTotalSaldoRembesWarung($user);
            $data['nama'] = null;
            if(isset($cari) && isset($tgl)){
                $data['transaksi'] = TransaksiPembelian::getTransaksiRembesWarung($user, $tipe, $cari, $tgl);
                $data['nama'] = $cari;
            } else if(isset($tgl)){
                $data['transaksi'] = TransaksiPembelian::getTransaksiRembesWarung($user, $tipe, 'null', $tgl);
            } else {
                $data['transaksi'] = TransaksiPembelian::getTransaksiRembesWarung($user, $tipe, 'null', 'null');
            }

            return view('warung.rembes.index', $data);
        }else{
            abort(404);
        }
    }

    public function detailTransaksi($id){
        return TransaksiPembelian::getDetailTransaksiRiwayat($id);
    }
}
