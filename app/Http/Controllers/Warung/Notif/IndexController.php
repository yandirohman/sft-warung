<?php

namespace App\Http\Controllers\Warung\Notif;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notifikasi;

class IndexController extends Controller
{
    public function index()
    {
        $datas = Notifikasi::getData();
        return view('warung.notif.index', [
            'datas' => $datas
        ]);
    }
}
