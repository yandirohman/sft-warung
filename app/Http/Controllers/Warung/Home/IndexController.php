<?php

namespace App\Http\Controllers\Warung\Home;

use App\Http\Controllers\Controller;
use App\Models\Produk;
use Illuminate\Http\Request;
use App\Models\Warung;
class IndexController extends Controller
{
    public function index()
    {
        $username = auth()->user()->username;
        $data['namaWarung']         = Warung::ambilWarungDenganUsername($username)->nama_warung;
        $data['pendapatanWarung']   = Warung::getPendapatanWarungByUsername($username);
        $data['produk']             = Produk::getWarungProdukTerlaris($username);
        return view('warung.home.index', $data);
    }
}
