<?php

namespace App\Http\Controllers\Warung\Belanja;

use App\Http\Controllers\Controller;
use App\Library\Upload;
use App\Models\TransaksiPembelian;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class UploadFotoSelfieController extends Controller
{
    public function index() {
        return view('warung.belanja.upload-foto-selfie');
    }

    public function store(Request $request) {
        if (isset($request->base64)) {
            $file = Upload::uploadBase64($request->base64, 'foto_penerima');
            TransaksiPembelian::updateFotoPenerima($request->id, $file);
            Alert::success("Berhasil", "Transaksi Anda Berhasil");
            return redirect()->route('warung.belanja.index');
        } else {
            Alert::error("Gagal", "Mohon Selfie Terlebih Dahulu");
            return back();
        }
    }
}
