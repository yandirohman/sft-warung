<?php

namespace App\Http\Controllers\Warung\Belanja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        return view('warung.belanja.index');
    }
}
