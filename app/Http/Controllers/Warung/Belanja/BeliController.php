<?php

namespace App\Http\Controllers\Warung\Belanja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Paket;
use App\Models\Warung;
use App\Models\Produk;
use App\Models\Dompet;
use App\Models\TransaksiPembelian;
use Session;

class BeliController extends Controller
{
    public function index(Request $request, $idPaket = false)
    {
        $penerimaId = $request->p;
        $gelombang = $request->g;
        $pakets = Paket::getData();
        if (count($pakets) == 0) {
            return redirect()->route('warung.konfigurasi.index')->with([
                'msg' => '<div class="alert alert-danger">Silahkan buat paket belanja terlebih dahulu !</div>'
            ]);
        } else {
            $idPaket = $idPaket ? $idPaket : $pakets[0]->id;
            $paketData = Paket::getDataById($idPaket);
            return view('warung.belanja.beli', [
                'penerimaId' => $penerimaId,
                'gelombang' => $gelombang,
                'idPaket' => $idPaket,
                'pakets' => $pakets,
                'paketData' => $paketData
            ]);
        }
    }

    public function beliPaket(Request $request, $idPaket) {
        $request->validate([
            'penerima_id' => 'required',
            'gelombang' => 'required'
        ]);

        $paketExist = Paket::getDataById($idPaket);
        if ($paketExist) {
            Session::put('paket', $paketExist);
            Session::put('request', $request->input());
            return redirect()->route('warung.belanja.checkout.index');
        } else {
            return 'paket tidak ada';
        }
    }

    public function checkout() {
        $paket = Session::get('paket');
        $request = Session::get('request');
        $request = (object) $request;
        $penerimaId = $request->penerima_id;
        $dompet = Dompet::getDataByIdPenerima($penerimaId);
        return view('warung.belanja.checkout', [
            'produks' => json_decode($paket->list_produk),
            'paket' => $paket,
            'request' => $request,
            'dompet' => $dompet
        ]);
    }

    public function catatTransaksi() {
        $paket = Session::get('paket');
        $request = Session::get('request');
        $request = (object) $request;
        $trxId = TransaksiPembelian::beli($request, $paket);
        return redirect()->route('warung.belanja.upload-foto-selfie', [
            'id' => $trxId
        ]);
    }
}
