<?php

namespace App\Http\Controllers\Warung\Belanja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Penerima;
use App\Models\TransaksiPembelian;
use App\Helper\GeneralHelper;

class VerificationController extends Controller
{

    public function verifying(Request $request) {
        $code = $request->code;
        if ($code) {
            $codeSplit = substr($code, 0, 1);
            $idPenerima = str_replace($codeSplit, '', $code);
            $gelombang = GeneralHelper::gelombang($codeSplit);
            $dataExist = Penerima::getDataByIdAndGelombang($idPenerima, $gelombang);
            if ($dataExist) {
                return $this->verificationSuccess($dataExist, $gelombang);
            } else {
                return $this->verificationFail($code);
            }
        } else {
            return $this->verificationFail($code);
        }
    }

    public function verificationSuccess($data, $gelombang)
    {
        $data->gelombang = $gelombang;
        $riwayatTransaksi = TransaksiPembelian::getRiwayatTransaksiPenerima($data->id);
        return view('warung.belanja.verif-success', [
            'data' => $data,
            'riwayatTransaksi' => $riwayatTransaksi
        ]);
    }

    public function verificationFail($code = false)
    {
        return view('warung.belanja.verif-fail', [
            'code' => $code ? $code : 'null'
        ]);
    }

    public function getDetailTransaksi($id)
    {
        return TransaksiPembelian::getDetailTransaksiRiwayat($id);
    }
}
