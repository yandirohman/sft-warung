<?php

namespace App\Http\Controllers\Warung\Konfigurasi;

use App\Http\Controllers\Controller;
use App\Models\ConfigHarga;
use App\Models\Produk;
use App\Models\Warung;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use PhpParser\Node\Expr\Cast\Array_;
use App\Models\Paket;
use App\Models\DetailPaket;

class KonfigurasiController extends Controller
{
    public function index(Request $request)
    {
        $warungId = Warung::getMyId();
        $produk = Produk::getData('id, nama');
        foreach($produk as $data){
            $prod = ConfigHarga::getConfigHargaWarung($warungId, $data->id);
            $data->id_config = $prod->id ?? null;
            $data->harga_beli = $prod->harga_beli ?? 0;
        }

        $page = $request->page ?? 'paket';
        return view('warung.konfigurasi.index', [
            'page' => $page
        ]);
    }

    public function setHargaProdukWarung($id){
        $data = ConfigHarga::getConfigById($id);
        if($data){
            $produk = Produk::getData('harga_terendah, harga_tertinggi', $id);
            $data->harga_terendah = $produk->harga_terendah ?? 0;
            $data->harga_tertinggi = $produk->harga_tertinggi ??     0;
            $data->status = 'update';

            return $data;
        }
        $data = [
            'status'     => 'insert',
            'harga_beli' => 0
        ];

        return $data;
    }

    public function insertOrUpdate(Request $request){
        ConfigHarga::insertOrUpdate($request);
    }

    public function getDataPaket() {
        $warungId = Warung::getMyId();
        $datas = Paket::getDataByWarungId($warungId);
        return $datas;
    }

    public function storeDataPaket(Request $req) {
        $namaPaket = $req->nama_paket ?? 'Paket Belanja';
        $warungId = Warung::getMyId();
        $dataStore = [
            'nama' => $namaPaket,
            'total_harga' => 0,
            'jml_produk' => 0,
            'warung_id' => $warungId,
        ];
        Paket::storeData($dataStore);
    }

    public function deleteDataPaket($id) {
        Paket::deleteData($id);
    }

    public function getDetailPaket($idPaket) {
        $datas = DetailPaket::getDataByIdPaket($idPaket);
        return $datas;
    }

    public function getDataProduk() {
        $datas = Produk::getData();
        return $datas;
    }

    public function storeDataDetailPaket(Request $req) {
        $dataStore = [
            'harga_beli' => $req->harga_beli,
            'harga_satuan' => $req->harga_satuan,
            'jumlah_barang' => $req->jml_produk,
            'paket_id' => $req->paket_id,
            'produk_id' => $req->produk_id,
            'sub_total' => $req->sub_total,
            'untung' => $req->untung,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
        DetailPaket::store($dataStore);
    }

    public function deleteDataDetailPaket($idDetailPaket) {
        DetailPaket::deleteData($idDetailPaket);
    }

    public function storeChangeDetailPaket(Request $req) {
        $datas = $req->datas;
        DetailPaket::changeJumlahAll($datas);
    }

    public function getDataProdukForList() {
        $datas = Produk::getDataLeftJoinConfigHarga();
        return $datas;
    }

    public function changeConfigHarga(Request $req) {
        $data = $req->data;
        ConfigHarga::ubahConfigHarga($data);
    }

    public function checkConfigHarga() {
        $configExist = ConfigHarga::checkConfigHargaExist();
        return $configExist;
    }

}
