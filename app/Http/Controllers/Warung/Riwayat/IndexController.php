<?php

namespace App\Http\Controllers\Warung\Riwayat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Warung;
use App\Models\TransaksiPembelian;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $tanggal = $request->daterange;
        $user = Auth()->user()->username;
        $data['saldo'] = Warung::getTotalSaldoWarung($user);
        if(isset($tanggal)){
            $data['riwayat'] = TransaksiPembelian::getRiwayatTransaksiWarung($user, $tanggal);
        } else {
            $data['riwayat'] = TransaksiPembelian::getRiwayatTransaksiWarung($user, 'null');
        }

        return view('warung.riwayat.index', $data);
    }

    public function getDetailTransaksi($id)
    {
        return TransaksiPembelian::getDetailTransaksiRiwayat($id);
    }
}
