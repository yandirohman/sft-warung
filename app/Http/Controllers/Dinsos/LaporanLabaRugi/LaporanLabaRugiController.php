<?php

namespace App\Http\Controllers\Dinsos\LaporanLabaRugi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Warung;
use App\Models\Kabupaten;

class LaporanLabaRugiController extends Controller
{
    public function index(Request $req){
        $cari       = $req->cari ?? null;
        $kelurahan  = $req->kelurahan ?? null;

        if(!empty($req->all())) {
            $data['kelId'] = $req->kelurahan;
            $data['warung'] = Warung::getLaporanLabaRugi($cari, $kelurahan);
        } else {
            $data['kelId'] = false;
            $data['warung'] = Warung::getLaporanLabaRugi();
        }
        $akumulasi = Warung::getLaporanLabaRugi($cari, $kelurahan, 'tidak');
        $data['akumulasi']['modal']     = $akumulasi->sum('modal');
        $data['akumulasi']['terjual']   = $akumulasi->sum('terjual');
        $labaRugi = $akumulasi->sum('laba_rugi');
        $data['akumulasi']['laba_rugi'] = $labaRugi > 0 ?? 0;
        $data['akumulasi']['piutang']   = $akumulasi->sum('piutang');
        $data['akumulasi']['sudah_cair'] = $akumulasi->sum('sudah_cair');

        $data['kabupaten'] = Kabupaten::getKabJabar();
        $data['currentPage'] = $data['warung']->currentPage();
        $data['lastPage']    = $data['warung']->lastPage();
        $path                = $data['warung']->path();
        $data['nextPage']    = $data['currentPage'] + 1 <= $data['lastPage'] ? $path.'?page='.($data['currentPage'] + 1) : '#';
        $data['prevPage']    = $data['currentPage'] - 1 > 0 ?  $path.'?page='.($data['currentPage'] - 1) : '#';

        return view('dinsos.laporan-laba-rugi.index', $data);
    }
}
