<?php

namespace App\Http\Controllers\Dinsos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;

class FilterDaerahController extends Controller
{
    public function getKabupaten($provId)
    {
        $dataKab = Kabupaten::getDataByProvId($provId);
        return $dataKab;
    }

    public function getKecamatan($kabId)
    {
        $dataKec = Kecamatan::getDataByKabId($kabId);
        return $dataKec;
    }

    public function getKelurahan($kecId)
    {
        $dataKel = Kelurahan::getDataByKecId($kecId);
        return $dataKel;
    }
}
