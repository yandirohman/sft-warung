<?php

namespace App\Http\Controllers\Dinsos\Logs;

use App\Http\Controllers\Controller;
use App\Models\Logs;

class LogsController extends Controller
{
    public function index()
    {
        $select = [
            "id",
            "nama",
            "keterangan",
            "created_at",
        ];

        $data['logsPaginate'] = Logs::pilihPaginate($select);

        return view('dinsos.logs.index', $data);
    }

    public function detail($id)
    {
        $select = [
            "id",
            "nama",
            "keterangan",
            "data_sebelum",
            "data_setelah",
            "created_at",
        ];

        $data['logs'] = Logs::pilihFirst($select, $id);

        $data['sebelum'] = json_decode($data['logs']->data_sebelum, true);
        $data['setelah'] = json_decode($data['logs']->data_setelah, true);

        return view('dinsos.logs.detail', $data);
    }
}
