<?php

namespace App\Http\Controllers\Dinsos\Warung;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Warung\WarungRequest;
use App\Models\Rembes;
use App\Models\Warung;
use App\Models\Kabupaten;
use App\Exports\WarungExport;
use App\Imports\WarungImport;
use Excel;
use Alert;

class WarungController extends Controller
{
    public function index()
    {
        if(isset($_GET['kel'])) {
            $data['kelId'] = $_GET['kel'];
            $data['warung'] = Warung::getDataByIdKel($data['kelId']);

        } else {
            $data['kelId'] = false;
            $data['warung'] = Warung::getData();
        }

        $data['kabupaten'] = Kabupaten::getKabJabar();

        return view('dinsos.warung.warung-index', $data);
    }


    public function cariWarung(Request $request)
    {
        $data['kelId'] = false;
        $data['kabupaten'] = Kabupaten::getKabJabar();
        $data['warung'] = Warung::cariWarung($request->dataCari);

        return view('dinsos.warung.warung-index', $data);
    }

    public function create()
    {
        $data['warung'] = new Warung;
        $data['kabupaten'] = Kabupaten::getKabJabar();
        $data['actionUrl'] = route('dinsos.warung.store');

        return view('dinsos.warung.warung-form', $data);
    }

    public function store(WarungRequest $request)
    {
        $data = $request->validated();
        $getId = Warung::store($data);
        Rembes::insertInWarung($getId);

        Alert::success('Sukses', 'Data Berhasil Ditambahkan');
        return redirect()->route('dinsos.warung.index');
    }

    public function edit(Warung $warung)
    {
        $data['warung'] = $warung;
        $data['kabupaten'] = Kabupaten::getKabJabar();
        $data['actionUrl'] = route('dinsos.warung.update', $warung->id);

        return view('dinsos.warung.warung-form', $data);
    }

    public function update(Warung $warung, WarungRequest $request)
    {
        $data = $request->validated();
        $warungId = $warung->id;
        Warung::store($data, $warungId);

        Alert::success('Sukses', 'Data Berhasil Diubah');
        return redirect()->route('dinsos.warung.index');
    }

    public function show(Warung $warung)
    {
        $data['warung'] = Warung::getDataById($warung->id);

        return view('dinsos.warung.warung-detail', $data);
    }

    public function destroy(Warung $warung)
    {
        Warung::deleteWarung($warung->id);

        Alert::success('Sukses', 'Data Berhasil Dihapus');
        return redirect()->back();
    }

    public function formImport()
    {
        $data['kabupaten'] = Kabupaten::getKabJabar();

        return view('dinsos.warung.import.warung-import', $data);
    }

    public function importWarung(Request $request)
    {
        $file = $request->file('excel_warung');
        $filename = $file->hashName();
        $path = $file->storeAs('public/excel/', $filename);
        $import = Excel::import(new WarungImport($request), storage_path('app/public/excel/' . $filename));

        unlink(storage_path('app/public/excel/' . $filename));
        Alert::success('Success', 'Import Data Warung Berhasil');

        return redirect()->back();
    }

    public function exportWarung()
    {
        return Excel::download(new WarungExport(), 'Warung.xls');
    }

    public function cetakDataWarung(Warung $warung)
    {
        $data['warung'] = $warung;

        return view('dinsos.warung.warung-cetak', $data);
    }

    public function cetakDataWarungAll()
    {
        $data['warungs'] = Warung::getData();

        return view('dinsos.warung.warung-cetak-all', $data);
    }

}
