<?php

namespace App\Http\Controllers\Dinsos\Produk;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdukRequest;
use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Logs;
use Session;
use Alert;

class ProdukController extends Controller
{
    public function index()
    {
        $select = [
            "id",
            "nama",
            "harga_terendah",
            "harga_tertinggi",
            "satuan",
            "foto_produk",
        ];

        $data['listProduk'] = Produk::pilih($select);
        return view('dinsos.produk.index', $data);
    }

    public function cari(Request $cari)
    {
        $select = [
            "id",
            "nama",
            "harga_terendah",
            "harga_tertinggi",
            "satuan",
        ];

        $data['listProduk'] = Produk::cari($select, $cari->cari);
        return view('dinsos.produk.index', $data);
    }

    public function form($id = null)
    {
        $data['icon'] = [
            [
                'icon'=>'beras.svg',
                'nama'=>'beras'
            ],
            [
                'icon'=>'gula.svg',
                'nama'=>'Gula'
            ],
            [
                'icon'=>'minyak.svg',
                'nama'=>'Minyak'
            ],
            [
                'icon'=>'dagingayam.svg',
                'nama'=>'Daging Ayam'
            ],
            [
                'icon'=>'telur.svg',
                'nama'=>'Telur'
            ],
            [
                'icon'=>'gandum.svg',
                'nama'=>'Gandum'
            ],
            [
                'icon'=>'tahu.svg',
                'nama'=>'Tahu'
            ],
            [
                'icon'=>'dagingsapi.svg',
                'nama'=>'Daging Sapi'
            ],
            [
                'icon'=>'gas.svg',
                'nama'=>'Gas'
            ],
        ];

        if (empty($id))
        {
            return view('dinsos.produk.form', $data);
        }
        else
        {
            $select = [
                "id",
                "nama",
                "harga_terendah",
                "harga_tertinggi",
                "satuan",
                "foto_produk"
            ];
            $data['listProduk'] = Produk::pilihFirst($select, $id);
            $data['id'] = $id;
            return view('dinsos.produk.form', $data);
        }
    }

    public function simpan(ProdukRequest $req)
    {
        date_default_timezone_set('Asia/Jakarta');

        $logs = [
            'user_id'    => Session::get('id'),
            'nama'       => Session::get('name'),
            'keterangan' => "Menambahkan Produk Baru <b>$req->nama</b>",
            'created_at' => date('Y-m-d H:i:s')
        ];
        Produk::simpan($req->validated());
        Logs::simpan($logs);
        Alert::success('Sukses', 'Data Berhasil Ditambahkan');
		return redirect('dinsos/produk');
    }

    public function ubah(ProdukRequest $req, $id)
    {
        date_default_timezone_set('Asia/Jakarta');

        $select = [
            "id",
            "nama",
            "harga_terendah",
            "harga_tertinggi",
            "satuan",
        ];

        $beforeProduk = Produk::pilihFirst($select, $id);
        $dataSebelum = json_encode($beforeProduk);

        Produk::ubah($req->validated(), $id);

        $afterProduk = Produk::pilihFirst($select, $id);
        $dataSetelah = json_encode($afterProduk);

        $logs = [
            'user_id'    => Session::get('id'),
            'nama'       => Session::get('name'),
            'keterangan' => "Mengubah Data Produk <b>$beforeProduk->nama</b>",
            'data_sebelum' => $dataSebelum,
            'data_setelah' =>  $dataSetelah,
            'created_at' => date('Y-m-d H:i:s')
        ];

        Logs::simpan($logs);
        Alert::success('Sukses', 'Data Berhasil Dirubah');
		return redirect('dinsos/produk');
    }

    public function hapus($id)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = ['deleted_at' => date('Y-m-d H:i:s')];

        $select = [
            "id",
            "nama",
            "harga_terendah",
            "harga_tertinggi",
            "satuan",
        ];

        $produk = Produk::pilihFirst($select, $id);

        $logs = [
            'user_id'    => Session::get('id'),
            'nama'       => Session::get('name'),
            'keterangan' => "Menghapus Data Produk <b>$produk->nama</b>",
            'created_at' => date('Y-m-d H:i:s')
        ];

        Logs::simpan($logs);
        Produk::ubah($data, $id);
        Alert::success('Sukses', 'Data Berhasil Dihapus');
		return redirect('dinsos/produk');
    }
}
