<?php

namespace App\Http\Controllers\Dinsos\Monitoring;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rembes;
use App\Models\Gelombang;
use App\Models\TransaksiPembelian;
use App\Models\Konfigurasi;
use App\Models\Warung;
use App\Helper\TimeHelper;
use App\Exports\MonitoringExport;
use Excel;

class MonitoringController extends Controller
{
    public function index()
    {
        return view('dinsos.monitoring.monitoring-index');
    }

    public function getData(Request $req){
        $gelombangSaatIni    = Konfigurasi::getDataActive()->bulan;
        if(empty($req->all())){
            $data['gelombang']   = $gelombangSaatIni    = Konfigurasi::getDataActive()->bulan;
            $data['danaBantuan'] = Gelombang::getTotalDanaGelombang($gelombangSaatIni)['total_dana'];
            $data['uangRembes']  = Rembes::getTotalRembesGelombang($gelombangSaatIni)['total'];
            $data['sisaDana']    = $data['danaBantuan'] - $data['uangRembes'];

            $dana = $this->getDana();
            $dataDanaBantuan = $dana['danaBantuan'];
            $dataRealisasi   = $dana['danaRealisasi'];
            $dataRembes      = $dana['danaRembes'];

            $data['labelChart']         = [];
            $data['dataDanaBantuan']    = [];
            $data['dataDanaRealisasi']  = [];
            $data['dataDanaRembes']     = [];
            for($i = 0; $i < $gelombangSaatIni; $i++){
                $gelombang = "Gelombang ".($i + 1);
                array_push($data['labelChart'], $gelombang);

                if(!empty($dataDanaBantuan[$gelombang]) || !empty($dataDanaBantuan[$i+1])){
                    $bantuan = (int) ($dataDanaBantuan[$gelombang]['total_dana'] ?? $dataDanaBantuan[$i+1]['total_dana']);
                }else{
                    $bantuan = 0 ;
                }
                array_push($data['dataDanaBantuan'], (string) $bantuan);

                if(!empty($dataRealisasi[$gelombang]) || !empty($dataRealisasi[$i+1])){
                    $realisasi = (int)($dataRealisasi[$gelombang]['total_harga'] ?? $dataRealisasi[$i+1]['total_harga']);
                }else{
                    $realisasi = 0;
                }
                array_push($data['dataDanaRealisasi'], (string) $realisasi);

                if(!empty($dataRembes[$gelombang]) || !empty($dataRembes[$i+1])){
                    $rembes = (int)($dataRembes[$gelombang]['saldo_rembes'] ?? ($dataRembes[$i+1]['saldo_rembes']) ?? 0);
                }else{
                    $rembes = 0;
                }
                array_push($data['dataDanaRembes'], (string) $rembes);
            }

            $data['warung'] = Warung::getPendapatanSemuaWarungGelombang($gelombangSaatIni);

            return $data;
        }else{
            $tanggalAwal  = $req->tanggalAwal;
            $tanggalAkhir = $req->tanggalAkhir;

            $data['danaBantuan'] = Gelombang::getTotalDanaBerdasarkanRangeTanggal($tanggalAwal, $tanggalAkhir)['total_dana'];
            $data['uangRembes']  = Rembes::getTotalRembesBerdasarkanRangeTanggal($tanggalAwal, $tanggalAkhir)['total'];
            $data['sisaDana']    = $data['danaBantuan'] - $data['uangRembes'];

            $dana = $this->getDana($tanggalAwal, $tanggalAkhir);
            $dataDanaBantuan = $dana['danaBantuan'];
            $dataRealisasi   = $dana['danaRealisasi'];
            $dataRembes      = $dana['danaRembes'];

            $data['labelChart']         = [];
            $data['dataDanaBantuan']    = [];
            $data['dataDanaRealisasi']  = [];
            $data['dataDanaRembes']     = [];
            $daftarTanggal = TimeHelper::getTanggalDariRangeTanggal($tanggalAwal, $tanggalAkhir);
            foreach($daftarTanggal as $tanggal){
                array_push($data['labelChart'], $tanggal);

                if(!empty($dataDanaBantuan[$tanggal])){
                    $bantuan = (int) ($dataDanaBantuan[$tanggal]['total_dana']);
                }else{
                    $bantuan = 0 ;
                }
                array_push($data['dataDanaBantuan'], (string) $bantuan);

                if(!empty($dataRealisasi[$tanggal])){
                    $realisasi = (int)($dataRealisasi[$tanggal]['total_harga']);
                }else{
                    $realisasi = 0;
                }
                array_push($data['dataDanaRealisasi'], (string) $realisasi);

                if(!empty($dataRembes[$tanggal])){
                    $rembes = (int)($dataRembes[$tanggal]['saldo_rembes']);
                }else{
                    $rembes = 0;
                }
                array_push($data['dataDanaRembes'], (string) $rembes);
            }

            $data['warung'] = Warung::getPendapatanSemuaWarungBerdasarkanRangeTanggal($tanggalAwal, $tanggalAkhir);

            return $data;
        }
    }

    public function getDana($tanggalAwal = null, $tanggalAkhir = null){
        if(empty($tanggalAwal) && empty($tanggalAkhir)){
            $dataDanaBantuan     = Gelombang::getTotalDanaPerGelombang();
            $dataDanaBantuan     = $dataDanaBantuan->mapWithKeys(function($item) {
                return [$item['gelombang'] => $item];
            });

            $dataRealisasi       = TransaksiPembelian::getTotalHargaTransaksiPerGelombang();
            $dataRealisasi       = $dataRealisasi->mapWithKeys(function($item) {
                return [$item['gelombang'] => $item];
            });

            $dataRembes          = Rembes::getTotalRembesPerGelombang();
            $dataRembes          = $dataRembes->mapWithKeys(function($item) {
                return [$item['gelombang'] => $item];
            });
        }else{
            $dataDanaBantuan = Gelombang::getTotalDanaBerdasarkanRangeTanggal($tanggalAwal, $tanggalAkhir, true);
            $dataRembes      = Rembes::getTotalRembesBerdasarkanRangeTanggal($tanggalAwal, $tanggalAkhir, true);
            $dataRealisasi   = TransaksiPembelian::getTotalHargaTransaksiBerdasarkanRangeTanggal($tanggalAwal, $tanggalAkhir, true);

            $dataDanaBantuan     = $dataDanaBantuan->mapWithKeys(function($item) {
                return [date('d/m/Y', strtotime($item['created_at'])) => $item];
            });

            $dataRealisasi       = $dataRealisasi->mapWithKeys(function($item) {
                return [date('d/m/Y', strtotime($item['created_at'])) => $item];
            });

            $dataRembes          = $dataRembes->mapWithKeys(function($item) {
                return [date('d/m/Y', strtotime($item['created_at'])) => $item];
            });
        }

        return ['danaBantuan' => $dataDanaBantuan, 'danaRealisasi' => $dataRealisasi, 'danaRembes' => $dataRembes];
    }

    public function getDataWarung(Request $req){
        $search         = $req->search ?? null;
        $pencairanFrom  = $req->pencairanFrom ?? null;
        $pencairanTo    = $req->pencairanTo ?? null;
        $gelombang      = $req->gelombang ?? null;

        if(!empty($req->tanggalAwal) && !empty($req->tanggalAkhir)){
            $data['warung']   = Warung::getPendapatanSemuaWarungBerdasarkanRangeTanggal($req->tanggalAwal, $req->tanggalAkhir, $search, $pencairanFrom, $pencairanTo);
        }else{
            $gelombangSaatIni = Konfigurasi::getDataActive()->bulan;
            $data['warung']   = Warung::getPendapatanSemuaWarungGelombang($gelombang ?? $gelombangSaatIni, $search, $pencairanFrom, $pencairanTo);
        }
        return $data;
    }

    public function exportMonitoring(Request $req){
        return Excel::download(new MonitoringExport($req->all()), 'Laporan Monitoring.xls');
    }

}
