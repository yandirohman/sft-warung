<?php

namespace App\Http\Controllers\Dinsos\Paket;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Dinsos\Paket\PaketStoreRequest;
use App\Models\Paket;
use App\Models\Produk;
use App\Models\Logs;
use Session;
use Alert;

class PaketController extends Controller
{
    private function preData($data) {
        $dataStore = [
            'nama' => $data['nama'],
            'jml_produk' => $data['jml_produk'],
            'list_produk' => $data['list_produk'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'), 
        ];
        $listProduk = json_decode($data['list_produk'], true);
        $dataStore['total_harga'] = 0;
        foreach ($listProduk as $produk) {
            $total = $produk['harga'] * $produk['jumlah'];
            $dataStore['total_harga'] += $total;
        }
        return $dataStore;
    }

    public function index()
    {
        $datas = Paket::getData();
        return view('dinsos.paket.index', [
            'datas' => $datas
        ]);
    }

    public function create() {
        return view('dinsos.paket.form');
    }

    public function search(Request $request) {
        $str = $request->str;
        $datas = Produk::getDataByName($str);
        return $datas;
    }

    public function store(PaketStoreRequest $request) {
        $data = $request->validated();
        $dataStore = $this->preData($data);
        Paket::store($dataStore);
        return redirect()->route('dinsos.paket.index')->with([
            'msg' => '<div class="alert alert-success">Paket berhasil ditambahkan</div>'
        ]);
    }

    public function destroy($id) {
        Paket::destroy($id);
        Alert::success('Sukses', 'Data Berhasil Dihapus');
        return redirect()->back();
    }

    public function show($id) {
        $dataExist = Paket::getDataById($id);
        if ($dataExist) {
            return view('dinsos.paket.show', [
                'datas' => json_decode($dataExist->list_produk, true)
            ]);
        } else {

        }
    }

}
