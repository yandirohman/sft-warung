<?php

namespace App\Http\Controllers\Dinsos\Gelombang;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Konfigurasi;

use Alert;

class GelombangController extends Controller
{
    public function index()
    {
        $data['configGelombang'] = Konfigurasi::getData();

        return view('dinsos.gelombang.gelombang-index', $data);
    }

    public function edit(Konfigurasi $gelombang)
    {
        return $gelombang;
    }

    public function update(Konfigurasi $gelombang, Request $request)
    {
        Konfigurasi::updateData($gelombang->id, $request);

        Alert::success('Success', 'Data Berhasil Diubah');
        return redirect()->back();
    }
}
