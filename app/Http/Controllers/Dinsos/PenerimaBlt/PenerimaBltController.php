<?php

namespace App\Http\Controllers\Dinsos\PenerimaBlt;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Penerima\PenerimaRequest;
use App\Models\Penerima;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Konfigurasi;
use App\Models\Gelombang;
use App\Models\Logs;
use App\Helper\GeneralHelper;
use Session;
use Alert;
use Excel;
use App\Exports\PenerimaBltExport;
use App\Imports\PenerimaBltImport;
use App\Models\Dompet;
use DB;

class PenerimaBltController extends Controller
{
    public function index()
    {
        $select = [
            "penerima.id as penerima_id",
            "penerima.nama",
            "penerima.nik",
            "penerima.kk",
            "penerima.hp",
            "penerima.alamat",
            "penerima.deleted_at",
            "penerima.created_at",
            "gelombang.id as gelombang_id",
            "gelombang.penerima_id",
            "gelombang.gelombang",
            "gelombang.besaran_dana",
        ];

        $data['penerima'] = Penerima::pilih($select);

        return view('dinsos.penerima-blt.penerima-blt-index', $data);
    }

    public function cari(Request $cari)
    {
        $select = [
            "penerima.id as penerima_id",
            "penerima.nama",
            "penerima.nik",
            "penerima.kk",
            "penerima.hp",
            "penerima.alamat",
            "penerima.deleted_at",
            "penerima.created_at",
            "gelombang.id as gelombang_id",
            "gelombang.penerima_id",
            "gelombang.gelombang",
            "gelombang.besaran_dana",
        ];

        $data['penerima'] = Penerima::cari($select, $cari->cari);
        return view('dinsos.penerima-blt.penerima-blt-index', $data);
    }

    public function filter($id)
    {
        $select = [
            "penerima.id as penerima_id",
            "penerima.nama",
            "penerima.nik",
            "penerima.kk",
            "penerima.hp",
            "penerima.alamat",
            "penerima.deleted_at",
            "penerima.created_at",
            "gelombang.id as gelombang_id",
            "gelombang.penerima_id",
            "gelombang.gelombang",
            "gelombang.besaran_dana",
        ];

        $data['penerima'] = Penerima::filter($select, $id);
        $data['id'] = $id;

        return view('dinsos.penerima-blt.penerima-blt-index', $data);
    }

    public function cetakCode($id)
    {
        $select = [
            "penerima.id as penerima_id",
            "penerima.nama",
            "penerima.nik",
            "penerima.kk",
            "penerima.hp",
            "penerima.alamat",
            "penerima.deleted_at",
            "penerima.created_at",
            "gelombang.id as gelombang_id",
            "gelombang.penerima_id",
            "gelombang.gelombang",
            "gelombang.besaran_dana",
        ];

        $data['penerima'] = Penerima::filter($select, $id);
        $data['gelombang'] = GeneralHelper::char($id);

        return view('dinsos.penerima-blt.penerima-blt-cetak-code', $data);
    }

    public function cetakCodeIndividu($id)
    {
        $select = [
            "penerima.id as penerima_id",
            "penerima.nama",
            "penerima.nik",
            "penerima.kk",
            "penerima.hp",
            "penerima.alamat",
            "penerima.deleted_at",
            "penerima.created_at",
            "gelombang.id as gelombang_id",
            "gelombang.penerima_id",
            "gelombang.gelombang",
            "gelombang.besaran_dana",
        ];

        $data['penerima'] = Penerima::pilihFirstJoin($select, $id);
        $data['gelombang'] = GeneralHelper::char($data['penerima']->gelombang);

        return view('dinsos.penerima-blt.penerima-blt-cetak-code-individu', $data);
    }

    public function create()
    {
        $select = [
            'id_kab',
            'id_prov',
            'nama',
            'id_jenis',
        ];

        $data['kabupaten'] = Kabupaten::pilih($select);

        return view('dinsos.penerima-blt.penerima-blt-form', $data);
    }

    public function store(PenerimaRequest $req)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = [
            'id_kab'     => $req->kabupaten,
            'id_kec'     => $req->kecamatan,
            'id_kel'     => $req->kelurahan,
            'nama'       => $req->nama,
            'nik'        => $req->nik,
            'kk'         => $req->kk,
            'hp'         => $req->hp,
            'alamat'     => $req->alamat,
            'created_at' => date('Y-m-d H:i:s')
        ];

        $penerima_id = Penerima::simpanGetId($data);
        $gelombang = Konfigurasi::getDataActive();

        $data_gelombang = [
            'penerima_id'   => $penerima_id,
            'gelombang'     => $req->gelombang ?? $gelombang->bulan,
            'besaran_dana'  => $gelombang->besaran_dana,
            'is_cair'       => 0,
            'created_at'    => date('Y-m-d H:i:s')
        ];

        Gelombang::simpan($data_gelombang);

        $logs = [
            'user_id'    => Session::get('id'),
            'nama'       => Session::get('name'),
            'keterangan' => "Menambahkan Penerima Baru <b>$req->nama</b>",
            'created_at' => date('Y-m-d H:i:s')
        ];

        Logs::simpan($logs);
        Alert::success('Sukses', 'Data Berhasil Ditambahkan');
		return redirect()->route('dinsos.penerima-blt.index');
    }

    public function show($id)
    {
        $select = [
            "id",
            "nama",
            "keterangan",
            "data_sebelum",
            "data_setelah",
            "created_at",
        ];

        $penerima = [
            "penerima.id as penerima_id",
            "penerima.nama",
            "penerima.nik",
            "penerima.kk",
            "penerima.hp",
            "penerima.alamat",
            "gelombang.gelombang",
            "gelombang.besaran_dana",
        ];

        $data['penerima'] = Penerima::pilihFirstJoin($penerima, $id);

        return view('dinsos.penerima-blt.penerima-blt-detail', $data);
    }

    public function edit($id)
    {
        $select = [
            'id_kab',
            'id_prov',
            'nama',
            'id_jenis',
        ];

        $penerima = [
            "penerima.id",
            "penerima.nama",
            "penerima.nik",
            "penerima.kk",
            "penerima.hp",
            "penerima.alamat",
        ];

        $data['kabupaten'] = Kabupaten::pilih($select);
        $data['penerima'] = Penerima::pilihFirst($penerima, $id);

        return view('dinsos.penerima-blt.penerima-blt-form', $data);
    }

    public function update(PenerimaRequest $req, $id)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = [
            'id_kel'     => $req->kelurahan,
            'nama'       => $req->nama,
            'nik'        => $req->nik,
            'kk'         => $req->kk,
            'hp'         => $req->hp,
            'alamat'     => $req->alamat,
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $select = [
            "penerima.id",
            "penerima.nama",
            "penerima.nik",
            "penerima.kk",
            "penerima.hp",
            "penerima.alamat",
            "gelombang.gelombang",
            "gelombang.besaran_dana",
        ];

        $beforePenerima = Penerima::pilihFirstJoin($select, $id);
        $dataSebelum = json_encode($beforePenerima);

        Penerima::ubah($data, $id);

        $afterPenerima = Penerima::pilihFirstJoin($select, $id);
        $dataSetelah = json_encode($afterPenerima);

        $logs = [
            'user_id'    => Session::get('id'),
            'nama'       => Session::get('name'),
            'keterangan' => "Mengubah Data Penerima <b>$beforePenerima->nama</b>",
            'data_sebelum' => $dataSebelum,
            'data_setelah' =>  $dataSetelah,
            'created_at' => date('Y-m-d H:i:s')
        ];

        Logs::simpan($logs);
        Alert::success('Sukses', 'Data Berhasil Dirubah');
		return redirect()->route('dinsos.penerima-blt.index');
    }

    public function destroy($id)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = ['deleted_at' => date('Y-m-d H:i:s')];

        $select = [
            "nama",
            "nik"
        ];

        $penerima = Penerima::pilihFirst($select, $id);

        $logs = [
            'user_id'    => Session::get('id'),
            'nama'       => Session::get('name'),
            'keterangan' => "Menghapus Data Penerima <b>$penerima->nama</b> dengan NIK <b>$penerima->nik</b>",
            'created_at' => date('Y-m-d H:i:s')
        ];

        Logs::simpan($logs);
        Gelombang::softDelete($data, $id);
        Penerima::ubah($data, $id);
        Alert::success('Sukses', 'Data Berhasil Dihapus');
		return redirect()->route('dinsos.penerima-blt.index');
    }

    public function jsonKecamatan(Request $req)
    {
        $select = [
            'id_kec',
            'id_kab',
            'nama',
        ];

        return Kecamatan::pilih($select, $req->id);
    }

    public function jsonKelurahan(Request $req)
    {
        $select = [
            'id_kel',
            'id_kec',
            'nama',
        ];

        return Kelurahan::pilih($select, $req->id);
    }

    // Import Export Excel
    public function formImport()
    {
        $data['kabupaten'] = Kabupaten::getKabJabar();

        return view('dinsos.penerima-blt.import.penerima-blt-import', $data);
    }

    public function importPenerima(Request $request)
    {
        $file = $request->file('excel_penerima');
        $filename = $file->hashName();
        $path = $file->storeAs('public/excel/', $filename);
        $import = Excel::import(new PenerimaBltImport($request), storage_path('app/public/excel/' . $filename));

        unlink(storage_path('app/public/excel/' . $filename));
        Alert::success('Success', 'Import Data Penerima BLT Berhasil');

        return redirect()->back();

    }

    public function exportPenerima()
    {
        return Excel::download(new PenerimaBltExport(), 'Daftar-penerima-blt.xlsx');
    }

    public function cairkanDana($id)
    {
        $data = Gelombang::getDataCairkan($id);

        Dompet::store($data);
        Gelombang::ubah($id);
        Alert::success('Success', 'Pencairan Dana Penerima BLT Berhasil');
		return redirect()->back();
    }
}
