<?php

namespace App\Http\Controllers\Auth;

use App\Helper\UserHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Warung;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisteredUserController extends Controller
{
    public function check()
    {
        return view('auth.register-check');
    }
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        Auth::login($user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'role' => 2 # warung
        ]));

        event(new Registered($user));
        UserHelper::setSession(auth()->user()->role);
        return redirect()->route('warung.home.index');
    }

    public function storeCheck(Request $request)
    {
        $warung = Warung::checkWarung($request->username);
        if ($warung) {
            $user = User::checkUser($warung->username);
            if (!$user) {
                return back()->with('success', "Warung Anda Terdaftar, Klik Di <a href='/register?id=$warung->id&name=$warung->nama_warung&username=$warung->username'><b>Sini</b></a>");
            } else {
                return back()->with('info', "Warung Anda Sudah Terdaftar, Silahkan <a href='/login'><b>Login<b></a>");
            }
        }
        return back()->with('info', 'Warung Anda Belum Terdaftar');
    }
}
