<?php

namespace App\Http\Controllers\Bjb\ActivityLog;

use App\Http\Controllers\Controller;
use App\Models\Logs;
use Illuminate\Http\Request;

class BjbActivityLogController extends Controller
{
    public function index()
    {
        $select = [
            'id',
            'nama',
            'keterangan',
            'created_at'
        ];
        return Logs::pilih($select);
    }

    public function detail($id)
    {
        $select = [
            "id",
            "nama",
            "keterangan",
            "data_sebelum",
            "data_setelah",
            "created_at",
        ];

        $data['logs'] = Logs::pilihFirst($select, $id);

        $data['sebelum'] = json_decode($data['logs']->data_sebelum, true);
        $data['setelah'] = json_decode($data['logs']->data_setelah, true);

        return view('dinsos.logs.detail', $data);
    }

}
