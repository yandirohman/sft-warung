<?php

namespace App\Http\Controllers\Bjb\PencairanRembes;

use App\Http\Controllers\Controller;
use App\Models\Rembes;
use App\Models\RiwayatRembes;
use App\Models\TransaksiPembelian;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Warung;

class PencairanRembesController extends Controller
{
    public function index()
    {
        return view('bjb.pencairan-rembes.pencairan-rembes-index');
    }
    public function semua(Request $request) {
        return Rembes::getRembesSemuaWarung($request);
    }

    public function history($id){
        return RiwayatRembes::getRiwayatRembesByWarungId($id);
    }

    public function detailHistory($id, $warungId){
        $data['transaksi']  = RiwayatRembes::getDetailRiwayatRembesById($id);
        $data['warung']     = Warung::getDataById($warungId);
        return $data;
    }

    public function cariTransaksi(Request $req, $warungId){
        $cariPenerima = $req->penerima ?? null;
        $bulan = $req->bulan ?? null;
        $tahun = $req->tahun ?? null;

        if ($req->tanggal_from) {
            $dataTransaksi = TransaksiPembelian::getSemuaTransaksiWarungRange($warungId, $req);
        } else {
            $dataTransaksi = TransaksiPembelian::getSemuaTransaksiWarung($warungId, $cariPenerima, $bulan, $tahun);
        }
        foreach($dataTransaksi as $index => $transaksi){

            if(is_null($transaksi['rembesed_at'])){
                $status = 'Pending';
            }else{
                $status = 'Sudah Dicairkan';
            }

            if(is_null($transaksi['foto_transaksi']) || $transaksi['foto_transaksi'] == '-'){
                $status = 'Foto Tidak Ada';
            }

            $dataTransaksi[$index]['status'] =  $status;
            $dataTransaksi[$index]['tanggal'] = date('d/m/Y', strtotime($transaksi['created_at']));
        }

        return $dataTransaksi;
    }

    public function cariTransaksiRange(Request $req, $warungId){
        $dataTransaksi = TransaksiPembelian::getSemuaTransaksiWarungRange($warungId, $req);
        foreach($dataTransaksi as $index => $transaksi){

            if(is_null($transaksi['rembesed_at'])){
                $status = 'Pending';
            }else{
                $status = 'Sudah Dicairkan';
            }

            if(is_null($transaksi['foto_transaksi']) || $transaksi['foto_transaksi'] == '-'){
                $status = 'Foto Tidak Ada';
            }

            $dataTransaksi[$index]['status'] =  $status;
            $dataTransaksi[$index]['tanggal'] = date('d/m/Y', strtotime($transaksi['created_at']));
        }

        return $dataTransaksi;
    }

    public function pencairanRembes(Request $request) {
        $transaksiBelumRembes = TransaksiPembelian::getTransaksiBelumRembes($request);
        if ($transaksiBelumRembes) {
            RiwayatRembes::simpanRiwayatRembes($transaksiBelumRembes);
            TransaksiPembelian::rembeskanTransaksi($request);
            Rembes::setujuiRembes($request);
            return response()->json([
                'status' => 200,
                'message' => 'Data Berhasil Dirembeskan'
            ]);
        } else {
            return response()->json([
                'status' => 204,
                'message' => 'Tidak Ada Data Yang Bisa Dirembeskan'
            ]);
        }
    }
    public function cetakSemuaWarungRembes(Request $req, $warungId){
        if(empty($req->all())){
            $filter = ['bulan' => date('m'), 'tahun' => date('Y')];
        }else{
            $filter = $req;
        }

        $data['warung'] = Warung::getDataById($warungId);
        $data['transaksi'] = TransaksiPembelian::getTransaksiBelumRembesByIdWarung($filter, false);
        return view('bjb.pencairan-rembes.cetak-pencairan-rembes', $data);
    }

    public function getDetailTransaksi($id) {
        return TransaksiPembelian::getDetailTransaksi($id);
    }
    public function getDetailTransaksiPenerima($id) {
        return TransaksiPembelian::getDetailPenerimaTransaksi($id);
    }
}
