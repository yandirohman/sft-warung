<?php

namespace App\Http\Controllers\Bjb\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Gelombang;
use App\Models\Konfigurasi;
use App\Models\Produk;
use App\Models\TransaksiPembelian;
use App\Models\Warung;
use Illuminate\Http\Request;

class BjbDashboardController extends Controller
{
    public function index(){
        $data['warungTerlaris'] = Warung::getSemuaWarungTerlaris();
        $totalRembes            = 0;
        foreach($data['warungTerlaris'] as $warung){
            $totalRembes        += $warung->jumlah;
        }
        $data['totalRembes'] = $totalRembes;

        $data['riwayatTransaksiBulanIni']  = TransaksiPembelian::getTransaksiSemuaWarung('bulan ini');
        $data['riwayatTransaksiMingguIni'] = TransaksiPembelian::getTransaksiSemuaWarung('minggu ini');
        $data['riwayatTransaksiHariIni']   = TransaksiPembelian::getTransaksiSemuaWarung('hari ini');

        $data['produk'] = Produk::getProdukBerdasarkanHarga();
        return view('bjb.dashboard.dashboard-bjb', $data);
    }

    public function getDataChart(){
        $gelombangSekarang     = Konfigurasi::getDataActive()->bulan;
        // return $gelombangSekarang;

        // Data Pie Chart Penyaluran
            $saldoWarung           = (int) Warung::getTotalSaldoSemuaWarungGelombang($gelombangSekarang)['saldo_total'] ?? 0;
            $saldoPenerima         = (int) Gelombang::getTotalDanaGelombang($gelombangSekarang)['total_dana'] ?? 0;
            $total                 = $saldoWarung + $saldoPenerima;
            $data['saldoWarung']   = $saldoWarung == 0 ? 0 : round(($saldoWarung/$total)*100);
            $data['saldoPenerima'] = $saldoPenerima == 0 ? 0 : 100 - $data['saldoWarung'];
        //End Data Pie Chart

        // Data Bar Chart
            $tempRealisasi      = TransaksiPembelian::getTotalHargaTransaksiPerBulan();
            $tempDanaBantuan    = Gelombang::getTotalDanaPerBulan();
            $tempRealisasi      = $tempRealisasi->mapWithKeys(function($item){
                return [$item->bulan => $item];
            });
            $tempDanaBantuan      = $tempDanaBantuan->mapWithKeys(function($item){
                return [$item->bulan => $item];
            });

            $dana = [];
            $danaRealisasi['name']  = 'Realisasi';
            $danaBantuan['name']    = 'Dana Bantuan';
            $danaRealisasi['data']  = [];
            $danaBantuan['data']    = [];
            for( $i = 0; $i < 12; $i++){
                array_push($danaRealisasi['data'], (int)( $tempRealisasi[$i]['total_harga'] ?? 0));
                array_push($danaBantuan['data'], (int)( $tempDanaBantuan[$i]['total_dana'] ?? 0));
            }
            array_push($dana, $danaBantuan, $danaRealisasi);
            $data['chartDanaVsRealisasi'] = $dana;
        // End Data Bar Chart

        // Data Pie Chart Rembes
            $saldo = Warung::getTotalSaldoSemuaWarungGelombang($gelombangSekarang);
            $saldoTotal          = (int) ($saldo['saldo_total'] ?? 0);
            $saldoRembes         = (int) ($saldo['saldo_rembes'] ?? 0);
            $total               = $saldoTotal + $saldoRembes;
            $data['saldoTotal']  = $saldoTotal == 0 ? 0 : round(($saldoTotal/$total)*100);
            $data['saldoRembes'] = $saldoRembes == 0 ? 0 : 100 - $data['saldoWarung'];
        //End Data Pie Chart

        // Data Bar Chart Saldo Warung
            $saldoPerWarung = Warung::getPendapatanSemuaWarungGelombang($gelombangSekarang, null, null, null, 'tidak');
            // return $saldoPerWarung;
            foreach($saldoPerWarung as $warung){
                $data['pendapatanPerwarung']['transaksi'][] = $warung->saldo_total;
                $data['pendapatanPerwarung']['rembes'][] = $warung->saldo_rembes;
                $data['pendapatanPerwarung']['namaWarung'][] = $warung->nama_warung;
            }
        // End Data Bar Chart
        return $data;
    }

    public function getProdukPerWarung(string $id){
        $data['produk'] = Produk::getData('nama, harga_terendah, harga_tertinggi', $id);
        $data['produkPerWarung'] = Produk::getProdukPerWarung($id);
        return $data;
    }
}
