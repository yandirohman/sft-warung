<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                $role = Session::get('loginRole');
                switch ($role) {
                    case 'dinsos':
                        $redirect = 'dinsos.dashboard';
                        break;

                    case 'warung':
                        $redirect = 'warung.home.index';
                        break;

                    case 'bjb':
                        $redirect = 'bjb.dashboard';
                        break;

                    default:
                        $redirect = '404';
                        break;
                }
                return redirect()->route($redirect);
            }
        }

        return $next($request);
    }
}
