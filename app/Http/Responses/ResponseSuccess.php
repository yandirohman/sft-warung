<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Response;

class ResponseSuccess
{
    public static function make($data)
    {
        return Response::json([
            'data' => $data,
        ], 200);
    }
}
