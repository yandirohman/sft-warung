<?php

namespace App\Http\Requests\Penerima;

use Illuminate\Foundation\Http\FormRequest;

class PenerimaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'      => 'required',
            'nik'       => 'required',
            'kk'        => 'required',
            'hp'        => 'required',
            'alamat'    => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'gelombang' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'nama'      => 'Nama Penerima',
            'nik'       => 'NIK Penerima',
            'kk'        => 'KK Penerima',
            'hp'        => 'No Hp Penerima',
            'alamat'    => 'Alamat Penerima',
            'kabupaten' => 'Kabupaten Penerima',
            'kecamatan' => 'Kecamatan Penerima',
            'kelurahan' => 'Kelurahan Penerima',
            'gelombang' => 'Bulan',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Data :attribute perlu diisi'
        ];
    }
}
