<?php

namespace App\Http\Requests\Warung;

use Illuminate\Foundation\Http\FormRequest;
use Str;

class WarungRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nama_warung' => 'required',
            'nama_pemilik' => 'required',
            'nik' => 'required',
            'hp' => 'required',
            'alamat' => 'required',
            'username' => 'nullable',
            'id_prov' => 'nullable',
            'id_kab' => 'nullable',
            'id_kec' => 'nullable',
            'id_kel' => 'nullable'
        ];
    }

    protected function prepareForValidation()
    {
        $uniqueCode = Str::random(3);

        $this->merge([
            'username' => strtolower(str_replace(' ', '', $this->nama_warung)).$uniqueCode
        ]);
    }

    public function attributes()
    {
        return [
            'nama_warung' => 'Nama Warung',
            'nama_pemilik' => 'Nama Pemilik',
            'nik' => 'NIK',
            'hp' => 'Nomor HP',
            'alamat' => 'Alamat'
        ];
    }


    public function messages()
    {
        return [
            'required' => 'Data :attribute perlu diisi'
        ];
    }
}
