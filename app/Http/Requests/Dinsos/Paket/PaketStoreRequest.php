<?php

namespace App\Http\Requests\Dinsos\Paket;

use Illuminate\Foundation\Http\FormRequest;

class PaketStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nama' => 'required',
            'jml_produk' => 'required',
            'list_produk' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Nama Produk',
            'jml_produk' => 'Jumlah Produk',
            'list_produk' => 'List Produk',
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => ':attribute harus diisi',
            'jml_produk.required' => ':attribute harus diisi',
            'list_produk.required' => ':attribute harus diisi',
        ];
    }
}

