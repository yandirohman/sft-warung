<?php

namespace App\Http\Requests;

use App\Library\Upload;
use Illuminate\Foundation\Http\FormRequest;

class ProdukRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nama' => 'required',
            'harga_terendah' => 'required',
            'harga_tertinggi' => 'required',
            'satuan' => 'required',
            'foto_produk' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Nama Produk',
            'harga_terendah' => 'Harga Produk',
            'harga_tertinggi' => 'Harga Produk',
            'satuan' => 'Satuan Produk',
            'foto_produk' => 'Foto Produk'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Data :attribute perlu diisi'
        ];
    }

    // public function prepareForValidation()
    // {
    //     if(strpos($this->foto_produk, 'https://storage.googleapis.com/') !== false) {
    //     } else {
    //         $this->merge([
    //             'foto_produk' => Upload::uploadBase64($this->foto_produk, "foto_produk")
    //         ]);
    //     }
    // }
}
