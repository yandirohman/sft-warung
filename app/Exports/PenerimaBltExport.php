<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\Penerima;

class PenerimaBltExport implements FromView, ShouldAutoSize
{
    public function __construct()
    {
    }

    public function view() : View
    {
        $data['penerimaBlt'] = Penerima::getDataForExport();
        return view('dinsos.penerima-blt.export.penerima-blt-export', $data);
    }
}
