<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\Warung;

class WarungExport implements FromView, ShouldAutoSize
{
    public function __construct(){
    }

    public function view() : View{
        $data['warung'] = Warung::getData(false);
        return view('dinsos.warung.export.warung-export', $data);
    }
}
