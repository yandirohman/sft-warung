<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\Warung;
use App\Models\Konfigurasi;

class MonitoringExport implements FromView, ShouldAutoSize
{
    private $tanggalAwal;
    private $tanggalAkhir;
    private $gelombang;
    private $search;
    private $pencairanFrom;
    private $pencairanTo;
    private $paginate;

    public function __construct($filter){
        $this->search         = $filter->search ?? null;
        $this->pencairanFrom  = $filter->pencairanFrom ?? null;
        $this->pencairanTo    = $filter->pencairanTo ?? null;
        $this->gelombang      = $filter->gelombang ?? null;
        $this->paginate       = $filter->paginate ?? 'Ya';
        $this->tanggalAwal    = $filter->tanggalAwal ?? null;
        $this->tanggalAkhir   = $filter->tanggalAkhir ?? null;
    }

    public function view() : View{
        if(!empty($this->tanggalAwal) && !empty($this->tanggalAkhir)){
            $data['dataWarung']   = Warung::getPendapatanSemuaWarungBerdasarkanRangeTanggal($this->tanggalAwal, $this->tanggalAkhir, $this->search, $this->pencairanFrom, $this->pencairanTo, $this->paginate);
        }else{
            $gelombangSaatIni = Konfigurasi::getDataActive()->bulan;
            $data['dataWarung']   = Warung::getPendapatanSemuaWarungGelombang($this->gelombang ?? $gelombangSaatIni, $this->search, $this->pencairanFrom, $this->pencairanTo, $this->paginate);
        }
        return view('dinsos.monitoring.export.export-monitoring', $data);
    }
}
