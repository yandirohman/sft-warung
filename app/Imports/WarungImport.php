<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\ToCollection;

use DB;
use Str;
use App\Models\Warung;
use App\Models\Rembes;
use Hash;

class WarungImport implements ToCollection, WithStartRow
{
    public function __construct($data)
    {
        $this->idKab = $data->id_kab;
        $this->idKec = $data->id_kec;
        $this->idKel = $data->id_kel;
    }

    public function collection(Collection $rows)
    {
        ini_set('max_execution_time', 300);

        foreach ($rows as $row) {
            $uniqueCode = Str::random(3);
            $kodeAkses = Str::random(6);
            $username = strtolower(str_replace(' ', '', $row[1])).$uniqueCode;

            $warungId = Warung::insertGetId([
                'username' => $username,
                'kode_akses' => $kodeAkses,
                'id_kab' => $this->idKab,
                'id_kec' => $this->idKec,
                'id_kel' => $this->idKel,
                'nama_warung' => $row[1],
                'nama_pemilik' => $row[2],
                'nik' => $row[3],
                'hp' => $row[4],
                'alamat' => $row[5],
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $getGelombang = DB::table('konfigurasi')
                          ->where('is_active', 1)
                          ->pluck('bulan')->first();

            Rembes::insert([
                'warung_id' => $warungId,
                'gelombang' => $getGelombang,
                'saldo_total' => 0,
                'saldo_rembes' => 0,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            // Insert to Table Users
            DB::table('users')->insert([
                'name' => $row[1],
                'username' => $username,
                'password' => Hash::make($kodeAkses),
                'role' => 2,
            ]);

        }
    }

    public function startRow(): int
    {
        return 2;
    }
}
