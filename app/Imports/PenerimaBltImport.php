<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\ToCollection;

use DB;
use App\Models\Penerima;
use App\Models\Gelombang;

class PenerimaBltImport implements ToCollection, WithStartRow
{
    public function __construct($data)
    {
        $this->idKab = $data->id_kab;
        $this->idKec = $data->id_kec;
        $this->idKel = $data->id_kel;
    }

    public function collection(Collection $rows)
    {
        ini_set('max_execution_time', 300);

        foreach ($rows as $row) {

            $penerimaId = Penerima::insertGetId([
                'id_kab' => $this->idKab,
                'id_kec' => $this->idKec,
                'id_kel' => $this->idKel,
                'nama' => $row[1],
                'kk' => $row[2],
                'nik' => $row[3],
                'hp' => $row[4],
                'alamat' => $row[5],
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $gelombang = DB::table('konfigurasi')
                           ->where('is_active', 1)
                           ->select('bulan', 'besaran_dana')
                           ->first();

            Gelombang::insert([
                'penerima_id' => $penerimaId,
                'gelombang' => $gelombang->bulan,
                'besaran_dana' => $gelombang->besaran_dana,
                'is_cair' => 0,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

    public function startRow(): int
    {
        return 2;
    }
}
