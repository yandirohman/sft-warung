FROM tiduronline/php:7.3.13-fpm-imagick
LABEL maintainer="Delly Fauzian <fauzianddelly@gmail.com>"

COPY . /tmp/html

COPY ./entrypoint.sh /etc/entrypoint.sh

COPY ./etc/php/php.ini-production /usr/local/etc/php/php.ini

RUN apt-get update \
    && apt-get install -y \
    libzip-dev \
    wget \
    && pecl install redis \
    && docker-php-ext-install pdo pdo_mysql zip \
    && docker-php-ext-enable redis \
    && cp -rf /tmp/html/ /var/www \
    && cd /var/www/html \
    && ls \
    && php composer.phar -o install; \
    chmod +x /etc/entrypoint.sh; \
    chown -Rf www-data.www-data /var/www/html; \
    # chown -Rf www-data.www-data /etc/vendor; \
    # chmod -R 777 /etc/vendor; \
    # cp /var/www/html/etc/IseedServiceProvider.php /etc/vendor/orangehill/iseed/src/Orangehill/Iseed/IseedServiceProvider.php; \
    php composer.phar dump-autoload; \
    rm -rf /tmp/html;

WORKDIR /var/www/html
ENTRYPOINT ["sh", "/etc/entrypoint.sh"]