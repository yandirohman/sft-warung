<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsInDetailPaket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_paket', function (Blueprint $table) {
            $table->integer('harga_beli');
            $table->integer('untung');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_paket', function (Blueprint $table) {
            $table->dropColumn('harga_beli');
            $table->dropColumn('untung');
        });
    }
}
