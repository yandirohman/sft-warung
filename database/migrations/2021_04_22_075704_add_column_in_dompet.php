<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInDompet extends Migration
{
    public function up()
    {
        Schema::table('dompet', function (Blueprint $table) {
            $table->integer('gelombang')->after('penerima_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('dompet', function (Blueprint $table) {
            $table->dropCplumn('gelombang');
        });
    }
}
