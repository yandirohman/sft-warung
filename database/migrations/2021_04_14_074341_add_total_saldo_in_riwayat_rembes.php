<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalSaldoInRiwayatRembes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riwayat_rembes', function (Blueprint $table) {
            $table->double('total_saldo')->after('transaksi_pembelian_ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riwayat_rembes', function (Blueprint $table) {
            $table->dropColumn('total_saldo');
        });
    }
}
