<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailTransaksiPembelianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksi_pembelian', function (Blueprint $table) {
            $table->id();
            $table->string('transaksi_pembelian_id');
            $table->string('penerima_id');
            $table->string('warung_id');
            $table->string('produk_id');
            $table->integer('jumlah_produk');
            $table->double('harga_satuan');
            $table->double('sub_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksi_pembelian');
    }
}
