<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInKonfigurasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('konfigurasi', function (Blueprint $table) {
            $table->string('besaran_dana')->nullable()->after('bulan');
            $table->integer('is_active')->nullable()->default(0)->after('besaran_dana');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('konfigurasi', function (Blueprint $table) {
            //
        });
    }
}
