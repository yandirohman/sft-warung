<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiwayatDompetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_dompet', function (Blueprint $table) {
            $table->id();
            $table->string('penerima_id');
            $table->string('transaksi_pembelian_id');
            $table->string('gelombang_id');
            $table->double('nominal');
            $table->string('sumber');
            $table->string('jenis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_dompet');
    }
}
