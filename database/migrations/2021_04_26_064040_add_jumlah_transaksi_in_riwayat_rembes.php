<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJumlahTransaksiInRiwayatRembes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riwayat_rembes', function (Blueprint $table) {
            $table->integer('jumlah_transaksi')->after('transaksi_pembelian_ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riwayat_rembes', function (Blueprint $table) {
            $table->dropColumn('jumlah_transaksi');
        });
    }
}
