<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInPenerima extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penerima', function (Blueprint $table) {
            $table->integer('id_prov')->after('id')->nullable();
            $table->integer('id_kab')->after('id_prov');
            $table->integer('id_kec')->after('id_kab');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penerima', function (Blueprint $table) {
            $table->dropCplumn('id_prov');
            $table->dropCplumn('id_kab');
            $table->dropCplumn('id_kec');
        });
    }
}
