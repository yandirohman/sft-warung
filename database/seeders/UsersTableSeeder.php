<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Dinas Sosial',
                'username' => 'dinsos',
                'password' => '$2y$10$Q16LrZffEgaP.gINqIntZ.WJCvlpXQWMB.CU.bZGcOCV2Yj5cR7Aa',
                'role' => '1',
                'created_at' => '2021-04-14 04:01:59',
                'updated_at' => '2021-04-14 04:01:59',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Warung',
                'username' => 'warung',
                'password' => '$2y$10$Q16LrZffEgaP.gINqIntZ.WJCvlpXQWMB.CU.bZGcOCV2Yj5cR7Aa',
                'role' => '2',
                'created_at' => '2021-04-14 04:01:59',
                'updated_at' => '2021-04-14 04:01:59',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Bjb',
                'username' => 'bjb',
                'password' => '$2y$10$Q16LrZffEgaP.gINqIntZ.WJCvlpXQWMB.CU.bZGcOCV2Yj5cR7Aa',
                'role' => '3',
                'created_at' => '2021-04-14 04:01:59',
                'updated_at' => '2021-04-14 04:01:59',
            ),
        ));


    }
}
