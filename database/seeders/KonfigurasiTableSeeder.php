<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class KonfigurasiTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('konfigurasi')->delete();
        
        \DB::table('konfigurasi')->insert(array (
            0 => 
            array (
                'id' => 1,
                'tahun' => '2021',
                'bulan' => '1',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'tahun' => '2021',
                'bulan' => '2',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'tahun' => '2021',
                'bulan' => '3',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'tahun' => '2021',
                'bulan' => '4',
                'besaran_dana' => '200000',
                'is_active' => 1,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'tahun' => '2021',
                'bulan' => '5',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'tahun' => '2021',
                'bulan' => '6',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'tahun' => '2021',
                'bulan' => '7',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'tahun' => '2021',
                'bulan' => '8',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'tahun' => '2021',
                'bulan' => '9',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'tahun' => '2021',
                'bulan' => '10',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'tahun' => '2021',
                'bulan' => '11',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'tahun' => '2021',
                'bulan' => '12',
                'besaran_dana' => '200000',
                'is_active' => 0,
                'created_at' => '2021-04-26 14:27:14',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}